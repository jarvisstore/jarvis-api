FORMAT: 1A

# Jarvis Api

# Products [/products]

## Display all products. [GET /products/products?bestseller=true,newproduct=true,query='',category='',collection='']
Get a JSON representation of all the products.

+ Parameters
    + bestseller: (string, optional) - filter product based on bestseller
        + Default: 
    + newproduct: (string, optional) - filter product based on newproduct
    + query: (string, optional) - search product by name
    + category: (string, optional) - search product by category name
    + collection: (string, optional) - search product by collection name

## Display 1 product. [GET /products/products/{id}]
Get a JSON representation of detail the product.

+ Parameters
    + id: (string, optional) - product id

# Blogs [/blogs]

## Display all blog. [GET /blogs/blogs?query='']
Get a JSON representation of all the blog.

+ Parameters
    + query: (string, optional) - search blog by name

# Orders [/orders]

## Display detail order. [GET /orders/orders/{order_code}]
Get a JSON representation of order.

+ Parameters
    + order_code: (string, optional) - order code