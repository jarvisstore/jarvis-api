<?php
function mandrill_template($replacements, $template)
{
    return preg_replace_callback('/\*\|(.+?)\|\*/', function($matches) use ($replacements)
    {
        try {
            return $replacements[$matches[1]];
        } catch (Exception $e) {
            return '';
        }

    }, $template);
}