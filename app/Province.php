<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 1/11/17
 * Time: 12:15 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinsi';
}