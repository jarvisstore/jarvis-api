<?php

namespace App\Jobs;

use App\JarvisMail;

class VerificationAccountJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(JarvisMail $mail)
    {
        $data = [
            'from' => 'Jarvis Store <info@jarvis-store.com>',
            'to' => $this->data['email'],
            'subject' => 'Verifikasi Akun Jarvis Store',

        ];
        $msg = view('emails.mandrill.verification-jarvis-store');
        $vars = [
            'SUBJECT' => $data['subject'],
            'URLTOKO' => url('http://'.$this->data['alamatJarvis'].'.jstore.co/admin/verification/'.$this->data['verification_code'])

        ];
        $data['message'] = mandrill_template($vars,$msg);
        $mail->sendMail($data);
    }
}
