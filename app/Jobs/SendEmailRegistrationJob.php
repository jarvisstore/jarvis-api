<?php

namespace App\Jobs;

use App\JarvisMail;

class SendEmailRegistrationJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(JarvisMail $mail)
    {
        $data = [
            'from' => 'Jarvis Store <info@jarvis-store.com>',
            'to' => $this->data['email'],
            'subject' => 'Jarvis Store - Selamat Toko Online Berhasil Dibuat',

        ];
        $msg = view('emails.mandrill.welcome-to-jarvis');
        $vars = [
            'SUBJECT' => $data['subject'],
            'NAMAPELANGGAN' => $this->data['namaToko'],
            'PASSWORD' => $this->data['password'],
            'URLTOKO' => 'http://'.$this->data['alamatJarvis'].'.'.config('app.subdomain'),
            'EMAILPELANGGAN' => $this->data['to'],

        ];
        $data['message'] = mandrill_template($vars,$msg);
        $mail->sendMail($data);
    }
}
