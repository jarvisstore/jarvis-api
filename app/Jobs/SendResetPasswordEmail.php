<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/24/17
 * Time: 5:32 PM
 */

namespace App\Jobs;


use App\Account;
use App\JarvisMail;

class SendResetPasswordEmail extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(JarvisMail $mail)
    {
        $account = Account::find($this->data['id']);
        $data = [
            'from' => 'Jarvis Store <info@jarvis-store.com>',
            'to' => $this->data['to'],
            'subject' => 'Reset Password Jarvis Store',

        ];
        $msg = view('emails.mandrill.request-reset-password-jarvis');
        $vars = [
            'SUBJECT' => $data['subject'],
            'NAMAPELANGGAN' => $this->data['to'],
            'LINKRESETPASS' => url($account->namaToko.'.jstore.co/admin/recovery/' . $this->data['id'] . '/' . $this->data['reset_code'])
        ];
        $data['message'] = mandrill_template($vars,$msg);
        $mail->sendMail($data);
    }

}
