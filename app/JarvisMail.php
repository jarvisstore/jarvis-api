<?php namespace App;

use Aws\Ses\SesClient;

class JarvisMail
{

    public $client;
    public $bucket;
    public $key;
    public $akun;

    public function __construct()
    {
        $client = new SesClient(array(
            'credentials' => array(
                'key' => env('SES_KEY'),
                'secret' => env('SES_SECRET'),
            ),
            'region' => 'us-east-1',
            'version' => 'latest'
        ));
        $this->client = $client;
    }

    public function sendMail($data)
    {

        $msg = array();
        $msg['Source'] = $data['from'];
        //ToAddresses must be an array
        $msg['Destination']['ToAddresses'][] = $data['to'];

        $msg['Message']['Subject']['Data'] = $data['subject'];
        $msg['Message']['Subject']['Charset'] = "UTF-8";
        $msg['Message']['Body']['Html']['Data'] = $data['message'];
        $msg['Message']['Body']['Html']['Charset'] = "UTF-8";

        try {
            $this->client->sendEmail($msg);
            return true;
        } catch (Exception $e) {
            //An error happened and the email did not get sent
            return ($e->getMessage());
            return false;
        }


    }
}
