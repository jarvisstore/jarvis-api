<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 12/7/16
 * Time: 10:46 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    public static $rules = array();
    protected $table = 'order';

}