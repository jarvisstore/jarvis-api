<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 1/11/17
 * Time: 11:43 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    public static $rules = array();
    protected $table = 'pengaturan';

    public function city()
    {
        return $this->belongsTo(City::class,'kota');
    }

    public function province()
    {
        return $this->belongsTo(Province::class,'provinsi');
    }

    public function country()
    {
        return $this->belongsTo(Country::class,'negara');
    }
}