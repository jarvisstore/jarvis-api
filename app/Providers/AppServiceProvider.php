<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
    {
        Validator::extend('valid_shop_name', function($attribute, $value, $parameters)
        {
            $terlarang = array('blog','direct','directs');
            if(in_array($value,$terlarang)){
                return false;
            }else{
                return true;
            }
        });

        Validator::extend('alpha_num_space', function($attribute, $value, $parameters)
        {
            return preg_match('/^([a-z0-9 ])+$/i', $value);
        });
        Validator::extend('alpha_num_without_space', function($attribute, $value, $parameters)
        {
            return preg_match('/^([a-z0-9-])+$/i', $value);
        });

    }

}
