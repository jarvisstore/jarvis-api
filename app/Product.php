<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {
    protected $guarded = array();
    public $timestamps = false;
    public static $rules = array();
    protected $table = 'produk';

    public function options(){
        return $this->hasMany(ProductOption::class,'produkId');
    }
    public function options_sku(){
        return $this->hasMany(ProductSku::class,'produkId')->orderBy('id','ASC');
    }
    public function category()
    {
        return $this->belongsTo(Category::class,'kategoriId');
    }
    public function collection()
    {
        return $this->belongsTo(Collection::class,'koleksiId');
    }
    public function detailorder(){
        return $this->hasMany('DetailOrder','id');
    }

    public function produkMall(){
        return $this->hasOne('ProdukMall','productId');
    }

    public function url(){
        $account = app()->make('App\Account');
        if($this->slug==''){
            if($account->alamatWeb != '')
            {
                return url('http://'.$account->alamatWeb . '/produk/'.Str::slug($this->nama));
            }
            return url('http://'.$account->alamatJarvis. '.jstore.co/produk/'.str_slug($this->nama));
        }

        if($account->alamatWeb != '')
        {
            return url('http://'.$account->alamatWeb . '/produk/'.$this->slug);
        }
        return url('http://'.$account->alamatJarvis. '.jstore.co/produk/'.$this->slug);
    }

    public function image($id){
        $account = app()->make('App\Account');
        switch ($id) {
            case '1':
                $url = $this->gambar1;
                break;
            case '2':
                $url = $this->gambar2;
                break;
            case '3':
                $url = $this->gambar3;
                break;
            case '4':
                $url = $this->gambar4;
                break;
            
            default:
                # code...
                break;
        }
        $type = '';
        if($url == '')
            return '';
        if($account->s3 == 1){
            return 'https://d2kl9mvmw5l7p9.cloudfront.net/'.$account->folder.'/produk/'.$type.$url;
        }else{
            return url('http://jarvis-store.com/'.$account->folder.'/produk/'.$type.$url);
        }
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($produk) { // called BEFORE delete()

            $produk->options()->delete();
            $produk->options_sku()->delete();

        });
    }
}
