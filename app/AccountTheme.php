<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/24/17
 * Time: 3:27 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class AccountTheme extends Model
{
    protected $table = 'themes';
    public $timestamps = false;
}