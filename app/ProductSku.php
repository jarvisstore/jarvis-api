<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 8/9/17
 * Time: 12:16 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class ProductSku extends Model
{
    protected $table = 'opsisku';
}