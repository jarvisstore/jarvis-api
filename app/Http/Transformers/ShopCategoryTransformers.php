<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/28/17
 * Time: 3:31 PM
 */

namespace App\Http\Transformers;


use App\ShopCategory;
use League\Fractal\TransformerAbstract;

class ShopCategoryTransformers extends TransformerAbstract
{
    public function transform(ShopCategory $category){
        return [
            'id' => $category->id,
            'name' => $category->nama,
        ];
    }

}