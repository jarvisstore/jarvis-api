<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 9/22/17
 * Time: 10:54 AM
 */

namespace App\Http\Transformers;


use App\ProductOption;
use League\Fractal\TransformerAbstract;

class ProductOptionTransformer extends TransformerAbstract
{
    public function transform(ProductOption $product){
        return [
            'id' => $product->id,
            'name' => $product->nama,
            'value' => $product->value,
        ];
    }
}