<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 9/22/17
 * Time: 10:41 AM
 */

namespace App\Http\Transformers;


use App\ProductSku;
use League\Fractal\TransformerAbstract;

class OpsiSkuTransformer extends TransformerAbstract
{
    public function transform(ProductSku $product){
        return [
            'id' => $product->id,
            'opsi_1' => $product->opsi1,
            'opsi_2' => $product->opsi2,
            'opsi_3' => $product->opsi3,
            'price'    => $product->harga,
            'stock' => $product->stock,
            'barcode' => $product->barcode,
        ];
    }
}