<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/24/17
 * Time: 6:16 PM
 */

namespace App\Http\Transformers;

use App\City;
use League\Fractal\TransformerAbstract;

class CityTransformers extends TransformerAbstract
{
    public function transform(City $obj){
        return [
            'id' => (int) $obj->id,
            'name' => $obj->nama,
        ];
    }
}