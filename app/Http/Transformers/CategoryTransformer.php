<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 12/7/16
 * Time: 10:47 AM
 */

namespace App\Http\Transformers;

use App\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends  TransformerAbstract
{
    public function transform(Category $category){
        return [
            'id' => $category->id,
            'name' => $category->nama,
            'slug' => $category->slug,
            'parent' => $category->parent,
            'order' => $category->urutan,
        ];
    }
}