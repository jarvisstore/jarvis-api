<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/28/17
 * Time: 3:35 PM
 */

namespace App\Http\Transformers;


use App\JarvisTheme;
use League\Fractal\TransformerAbstract;

class JarvisThemeTransformers extends TransformerAbstract
{
    public function transform(JarvisTheme $obj){
        return [
            'id' => (int) $obj->id,
            'name' => $obj->nama,
            'image' => 'https://d3kamn3rg2loz7.cloudfront.net/img/themes/'.$obj->nama.'/'.$obj->nama.'-preview-720.jpg',
            'description' => $obj->ket,
            'type' => $obj->type

        ];
    }
}