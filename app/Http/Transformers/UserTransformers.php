<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 12/20/16
 * Time: 11:58 AM
 */

namespace App\Http\Transformers;


use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformers extends TransformerAbstract
{

    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->nama,
            'email' => $user->email,
            'address' => $user->alamat,
            'telp'   =>  $user->telp,
            'country_id' => $user->negara,
            'province_id' => $user->provinsi,
            'city_id' => $user->kota,
            'company' => $user->perusahaan,
            'date_of_birth' => $user->tglLahir,
            'type' => $user->tipe,
            'note' => $user->catatan,
            'last_login' => $user->last_login,
            'created_at' => $user->created_at,
        ];
    }

}
