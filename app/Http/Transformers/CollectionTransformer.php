<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 8/16/17
 * Time: 3:05 PM
 */

namespace App\Http\Transformers;


use App\Collection;
use League\Fractal\TransformerAbstract;

class CollectionTransformer extends TransformerAbstract
{
    public function transform(Collection $obj){
        return [
            'id' => (int) $obj->id,
            'name' => $obj->nama,
        ];
    }

}