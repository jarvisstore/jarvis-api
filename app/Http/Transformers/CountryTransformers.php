<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/24/17
 * Time: 6:16 PM
 */

namespace App\Http\Transformers;


use App\Country;
use League\Fractal\TransformerAbstract;

class CountryTransformers extends TransformerAbstract
{
    public function transform(Country $country){
        return [
            'id' => (int) $country->id,
            'name' => $country->nama,
        ];
    }
}