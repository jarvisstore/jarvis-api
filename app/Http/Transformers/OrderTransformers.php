<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 12/20/16
 * Time: 11:45 AM
 */

namespace App\Http\Transformers;


use App\Order;
use League\Fractal\TransformerAbstract;

class OrderTransformers extends TransformerAbstract
{
    public function transform(Order $order){
        return [
            'order_code' => (int) $order->kodeOrder,
            'order_date' => $order->tanggalOrder,
            'total' => (int) $order->total,
            'status' => (int) $order->status,
            'status_text' => $order->status == 0 ? 'Pending' : ($order->status == 1 ? 'Pembaran masuk' :($order->status == 2 ? 'Pembayaran Diterima' : ($order->status == 3 ? 'Terkirim' : 'Cancel' )) ),
            'expedition' => $order->jenisPengiriman,
            'expedition_cost' => (int) $order->ongkosKirim,
            'payment_type' => (int) $order->jenisPembayaran,
            'name' => $order->nama,
            'telp' => $order->telp,
            'address' => $order->alamat,
            'city' => $order->kota,
            'subdisctric' => $order->kecamatan,
            'message' => $order->pesan,
            'no_resi' => $order->noResi,
        ];
    }
}