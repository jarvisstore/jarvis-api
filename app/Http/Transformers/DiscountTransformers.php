<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 8/16/17
 * Time: 3:05 PM
 */

namespace App\Http\Transformers;


use App\Collection;
use App\Discount;
use League\Fractal\TransformerAbstract;

class DiscountTransformers extends TransformerAbstract
{
    public function transform(Discount $obj){
        return [
            'id' => (int) $obj->id,
            'code' => $obj->kode,
            'discount_type' => $obj->jenisPotongan,
            'discount' => $obj->besarPotongan,
            'total_discount' => $obj->jumlah,
            'total_claim' => $obj->klaim,
            'minimal_order' => $obj->minBuy,
            'product_id' => $obj->produkId,
            'category_id' => $obj->kategoriId,
            'collection_id' => $obj->koleksiId,
            'account_id' => $obj->akunId,
            'start_date' => $obj->tglMulai,
            'end_date' => $obj->tglBerakhir,
        ];
    }

}