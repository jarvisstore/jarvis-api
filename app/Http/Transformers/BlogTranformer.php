<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 12/20/16
 * Time: 11:58 AM
 */

namespace App\Http\Transformers;


use App\Blog;
use League\Fractal\TransformerAbstract;

class BlogTranformer extends TransformerAbstract
{

    public function transform(Blog $blog)
    {
        return [
            'id' => $blog->id,
            'name' => $blog->judul,
            'tags' => $blog->tags,
            'content' => $blog->isi,
            'url'   =>  $blog->url(),
            'status'   =>  $blog->status,
            'created_at'   =>  $blog->created_at,
            'updated_at'   =>  $blog->updated_at,
        ];
    }

}