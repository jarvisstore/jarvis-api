<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 1/11/17
 * Time: 11:37 AM
 */

namespace App\Http\Transformers;


use App\Account;
use App\AccountTheme;
use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;

class AccountTransformers extends TransformerAbstract
{
    public function transform(Account $account)
    {
        $akun = DB::table('onlineakun')->where('akunId','=',$account->id)->get();
        $doku_account = DB::table('doku_account')->where('akunId',$account->id)->first();
        $veritrans_account = DB::table('veritrans_account')->where('account_id',$account->id)->first();
        $banks = DB::table('bank')->join('bankdefault','bank.bankDefaultId','=','bankdefault.id')->where('akunId','=',$account->id)->where('status','=',1)->get();
        $total_tema = AccountTheme::where('akunId', $account->id)->count();
        $bank_transfer = [];
        foreach ($banks as $bank)
        {
            array_push($bank_transfer, [
                'rekening' => $bank->noRekening,
                'an' => $bank->atasNama,
                'bank' => $bank->nama,
            ]);

        }
        $rs = [
            'id' => $account->id,
            'shop_name' => $account->setting->nama,
            'logo'      =>  $account->logo(),
            'title'      =>  $account->setting->judul,
            'description'      =>  $account->setting->deskripsi,
            'email'      =>  $account->setting->email,
            'telp'      =>  $account->setting->telp,
            'mobile'      =>  $account->setting->hp,
            'bbm'      =>  $account->setting->bbm,
            'address'      =>  $account->setting->alamat,

            'city'      =>  $account->setting->city->nama,
            'province'      =>  $account->setting->province->nama,
            'country'      =>  $account->setting->country->nama,
            'facebook'      =>  $account->setting->fb,
            'twitter'      =>  $account->setting->tw,
            'google_plus'      =>  $account->setting->gp,
            'instagram'      =>  $account->setting->ig,
            'payment' => [
                'bank_transfer' => $bank_transfer
            ],
            'setup' => $account->status == 0 ? true : false,
            'theme_setup' => $total_tema == 0 ? true : false,
        ];

        if($doku_account)
        {
            $rs['payment']['doku_payment'] = true;
        }

        if($veritrans_account)
        {
            $rs['payment']['midtrans'] = true;
        }

        return $rs;
    }

}