<?php

namespace App\Http\Transformers;

use App\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'category',
        'options_sku',
        'options'
    ];

    public function transform(Product $product){
        return [
            'id' => $product->id,
            'name' => $product->nama,
            'short_description' => $this->shorter($product->deskripsi,200),
            'description' => $product->deskripsi,
            'vendor'    => $product->vendor,
            'sell_price' => $product->hargaJual,
            'mitra_price' => $product->hargaMitra,
            'discount_price' => $product->hargaCoret,
            'barcode' => $product->barcode,
            'weight' => $product->berat,
            'stock' => $product->stok,
            'image1' => $product->image(1),
            'image2' => $product->image(2),
            'image3' => $product->image(3),
            'image4' => $product->image(4),
            'tags'  => $product->tags,
            'dmt'  => $product->dmt,
            'kmt'  => $product->kmt,
            'visibility'  => $product->visibility,
            'bestseller'  => $product->terlaris,
            'new_product'  => $product->produkbaru,
            'url' => $product->url()
        ];
    }

    public function includeCategory(Product $product)
    {
        $category= $product->category;
        if($category)
            return $this->item($category, new CategoryTransformer());
        return null;
    }

    public function includeOptionsSku(Product $product)
    {
        $option = $product->options_sku;
        if($option)
            return $this->collection($option, new OpsiSkuTransformer());
        return null;
    }

    public function includeOptions(Product $product)
    {
        $option = $product->options;
        if($option)
            return $this->collection($option, new ProductOptionTransformer());
        return null;
    }

    private function shorter($text, $chars_limit)
    {
        // Check if length is larger than the character limit
        if (strlen($text) > $chars_limit)
        {
            // If so, cut the string at the character limit
            $new_text = substr($text, 0, $chars_limit);
            // Trim off white space
            $new_text = trim($new_text);
            // Add at end of text ...
            return $new_text . "...";
        }
    // If not just return the text as is
    else
        {
            return $text;
        }
    }
}