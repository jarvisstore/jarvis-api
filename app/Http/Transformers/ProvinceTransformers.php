<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/24/17
 * Time: 6:16 PM
 */

namespace App\Http\Transformers;


use App\Province;
use League\Fractal\TransformerAbstract;

class ProvinceTransformers extends TransformerAbstract
{
    public function transform(Province $obj){
        return [
            'id' => (int) $obj->id,
            'name' => $obj->nama,
        ];
    }
}