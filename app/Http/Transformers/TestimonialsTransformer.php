<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 12/7/16
 * Time: 10:47 AM
 */

namespace App\Http\Transformers;

use App\Testimonial;
use League\Fractal\TransformerAbstract;

class TestimonialsTransformer extends  TransformerAbstract
{
    public function transform(Testimonial $testimonial){
        return [
            'id' => $testimonial->id,
            'name' => $testimonial->nama,
            'content' => $testimonial->isi,
            'visibility' => $testimonial->visibility,
            'created_at' => $testimonial->created_at,
            'updated_at' => $testimonial->updated_at,
        ];
    }
}