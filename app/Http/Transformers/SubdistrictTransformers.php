<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/28/17
 * Time: 3:22 PM
 */

namespace App\Http\Transformers;


use App\Subdistrict;
use League\Fractal\TransformerAbstract;

class SubdistrictTransformers extends TransformerAbstract
{
    public function transform(Subdistrict $obj){
        return [
            'id' => (int) $obj->id,
            'name' => $obj->nama,
        ];
    }
}