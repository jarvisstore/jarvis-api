<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return "hello";
});

$app->get('/asd', function () use ($app) {
    return "hello";
});

$app->group(['namespace' => 'App\Http\Controllers\Bot', 'prefix' => 'bot'], function () use ($app) {
    // Using The "App\Http\Controllers\Admin" Namespace...
    $app->post('login', [
        'as' => 'bot.login',
        'uses' => 'LoginController@store'
    ]);

    $app->get('user', [
        'as' => 'bot.user',
        'uses' => 'LoginController@user'
    ]);
    $app->group(['namespace' => 'User'], function () use ($app) {
        // Using The "App\Http\Controllers\Admin\User" Namespace...
    });
});


$api = app('Dingo\Api\Routing\Router');

$api->version('v1', [
    'namespace' => 'App\Http\Controllers',
], function ($api) {

    $api->post('auth/register', [
        'as' => 'register',
        'uses' => 'AuthController@register'
    ]);

    //forget password
    $api->post('auth/forget-password', [
        'as' => 'forget-password',
        'uses' => 'AuthController@forgetPassword'
    ]);
    //login
    $api->post('/auth/login', 'AuthController@loginPost');

    //country
    $api->get('/countries', 'CountryController@index');
    $api->get('/provinces', 'ProvinceController@index');
    $api->get('/cities', 'CityController@index');
    $api->get('/subdistricts', 'SubdistrictController@index');
    $api->get('/shop-categories', 'JarvisStoreCategoryController@index');
    $api->get('/jarvis-themes', 'JarvisThemeController@index');


});

$api->version('v1', [
    'namespace' => 'App\Http\Controllers',
    'middleware' => 'domain'
], function ($api) {
    //account setup
    $api->post('auth/setup/{id}', [
        'as' => 'setup',
        'uses' => 'AuthController@setup'
    ]);
    $api->post('auth/install-theme/{id}', [
        'as' => 'install-theme',
        'uses' => 'AuthController@installTheme'
    ]);
});
$api->version('v1', [
    'namespace' => 'App\Http\Controllers\V1',
    'middleware' => 'domain'
], function ($api) {


    //product
    $api->resource('products', 'ProductController');

    //category
    $api->resource('categories', 'CategoryController');

    //koleksi
    $api->resource('collections', 'CollectionController');

    //discount
    $api->resource('discounts', 'DiscountController');

    //blog
    $api->resource('blogs', 'BlogController');

    //users
    $api->resource('users', 'UserController');

    //testimony
    $api->resource('testimonials', 'TestimonialsController');

    //ekspedisi

    //order
    $api->resource('orders', 'OrderController');

    //get all payment gateway

    //shop setting
    //$api->resource('settings', 'OrderController');

    //reporting

    $api->resource('images', 'ImageController');


    $api->get('account', 'AccountController@index');
});

$api->version('v2', [
    'namespace' => 'App\Http\Controllers\V1',
    'middleware' => 'domain'
], function ($api) {
    $api->get('products', function () {
        return 1;
    });

});
