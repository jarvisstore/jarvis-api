<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 12/20/16
 * Time: 10:31 AM
 */

namespace App\Http\Middleware;

use Closure;
use App\Account;
use Auth;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class StoreAuthMiddleware
{
    public function __construct(JWTAuth $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {

        if (! $this->auth->parser()->setRequest($request)->hasToken()) {
            throw new BadRequestHttpException('Token not provided');
        }

        try {
            $this->auth->parseToken()->authenticate();
        } catch (JWTException $e) {
            throw new UnauthorizedHttpException('jwt-auth', $e->getMessage(), $e, $e->getCode());
        }
        $user = Auth::getUser();

        $account = $user->account;

        if($account){
            app()->bind('App\Account', function ($app) use ($account){
                return $account;
            });
            return $next($request);
        }
        return response('Unauthorized.', 401);


    }

}
