<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/24/17
 * Time: 6:18 PM
 */

namespace App\Http\Controllers;


use App\City;
use App\Http\Controllers\Controller;
use App\Http\Transformers\CityTransformers;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

/**
 * @Resource("City",uri="/cities")
 */

class CityController extends Controller
{
    use Helpers;

    /**
     * Display all city.
     *
     * Get a JSON representation of all the city.
     *
     * @Get("?province_id={id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("province_id", description="filter by province id"),
     * })
     */
    public function index(Request $request)
    {
        if ($request->has('province_id')) {
            $cities = Cache::get('provinces_' . $request->get('province_id'), function () use ($request) {
                return City::where('provinsiId', $request->get('province_id'))->orderBy('nama')->get();
            });
        } else {
            $cities = Cache::get('provinces', function () {
                return City::orderBy('nama')->get();
            });
        }

        return $this->response->collection($cities, new CityTransformers());
    }
}