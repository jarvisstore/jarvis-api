<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/24/17
 * Time: 6:18 PM
 */

namespace App\Http\Controllers;

use App\Http\Transformers\SubdistrictTransformers;
use App\Subdistrict;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

/**
 * @Resource("Subdistrict",uri="/subdistricts")
 */
class SubdistrictController extends Controller
{
    use Helpers;

    /**
     * Display all subdistrict.
     *
     * Get a JSON representation of all the province.
     *
     * @Get("/?city_id={id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("city_id", description="city id"),
     * })
     */

    public function index(Request $request)
    {
        if ($request->has('city_id')) {
            $districts = Cache::get('districts_' . $request->get('city_id'), function () use ($request) {
                return Subdistrict::where('kabupatenId', $request->get('city_id'))->orderBy('nama')->get();
            });
        } else {
            $districts = Cache::get('districts_', function () {
                return Subdistrict::orderBy('nama')->get();
            });
        }

        return $this->response->collection($districts, new SubdistrictTransformers());
    }
}