<?php

namespace App\Http\Controllers;

use App\AccountTheme;
use App\Group;
use App\JarvisPackage;
use App\JarvisTheme;
use App\Jobs\SendEmailRegistrationJob;
use App\Jobs\SendResetPasswordEmail;
use App\Jobs\VerificationAccountJob;
use App\Setting;
use App\Subdistrict;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Queue;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\JWTAuth;
use Dingo\Api\Routing\Helpers;
use App\Http\Transformers\AccountTransformers;
use App\Http\Transformers\UserTransformers;
use League\Fractal;
use App\Account;
use League\Fractal\Manager;
use Laravel\Lumen\Routing\Controller;
use Validator;

/**
 * @Resource("Auth",uri="/auth")
 */

class AuthController extends Controller
{
    use Helpers;
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * Login.
     *
     * Login to system using email & password.
     *
     * @Post("/login")
     * @Versions({"v1"})
     * @Transaction({
     *     @Request({"email","password"}),
     *     @Response(404,body={"message":"user with that credential not found","status_code":404}),
     *     @Response(200,body={"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmphcnZpcy5sb2NhbC9hdXRoL2xvZ2luIiwiaWF0IjoxNTAxMTMwMTcyLCJleHAiOjE1MDExMzM3NzIsIm5iZiI6MTUwMTEzMDE3MiwianRpIjoibFUza25Zakk5Tmd5dTE5ZyIsInN1YiI6Mzc0Mn0.7TTs-kwAwSvnDRg-Q_nxG9NKXE5x84Skco_p334fMhk",
     *     "user":{"data":
     *                  {"id":3742,"name":"","email":"ajaajak@mailinator.com","address":"","telp":""}
 *              },"account":{"id":4024,"shop_name":"joools","setup":true}}),
     *     })
     */
    public function loginPost(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);
        try {
            if (!$token = $this->jwt->attempt($request->only('email', 'password'))) {
                return $this->response->error('user with that credential not found', 404);
            }
        } catch (TokenExpiredException $e) {
            return $this->response->error('token_expired', $e->getCode());
        } catch (TokenInvalidException $e) {
            return $this->response->error('token_invalid', $e->getCode());
        } catch (JWTException $e) {
            return $this->response->error($e->getMessage(), $e->getCode());
        }
        $fractal = new Manager();
        $resource2 = new Fractal\Resource\Item($this->jwt->user(), new UserTransformers);

        if ($this->jwt->user()->account->status == 0) {
            $account = [
                'id' => $this->jwt->user()->account->id,
                'shop_name' => $this->jwt->user()->account->namaToko,
                'setup' => true
            ];
        } else {
            $resource = new Fractal\Resource\Item($this->jwt->user()->account, new AccountTransformers);
            $account = $fractal->createData($resource)->toArray();
        }
        return response()->json([
            'token' => $token,
            'user' => $fractal->createData($resource2)->toArray(),
            'account' => $account
        ]);
    }

    /**
     * Register.
     *
     * Register new user to jarvis
     *
     * @Post("/register")
     * @Versions({"v1"})
     *
     * @Transaction({
     *     @Request({"shop_name","email","password"}),
     *     @Response(422, body={"message":"The shop name field is required.,The Email Address has already been taken.","status_code":422}),
     *     @Response(200, body={"url":"http:\/\/joools.jstore.co\/account_setup","token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmphcnZpcy5sb2NhbC9hdXRoL3JlZ2lzdGVyIiwiaWF0IjoxNTAxMTI5OTM3LCJleHAiOjE1MDExMzM1MzcsIm5iZiI6MTUwMTEyOTkzNywianRpIjoiUEFTd3hiWDltam42b3A3MyIsInN1YiI6Mzc0Mn0.VmlUww5ggUv94nhklkvF56s0k6V98ezr_DFYCh3ziS4","setup":true,"account":{"id":4024,"shop_name":"joools","setup":true}})
     * })

     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shop_name' => 'required|valid_shop_name|unique:akun,namaToko|alpha_num_space',
            'email' => 'required|email|unique:akun,email',
            'password' => 'required|min:5'
        ]);
        if ($validator->fails()) {
            return $this->response->error(implode(',',$validator->errors()->all()), 422);
        }

        $shop_name = preg_replace('!\s+!', ' ', $request->get('shop_name'));
        $shop_name = str_replace(' ', '-', strtolower(trim($shop_name)));

        $packet_type = 0;
        $jarvis_package = JarvisPackage::find($packet_type);

        $account = Account::create([
            'namaToko' => $shop_name,
            'email' => $request->get('email'),
            'folder' => $shop_name . '-upload',
            'ip' => $request->getClientIp(),
            'tanggalDaftar' => date("Y-m-d"),
            'alamatJarvis' => $shop_name,
            'jenisPaket' => $packet_type,
            'maxProduk' => $jarvis_package->total_product,
            'storage' => $jarvis_package->storage,
            's3' => 1,
            'jenisPaket' => $packet_type,
            'nama' => $shop_name,
        ]);

        //register pelanggan
        $data = array(
            'nama' => $account->nama,
            'akunId' => $account->id,
            'email' => $account->email,
            'password' => Hash::make($request->get('password')),
            'kodepos' => '',
            'perusahaan' => '',
            'telp' => '',
            'tglLahir' => '',
            'perusahaan' => '',
            'tipe' => 1,
            'tanggalMasuk' => date("Y-m-d"),
        );
        $user = User::create($data);
        $activationCode = $user->getActivationCode();
        $user->attemptActivation($activationCode);
        $userGroup = Group::find(1);
        $user->groups()->attach($userGroup);

        //AWS SES
        $this->sendEmail($account, $request->get('password'), $activationCode);
        $token = $this->jwt->attempt($request->only('email', 'password'));

        $fractal = new Manager();
        $resource = new Fractal\Resource\Item($account, new AccountTransformers());
        $rs = array(
            'url' => url('http://' . $account->alamatJarvis . '.' . config('app.subdomain') . '/account_setup'),
            'token' => $token,
            'setup' => true,
            'account' => [
                'id' => $account->id,
                'shop_name' => $account->namaToko,
                'setup' => true
            ]
        );
        return response($rs);
    }

    /**
     * Store setup.
     *
     * Setup store after register
     *
     * @Post("/setup/{id}")
     * @Versions({"v1"})
     * @Parameter("id", description="account id that receive after register",type="integer",required=true),
     * @Transaction({
     *     @Request({"shop_name","owner_name","address","telp","country_id","province_id",
     *     "city_id","subdistrict_id","post_code","category_id"},
     *     headers={"Authorization" : "bearer your-token-here"}),
     *     @Response(422,body={"The shop name field is required.","The owner name field is required.","The address field is required.","The telp field is required.","The country id field is required.","The province id field is required.","The city id field is required.","The subdistrict id field is required.","The post code field is required.","The category id field is required."}),
     *
     * })
     *
     *
     */
    public function setup(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'shop_name' => 'required',
            'owner_name' => 'required',
            'address' => 'required',
            'telp' => 'required',
            'country_id' => 'required|int',
            'province_id' => 'required|int',
            'city_id' => 'required|int',
            'subdistrict_id' => 'required|int',
            'post_code' => 'required|int',
            'category_id' => 'required|int',
        ]);

        if ($validator->fails()) {

            return $this->response->error(implode(',',$validator->errors()->all()), 422);
        }
        $account = Account::find($id);

        $account->namaTokoLengkap = $request->get('shop_name');
        $account->nama = $request->get('owner_name');
        $account->alamat = $request->get('address');
        $account->telp = $request->get('telp');
        $account->negara = $request->get('country_id');
        $account->provinsi = $request->get('province_id');
        $account->kecamatan = $request->get('subdistrict_id');
        $account->kota = $request->get('city_id');
        $account->status = 1;
        $account->kodeKeamanan = str_random(32);
        $account->kategori = $request->get('category_id');
        $account->verification_code = str_random(32);
        $account->verified = 0;
        $account->save();

        //save default pengaturan toko
        $setting = new Setting([
            'nama' => $request->get('shop_name'),
            'logo' => 'default-logo.png',
            'mataUang' => '1',
            'telepon' => $request->get('telp'),
            'alamat' => $request->get('address'),
            'negara' => $request->get('country_id'),
            'provinsi' => $request->get('province_id'),
            'kota' => $request->get('city_id'),
            'kecamatan' => $request->get('subdistrict_id'),
            'kodepos' => $request->get('post_code'),
            'kotaAsal' => Subdistrict::find($request->get('subdistrict_id'))->nama,
            'email' => $account->email,
            'emailAdmin' => $account->email,
            'bahasa' => 'id',
            'invoice' => '#',
            'fb' => 'http://facebook.com/jarvisstore',
            'tw' => 'http://twitter.com/jarvis_store',
            'statusEkspedisi' => '1',
            'statusApi' => '1',
            'keteranganEkspedisi' => '',
            'apiKey' => '3f78db529a09d0c68668d3e4e8f82352',
            'layananEkspedisi' => 'oke;reg;ss;yes;',
            'checkoutType' => 1
        ]);
        $account->setting()->save($setting);

        Queue::connection('sqs_jarvis')->push('App\Queue\RecursiveCopyService', array(
            'id' => $account->id,
        ));
        Queue::connection('sqs_jarvis')->push('App\Queue\DatabaseSeederService', array(
            'id' => $account->id,
        ));

        $fractal = new Manager();
        $resource2 = new Fractal\Resource\Item($this->jwt->user(), new UserTransformers);
        if ($this->jwt->user()->account->status == 0) {
            $account = [
                'id' => $this->jwt->user()->account->id,
                'shop_name' => $this->jwt->user()->account->namaToko,
                'setup' => true
            ];
        } else {

            $resource = new Fractal\Resource\Item($this->jwt->user()->account, new AccountTransformers);
            $account = $fractal->createData($resource)->toArray();
        }
        return response()->json([
            'user' => $fractal->createData($resource2)->toArray(),
            'account' => $account
        ]);
    }


    /**
     * Install Theme.
     *
     * Setup store after register
     *
     * @Post("/install-theme/{id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("id", description="Theme id"),
     * })
     * @Transaction({
     *     @Request(
     *     headers={"Authorization" : "bearer your-token-here"}),
     *     @Response(200,body={
    "success": true,
    "message": "Successfully install the theme .",
    "storage": 0
    }),
     *
     * })
     *
     *
     */
    public function installTheme(Request $request, $id)
    {
        //check akun status 1/3 for free
        $account = $this->jwt->user()->account;
        $total_tema = AccountTheme::where('akunId', $account->id)->count();
        if (($account->status == 1 || $account->status == 3) && ($total_tema >= 2)) {
            return $this->response->error(trans("message.admin.set_template"),'422');
        }

        $theme = JarvisTheme::find($id);
        if (!$theme) {
            return $this->response->error(trans("message.admin.set_template"), '422');
        }
        AccountTheme::where('akunId', $account->id)
            ->update(array('status' => 0));
        $tema = new AccountTheme;
        $tema->themeId = $id;
        $tema->status = 1;
        $tema->akunId = $account->id;
        $tema->lastAdd = date('Y-m-d H:m:s');
        $tema->code = 'FR33';
        $tema->save();

        //queu to install theme
        Queue::connection('sqs_jarvis')->push('App\Queue\InstallThemeService', array(
            'id' => $account->id,
            'theme_id' => $tema->id
        ));

        $usage = 0;//round(sizeFormat($toko->used_storage) / $this->akun->paket->storage * 100);

        return $this->response->array([
            'success' => true,
            'message' => trans("message.success.install-theme"),
            'storage' => $usage
        ]);
    }

    /**
     * Forget Password.
     *
     * Forget password
     *
     * @Post("/forget-password")
     * @Versions({"v1"})
     * @Transaction({
     *     @Request(
     *     body={"email" : "user email address"}),
     *     @Response(200,body={
    "success": true,
    "message": "Successfully install the theme .",
    "storage": 0
    }),
     *
     * })
     *
     *
     */
    public function forgetPassword(Request $request)
    {
        $email = $request->get('email');
        //check apakah email ada di db
        $user = User::where('email','=',$email)->whereRaw('(tipe=1 or tipe=2)')->first();
        if($user)
        {
            $resetCode = $user->getResetPasswordCode();

            $this->dispatch(new SendResetPasswordEmail(array(
                'reset_code' => $resetCode,
                'to' => $user->email,
                'id' => $user->akunId,
            )));

            $message = 'Sebuah email berisi link untuk mereset password sudah di kirim ke email kamu. Silakan cek inbox/spam folder di email kamu.';
            return $this->response->message($message,200);
        }

        $message = 'Email tersebut tidak ditemukan di sistem kami.';
        return $this->response->error($message,404);
    }

    protected function sendEmail($account, $password, $activationCode)
    {
        $this->dispatch(new SendEmailRegistrationJob(array(
            'email' => $account->email,
            'subject' => '',
            'namaToko' => $account->namaToko,
            'password' => $password,
            'alamatJarvis' => $account->alamatJarvis,
            'verification_code' => $account->verification_code,
            'to' => $account->email
        )));
        $this->dispatch(new VerificationAccountJob(array(
            'email' => $account->email,
            'subject' => '',
            'namaToko' => $account->namaToko,
            'password' => $password,
            'alamatJarvis' => $account->alamatJarvis,
            'verification_code' => $activationCode,
            'to' => $account->email
        )));

    }
}
