<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/24/17
 * Time: 6:30 PM
 */

namespace App\Http\Controllers;
use App\Http\Transformers\ShopCategoryTransformers;
use App\ShopCategory;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

/**
 * @Resource("Shop Category",uri="/shop-categories")
 */

class JarvisStoreCategoryController extends Controller
{
    use Helpers;

    /**
     * Display all shop category.
     *
     * Get a JSON representation of all the shop category.
     *
     * @Get("/")
     * @Versions({"v1"})
     *
     */
    public function index(Request $request)
    {
        $categories = ShopCategory::all();
        return $this->response->collection($categories, new ShopCategoryTransformers());
    }

}