<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/28/17
 * Time: 3:24 PM
 */

namespace App\Http\Controllers;



use App\Http\Transformers\ShopCategoryTransformers;
use App\ShopCategory;
use Dingo\Api\Routing\Helpers;

class ShopCategoryController extends Controller
{
    use Helpers;
    /**
     * Display jarvis shop category.
     *
     * Get a JSON representation of all jarvis shop category.
     *
     * @Get("")
     * @Versions({"v1"})
     */
    public function index()
    {
        $categories = ShopCategory::all();
        return $this->response->collection($categories, new ShopCategoryTransformers());
    }

}