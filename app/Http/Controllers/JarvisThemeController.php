<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/24/17
 * Time: 6:30 PM
 */

namespace App\Http\Controllers;
use App\Http\Transformers\JarvisThemeTransformers;
use App\JarvisTheme;
use Dingo\Api\Http\Request;
use Dingo\Api\Routing\Helpers;


/**
 * @Resource("Jarvis Theme",uri="/jarvis-themes")
 */
class JarvisThemeController extends Controller
{
    use Helpers;

    /**
     * Display all jarvis theme.
     *
     * Get a JSON representation of all the jarvis theme.
     *
     * @Get("?type=")
     * @Versions({"v1"})
     * @Parameters({
     *  @Parameter("type", description="theme type, 1 for free, 2 for paid", type="integer"),
     * })
     */
    public function index(Request $request)
    {
        if($request->has('type'))
        {
            $themes = JarvisTheme::where('tipe',$request->get('type'))->get();
        }else{
            $themes = JarvisTheme::all();
        }
        return $this->response->collection($themes, new JarvisThemeTransformers());
    }
}