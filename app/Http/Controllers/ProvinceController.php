<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/24/17
 * Time: 6:18 PM
 */

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Http\Transformers\ProvinceTransformers;
use App\Province;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

/**
 * @Resource("Province",uri="/provinces")
 */
class ProvinceController extends Controller
{
    use Helpers;

    /**
     * Display all provinces.
     *
     * Get a JSON representation of all the province.
     *
     * @Get("?country_id={id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("country_id", description="country id"),
     * })
     */

    public function index(Request $request)
    {
        if($request->has('country_id'))
        {
            $provinces = Cache::get('provinces_'.$request->get('country_id'), function () use ($request){
                return Province::where('negaraId',$request->get('country_id'))->orderBy('nama')->get();
            });
        }else{
            $provinces = Cache::get('provinces', function () {
                return Province::orderBy('nama')->get();
            });
        }

        return $this->response->collection($provinces, new ProvinceTransformers());
    }
}