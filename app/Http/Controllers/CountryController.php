<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 7/24/17
 * Time: 6:10 PM
 */

namespace App\Http\Controllers;


use App\Country;
use App\Http\Controllers\Controller;
use App\Http\Transformers\CountryTransformers;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Cache;

/**
 * @Resource("Country",uri="/countries")
 */
class CountryController extends Controller
{
    use Helpers;

    /**
     * Display all country.
     *
     * Get a JSON representation of all the products.
     *
     * @Get("/")
     * @Versions({"v1"})
     *
     */
    public function index()
    {
        $countries = Cache::get('countries', function () {
            return Country::orderBy('nama')->get();
        });
        return $this->response->collection($countries, new CountryTransformers());
    }
}