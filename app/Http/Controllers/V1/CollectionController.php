<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 8/16/17
 * Time: 3:01 PM
 */

namespace App\Http\Controllers\V1;

use App\Collection;
use App\Http\Transformers\CollectionTransformer;
use Dingo\Api\Routing\Helpers;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class CollectionController extends BaseController
{
    use Helpers;

    private $account;

    public function __construct(Request $request)
    {
        $this->account = app()->make('App\Account');
    }

    public function index()
    {
        $collections = Collection::where('akunId',$this->account->id)->get();

        return $this->response->collection($collections, new CollectionTransformer());
    }


}