<?php

namespace App\Http\Controllers\V1;

use App\Http\Transformers\ProductTransformer;
use App\Product;
use App\ProductOption;
use App\ProductSku;
use Validator;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Dingo\Api\Routing\Helpers;

/**
 * @Resource("Products",uri="/products")
 */
class ProductController extends BaseController
{
    use Helpers;

    /**
     * Display all products.
     *
     * Get a JSON representation of all the products.
     *
     * @Get("?bestseller=true,newproduct=true,query='',category='',collection='',limit=15")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("bestseller", description="filter product based on bestseller", default=false),
     *      @Parameter("newproduct", description="filter product based on newproduct"),
     *      @Parameter("query", description="search product by name"),
     *      @Parameter("category", description="search product by category name"),
     *      @Parameter("collection", description="search product by collection name"),
     *      @Parameter("limit", description="how many product return, default is 15"),
     * })
     * @Response(200, body={"data":{{"id":93886,"name":"Sepatu Duo","short_description":"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type...","description":"<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<\/p>","vendor":"Conversa","sell_price":10000,"mitra_price":60000,"discount_price":150000,"barcode":"8781238145","weight":300,"stock":4,"image1":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/5.gif","image2":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/5.gif","image3":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/5.gif","image4":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/5.gif","tags":"sepatu,colorful","dmt":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.","kmt":"keyword1,keyword2","visibility":1,"bestseller":0,"new_product":0,"url":"http:\/\/example.jstore.co\/produk\/sepatu-duo","options_sku":{"data":{}},"options":{"data":{}}},{"id":93906,"name":"adsad","short_description":"<p>asdad<\/p>","description":"<p>asdad<\/p>","vendor":"asd","sell_price":1,"mitra_price":0,"discount_price":1,"barcode":"","weight":1,"stock":-1,"image1":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/20160427-140323.png","image2":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/adsad-1488187713.jpg","image3":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/adsad-1488187757.jpg","image4":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/adsad-1489742695.jpg","tags":"","dmt":"","kmt":"","visibility":1,"bestseller":0,"new_product":0,"url":"http:\/\/example.jstore.co\/produk\/adsad","category":{"data":{"id":51697,"name":"Kategori 4","slug":"kategori-4","parent":51695,"order":1}},"options_sku":{"data":{}},"options":{"data":{}}},{"id":93885,"name":"Sepatu Hijau","short_description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and...","description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.","vendor":"Gajah Duduks","sell_price":80000,"mitra_price":60000,"discount_price":150000,"barcode":"8781238145","weight":300,"stock":3,"image1":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/4.gif","image2":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/4.gif","image3":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/4.gif","image4":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/4.gif","tags":"sepatu, colorful","dmt":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.","kmt":"keyword1,keyword2","visibility":1,"bestseller":0,"new_product":0,"url":"http:\/\/example.jstore.co\/produk\/sepatu-hijau","options_sku":{"data":{}},"options":{"data":{}}}},"meta":{"pagination":{"total":6,"count":3,"per_page":3,"current_page":1,"total_pages":2,"links":{"next":"http:\/\/api.jarvis.local\/products?page=2"}}}})
     */
    public function index(Request $request)
    {

        $products = Product::where('akunId', $request->user()->account->id)->where('deleted_at', null);

        if ($request->has('bestseller')) {
            $products = $products->where('terlaris', 1);
        }

        if ($request->has('newproduct')) {
            $products = $products->where('produkbaru', 1);
        }

        if ($request->has('query')) {
            $products = $products->where('nama', 'like', '%' . $request->get('query') . '%');
        }

        if ($request->has('category')) {
            $products = $products->whereHas('category', function ($q) use ($request) {
                $q->where('nama', 'like', '%' . $request->get('category') . '%');
            });
        }

        if ($request->has('collection')) {
            $products = $products->whereHas('collection', function ($q) use ($request) {
                $q->where('nama', 'like', '%' . $request->get('collection') . '%');
            });
        }

        $products = $products->paginate($request->get('limit'));

        return $this->response->paginator($products, new ProductTransformer());
    }

    /**
     * Store new products.
     *
     * Api to create new product .
     *
     * @Post("/")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("name", description="product name", required=true),
     *      @Parameter("description", description="Product production", required=true),
     *      @Parameter("category_id", description="Product category", required=true),
     *      @Parameter("sell_price", description="product sell price", required=true),
     *      @Parameter("mitra_price", description="Mitra price, used for mitra member"),
     *      @Parameter("discount_price", description="discount price", required=true),
     *      @Parameter("discount_price", description="discount price", required=true),
     *      @Parameter("weight", description="Product weight in gram", required=true),
     *      @Parameter("stock", description="Product stock", required=true),
     *      @Parameter("barcode", description="Product barcode"),
     *      @Parameter("tags", description="Product tags"),
     *      @Parameter("collections", description="Product collection in array"),
     *      @Parameter("dmt", description="description meta tag"),
     *      @Parameter("kmt", description="keyword meta tag"),
     *      @Parameter("visibility", description="Product visibility, 0 for hide. 1 for show"),
     *      @Parameter("bestseller", description="Product bestseller status, 0 for hide. 1 for show"),
     *      @Parameter("new_product", description="Product new product status, 0 for hide. 1 for show"),
     *      @Parameter("have_options", description="Product has options status, 0 for not have option sku. 1 for have option sku"),
     *      @Parameter("options", description="Product options name in array,ex: Size, Color, etc"),
     *      @Parameter("varians", description="Product varian name for every option name in string separated by coma(,),ex: Xl,L,M,S"),
     *      @Parameter("option_status", description="option status for the combination, 1 for save to db"),
     *      @Parameter("option_1", description="option 1 name"),
     *      @Parameter("option_2", description="option 2 name"),
     *      @Parameter("option_3", description="option 3 name"),
     *      @Parameter("option_price", description="option price"),
     *      @Parameter("option_stock", description="option stock"),
     *      @Parameter("option_barcode", description="option barcode"),
     *
     *
     * })
     * @Response(200, body={"data":{"id":94917,"name":"Product Options","short_description":"Ini product A","description":"Ini product A","vendor":"Converse","sell_price":"100000","mitra_price":"0","discount_price":"120000","barcode":"","weight":"1000","stock":"10","image1":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/image1.jpg","image2":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/image2.jpg","image3":"","image4":"","tags":"","dmt":"","kmt":"","visibility":"0","bestseller":"1","new_product":0,"url":"http:\/\/example.jstore.co\/produk\/product-options","category":{"data":{"id":1,"name":"Wanita","slug":"wanita","parent":0,"order":1}},"options_sku":{"data":{}},"options":{"data":{{"id":7037,"name":"Warna","value":"Merah,Biru,Hijau"}}}}})
     * @Response(422, body={"message":"The name field is required.,The description field is required.","status_code":422})
     */
    public function store(Request $request)
    {
        $account = $request->user()->account;
        $total_product = Product::where('akunId', '=', $account->id)->where('deleted_at', NULL)->count();
        if ($account->status == 1 || $account->status == 3) {
            if ($total_product >= $account->jarvisPacket->total_product) {
                return $this->response->error(trans("message.admin.product.max"), 422);
            }
        }

        $slug = preg_replace('!\s+!', ' ', $request->get('slug'));
        $slug = str_replace(' ', '-', strtolower(trim($slug)));
        $rules = array(
            'slug' => 'required|unique:produk,slug,NULL,id,akunId,' . $account->id
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $same_slug = Product::where('slug', 'like', '%' . $slug . '%')->where('akunId', '=', $account->id)->count();
            if ($slug == '') {
                $slug = preg_replace('!\s+!', ' ', $request->get('name'));
                $slug = str_replace(' ', '-', strtolower(trim($slug)));
            } else {
                $slug = $slug . '' . ($same_slug + 1);
            }

        }
        $rules = [
            'name' => 'required|min:2|max:200',
            'description' => 'required',
            'category_id' => 'required',
            'sell_price' => 'required|integer',
            'discount_price' => 'integer',
            'mitra_price' => 'integer',
            'weight' => 'required|integer',
            'stock' => 'required|integer'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return $this->response->error(implode(',',$validator->errors()->all()),422);
        }

        $collections = '';
        if ($request->get('collections') != '') {
            foreach ($request->get('collections') as $key => $value) {
                $collections = $collections . ',' . $value;
            }
        }

        $product = Product::create([
            'nama' => $request->get('name'),
            'deskripsi' => $request->get('description'),
            'vendor' => $request->get('vendor', ''),
            'hargaJual' => $request->get('sell_price'),
            'hargaMitra' => $request->get('mitra_price'),
            'hargaCoret' => $request->get('discount_price'),
            /*'hargaCustom' => $request->get('custom_price'),*/
            'barcode' => $request->get('barcode', ''),
            'berat' => $request->get('weight'),
            'stok' => $request->get('stock'),
            'tags' => $request->get('tags', ''),
            'koleksiId' => $collections,
            'dmt' => $request->get('dmt', ''),
            'kmt' => $request->get('kmt', ''),
            'keywordSeo' => $request->get('keyword', ''),
            'visibility' => $request->get('visibility', '0'),
            'kategoriId' => $request->get('category_id'),
            'akunId' => $account->id,
            'terlaris' => $request->get('bestseller', 0),
            'produkbaru' => $request->get('new_product', 0),
            'slug' => $slug
        ]);

        $images = $request->get('product_images', []);
        $i = 1;
        foreach ($images as $key => $value) {
            if ($i == 1) {
                $product->gambar1 = $value;
            }
            if ($i == 2) {
                $product->gambar2 = $value;
            }
            if ($i == 3) {
                $product->gambar3 = $value;
            }
            if ($i == 4) {
                $product->gambar4 = $value;
            }
            $i++;
        }
        $product->save();
        if ($request->get('have_options')) {
            $opsiproduk = $request->get('options');
            $varians = $request->get('varians');

            for ($i = 0; $i < count($opsiproduk); $i++) {
                $opsi = new ProductOption();
                $opsi->nama = ($opsiproduk[$i] == 'new_option' ? $request->get('new_option')[$i] : $opsiproduk[$i]);
                $opsi->value = $varians[$i];
                $opsi->produkId = $product->id;
                $opsi->save();
            }
            $opsisku = $request->get('option_status');
            $opsi1 = $request->get('option_1');
            $opsi2 = $request->get('option_2');
            $opsi3 = $request->get('option_3');
            $hargaopsi = $request->get('option_price');
            $stokopsi = $request->get('option_stock');
            $stokproduk = 0; //perhitungan totsl stok produk
            $barcodeopsi = $request->get('option_barcode');
            for ($i = 0; $i < count($opsisku); $i++) {
                if ($opsisku[$i] == 1) {
                    $op = new ProductSku();
                    $op->opsi1 = $opsi1[$i];
                    $op->opsi2 = isset($opsi2[$i]) ? $opsi2[$i] : '';
                    $op->opsi3 = isset($opsi3[$i]) ? $opsi3[$i] : '';
                    $op->harga = $hargaopsi[$i] == '' ? 0 : $hargaopsi[$i];
                    $op->stok = $stokopsi[$i] == '' ? 0 : $stokopsi[$i];
                    $op->barcode = $barcodeopsi[$i] == '' ? 0 : $barcodeopsi[$i];
                    $op->produkId = $product->id;
                    $op->save();
                }
                $stokproduk += $op->stok;
            }
        }
        return $this->response->item($product, new ProductTransformer());
    }

    /**
     * Display 1 product.
     *
     * Get a JSON representation of detail the product.
     *
     * @Get("/{id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("id", description="product id"),
     * })
     */
    public function show(Request $request, $id)
    {

        $product = Product::find($id);

        return $this->response->item($product, new ProductTransformer());
    }

    /**
     * Update product.
     *
     * Api to update product .
     *
     * @Post("/{product_id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("name", description="product name", required=true),
     *      @Parameter("description", description="Product production", required=true),
     *      @Parameter("category_id", description="Product category", required=true),
     *      @Parameter("slug", description="Product slug url", required=true),
     *      @Parameter("sell_price", description="product sell price", required=true),
     *      @Parameter("mitra_price", description="Mitra price, used for mitra member"),
     *      @Parameter("discount_price", description="discount price", required=true),
     *      @Parameter("discount_price", description="discount price", required=true),
     *      @Parameter("weight", description="Product weight in gram", required=true),
     *      @Parameter("stock", description="Product stock", required=true),
     *      @Parameter("barcode", description="Product barcode"),
     *      @Parameter("tags", description="Product tags"),
     *      @Parameter("collections", description="Product collection in array"),
     *      @Parameter("dmt", description="description meta tag"),
     *      @Parameter("kmt", description="keyword meta tag"),
     *      @Parameter("visibility", description="Product visibility, 0 for hide. 1 for show"),
     *      @Parameter("bestseller", description="Product bestseller status, 0 for hide. 1 for show"),
     *      @Parameter("new_product", description="Product new product status, 0 for hide. 1 for show"),
     *      @Parameter("product_images", description="array of product image, index 0 - 3 for image 1 - 4"),
     *
     *
     *
     * })
     * @Response(200, body={"data":{"id":94917,"name":"Product Options","short_description":"Ini product A","description":"Ini product A","vendor":"Converse","sell_price":"100000","mitra_price":"0","discount_price":"120000","barcode":"","weight":"1000","stock":"10","image1":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/image1.jpg","image2":"https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/image2.jpg","image3":"","image4":"","tags":"","dmt":"","kmt":"","visibility":"0","bestseller":"1","new_product":0,"url":"http:\/\/example.jstore.co\/produk\/product-options","category":{"data":{"id":1,"name":"Wanita","slug":"wanita","parent":0,"order":1}},"options_sku":{"data":{}},"options":{"data":{{"id":7037,"name":"Warna","value":"Merah,Biru,Hijau"}}}}})
     * @Response(422, body={"message":"The name field is required.,The description field is required.","status_code":422})
     */
    public function update(Request $request, $id)
    {
        $account = $request->user()->account;


        $slug = preg_replace('!\s+!', ' ', $request->get('slug'));
        $slug = str_replace(' ', '-', strtolower(trim($slug)));
        $rules = array(
            'slug' => 'required|unique:produk,slug,NULL,id,akunId,' . $account->id
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $same_slug = Product::where('slug', 'like', '%' . $slug . '%')->where('akunId', '=', $account->id)->count();
            if ($slug == '') {
                $slug = preg_replace('!\s+!', ' ', $request->get('name'));
                $slug = str_replace(' ', '-', strtolower(trim($slug)));
            } else {
                $slug = $slug . '' . ($same_slug + 1);
            }

        }
        $rules = [
            'name' => 'required|min:2|max:200',
            'description' => 'required',
            'category_id' => 'required',
            'sell_price' => 'required|integer',
            'mitra_price' => 'integer',
            'discount_price' => 'integer',
            'weight' => 'required|integer',
            'stock' => 'required|integer'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return $this->response->error(implode(',',$validator->errors()->all()),422);
        }
        $collections = '';
        if ($request->get('collections') != '') {
            foreach ($request->get('collections') as $key => $value) {
                $collections = $collections . ',' . $value;
            }
        }
        $product = Product::find($id);
        $product->update([
            'nama' => $request->get('name'),
            'deskripsi' => $request->get('description'),
            'vendor' => $request->get('vendor', ''),
            'hargaNet' => $request->get('nett_price','0'),
            'hargaJual' => $request->get('sell_price'),
            'hargaMitra' => $request->get('mitra_price', 0),
            'hargaCoret' => $request->get('discount_price', 0),
            /*'hargaCustom' => $request->get('custom_price'),*/
            'barcode' => $request->get('barcode', ''),
            'berat' => $request->get('weight'),
            'stok' => $request->get('stock'),
            'tags' => $request->get('tags', ''),
            'koleksiId' => $collections,
            'dmt' => $request->get('dmt', ''),
            'kmt' => $request->get('kmt', ''),
            'keywordSeo' => $request->get('keyword', ''),
            'visibility' => $request->get('visibility', '0'),
            'kategoriId' => $request->get('category'),
            'akunId' => $account->id,
            'terlaris' => $request->get('bestseller', 0),
            'produkbaru' => $request->get('new_product', 0),
            'slug' => $slug
        ]);

        $images = $request->get('product_images', []);
        $i = 1;
        foreach ($images as $key => $value) {
            if ($i == 1) {
                $product->gambar1 = $value;
            }
            if ($i == 2) {
                $product->gambar2 = $value;
            }
            if ($i == 3) {
                $product->gambar3 = $value;
            }
            if ($i == 4) {
                $product->gambar4 = $value;
            }
            $i++;
        }
        $product->save();

        if($request->get('has_options')){
            $opsiproduk = $request->get('options');
            $opsibaru = $request->get('new_option');
            $varian = $request->get('varians');
            $opsi_produk = array();
            $j = 0;

            for($i=0; $i<count($opsiproduk); $i++){
                if (!empty($opsibaru[$i])) {
                    array_push($opsi_produk, $opsibaru[$i]);
                }

                if ($opsiproduk[$i]=='opsibaru') {
                    $new_opsi_produk = $opsi_produk[$j];
                    $j++;
                } else {
                    $new_opsi_produk = $opsiproduk[$i];
                }

                $opsi = new ProductOption();
                $opsi->nama = $new_opsi_produk;
                $opsi->value = $varian[$i];
                $opsi->produkId = $product->id;
                if (!empty($opsiproduk[$i])) {
                    $opsi->save();
                }
            }
            $opsisku = $request->get('option_status');
            $opsi1 = $request->get('option_1');
            $opsi2 = $request->get('option_2');
            $opsi3 = $request->get('option_3');
            $hargaopsi = $request->get('option_price');
            $stokopsi = $request->get('option_stock');
            $stokproduk = 0;
            $barcodeopsi = $request->get('option_barcode');
            for($i=0; $i<count($opsisku); $i++){
                if($opsisku[$i]==1){
                    $op = new Opsisku;
                    $op->opsi1 = $opsi1[$i];
                    $op->opsi2 = $opsi2[$i];
                    $op->opsi3 = $opsi3[$i];
                    $op->harga = $hargaopsi[$i]==NULL?'0':$hargaopsi[$i];
                    $op->stok = $stokopsi[$i]==NULL?'0':$stokopsi[$i];
                    $op->barcode = $barcodeopsi[$i]==NULL?'':$barcodeopsi[$i];
                    $op->produkId = $product->id;
                    $op->save();
                }
                $stokproduk += $op->stok;
            }
        }

        return $this->response->item($product, new ProductTransformer());
    }

    public function destroy($id)
    {
        Product::destroy($id);
        return $this->response->error(trans('message.success.delete', ['obj' => 'product']), 200);
    }
}
