<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 12/20/16
 * Time: 11:42 AM
 */

namespace App\Http\Controllers\V1;

use App\Http\Transformers\OrderTransformers;
use App\Order;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Resource("Orders",uri="/orders")
 */

class OrderController extends BaseController
{
    use Helpers;

    /**
     * Display all order.
     *
     * Get a JSON representation of order.
     *
     * @Get("/")
     * @Versions({"v1"})
     */
    public function index(Request $request)
    {
        $orders = Order::where('akunId',$request->user()->akunId)->paginate();

        return $this->response()->item($orders,new OrderTransformers());

    }
    /**
     * Display detail order.
     *
     * Get a JSON representation of order.
     *
     * @Get("/{order_code}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("order_code", description="order code"),
     * })
     */
    public function show(Request $request,$order_code)
    {
        $order = Order::where('kodeOrder',$order_code)->first();
        if($order)
        {
            return $this->response()->item($order,new OrderTransformers());
        }

        throw new NotFoundHttpException('order not found');

    }
}