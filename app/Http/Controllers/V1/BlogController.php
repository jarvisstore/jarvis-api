<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 12/20/16
 * Time: 12:06 PM
 */

namespace App\Http\Controllers\V1;

use App\Blog;
use App\Http\Transformers\BlogTranformer;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Dingo\Api\Routing\Helpers;
use Validator;


/**
 * @Resource("Blogs",uri="/blogs")
 */
class BlogController extends BaseController
{
    use Helpers;

    /**
     * Display all blog.
     *
     * Get a JSON representation of all the blog.
     *
     * @Get("?query=''")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("query", description="search blog by name"), 
     * })
     *  @Response(200,body={"data":{{"id":22135,"name":"sample blog title","tags":"blogs, sample","content":"smaple blog content","url":"http:\/\/testingdong.jstore.co\/blog\/tips-dan-trik\/sample-blog-title","status":1,"created_at":{"date":"-0001-11-30 00:00:00.000000","timezone_type":3,"timezone":"UTC"},"updated_at":{"date":"-0001-11-30 00:00:00.000000","timezone_type":3,"timezone":"UTC"}},{"id":22136,"name":"sample blog title","tags":"blogs, sample","content":"smaple blog content","url":"http:\/\/testingdong.jstore.co\/blog\/tips-dan-trik\/sample-blog-title","status":1,"created_at":{"date":"-0001-11-30 00:00:00.000000","timezone_type":3,"timezone":"UTC"},"updated_at":{"date":"-0001-11-30 00:00:00.000000","timezone_type":3,"timezone":"UTC"}}}})
     */
    public function index(Request $request)
    {
        $this->account = app()->make('App\Account');
        $blogs = Blog::where('akunId',$this->account->id);

        if($request->has('query')){
            $blogs->where('judul','like','%'.$request->get('query').'%');
        }

        $blogs = $blogs->take(5)->get();

        return $this->response()->collection($blogs ,new BlogTranformer());
    }

    /**
     * Display one blog.
     *
     * Get a JSON representation of one blog.
     *
     * @Get("/{id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("id", description="blog id"),
     * })
     * @Transaction({
     *     @Response(200,body={"data":{"id":22135,"name":"sample blog title","tags":"blogs, sample","content":"smaple blog content","url":"http:\/\/testingdong.jstore.co\/blog\/tips-dan-trik\/sample-blog-title"}})
     * })
     */

    public function show(Request $request,$id)
    {
        $blog = Blog::find($id);

        return $this->response()->item($blog ,new BlogTranformer());
    }


    /**
     * Create one blog.
     *
     * Create new blog.
     *
     * @Post("")
     * @Versions({"v1"})
     * @Transaction({
     *     @Request({"title","content","category_id","slug","tags","status"}),
     *     @Response(200,body={"data":{"id":22137,"name":"sample blog title","tags":"blogs, sample","content":"smaple blog content","url":"http:\/\/testingdong.jstore.co\/blog\/tips-dan-trik\/sample-blog-title","status":null,"created_at":{"date":"-0001-11-30 00:00:00.000000","timezone_type":3,"timezone":"UTC"},"updated_at":{"date":"-0001-11-30 00:00:00.000000","timezone_type":3,"timezone":"UTC"}}}),
     *     @Response(422,body={"message":"The title field is required.,The slug field is required.","status_code":422})
     * })
     *
     */

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'content' => 'required',
            'category_id' => 'required',
            'slug' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {

            return $this->response->error(implode(',',$validator->errors()->all()), 422);
        }
        $blog = Blog::create([
            'judul' => $request->get('title'),
            'isi' => $request->get('content'),
            'author' => $request->user()->id,
            'tags' => $request->get('tags',''),
            'kategoriBlogId' => $request->get('category_id'),
            'slug' => $request->get('slug',str_slug($request->get('title'))),
            'akunId' => $request->user()->akunId,
            'status' => $request->get('status',0)
        ]);

        return $this->response()->item($blog ,new BlogTranformer());
    }

    /**
     * Update one blog.
     *
     * @Put("/{id}")
     * @Versions({"v1"})
     * @Transaction({
     *     @Request({"title","content","category_id","slug","tags","status"}),
     *     @Response(200,body={"data":{"id":22137,"name":"sample blog title","tags":"blogs, sample","content":"smaple blog content","url":"http:\/\/testingdong.jstore.co\/blog\/tips-dan-trik\/sample-blog-title","status":null,"created_at":{"date":"-0001-11-30 00:00:00.000000","timezone_type":3,"timezone":"UTC"},"updated_at":{"date":"-0001-11-30 00:00:00.000000","timezone_type":3,"timezone":"UTC"}}}),
     *     @Response(422,body={"message":"The title field is required.,The slug field is required.","status_code":422})
     * })
     *
     */

    public function update(Request $request,$id)
    {
        $rules = [
            'title' => 'required|max:255',
            'content' => 'required',
            'category_id' => 'required',
            'slug' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {

            return $this->response->error(implode(',',$validator->errors()->all()), 422);
        }

        $blog = Blog::find($id);
        $blog->update([
            'judul' => $request->get('title'),
            'isi' => $request->get('content'),
            'author' => $request->user()->id,
            'tags' => $request->get('tags',''),
            'kategoriBlogId' => $request->get('category_id'),
            'slug' => $request->get('slug',str_slug($request->get('title'))),
            'akunId' => $request->user()->akunId,
            'status' => $request->get('status')
        ]);

        return $this->response()->item($blog ,new BlogTranformer());
    }

    /**
     * Delete one blog.
     *
     * @Delete("/{id}")
     * @Versions({"v1"})
     * @Transaction({
     *     @Response(200,body={"message":"Successfully delete Blog","status_code":200})
     * })
     */

    public function destroy($id)
    {
        Blog::destroy($id);

        return $this->response()->message(trans('message.success.delete',['obj' => 'Blog']));
    }
}