<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 8/16/17
 * Time: 2:22 PM
 */

namespace App\Http\Controllers\V1;


use App\Testimonial;
use App\Http\Transformers\TestimonialsTransformer;
use Dingo\Api\Contract\Http\Request;
use Dingo\Api\Routing\Helpers;
use Validator;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @Resource("Testimonial",uri="/testimonial")
 */
class TestimonialsController extends BaseController
{
    use Helpers;

    /**
     * Get all testimonial.
     *
     * Get a JSON representation of all the testimonial.
     *
     * @Get("/")
     * @Versions({"v1"})
     */
    public function index(Request $request)
    {
        $testimonials = Testimonial::where('akunId', $request->user()->account->id)->get();
        return $this->response->collection($testimonials, new TestimonialsTransformer());
    }

    /**
     * Display 1 testimonial.
     *
     * Get a JSON representation of 1 the testimonial.
     *
     * @Get("/{id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("id", description="testimony id"),
     * })
     */
    public function show($id)
    {
        $testimony = Testimonial::find($id);
        return $this->response->item($testimony, new TestimonialsTransformer());
    }

    /**
     * Store testimonial.
     *
     * add new testimonial
     *
     * @Post("/")
     * @Versions({"v1"})
     *
     * @Transaction({
     *     @Request({"name","content","visibility"}),
     * })

     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'content' => 'required',
            'visibility' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return $this->response->error(implode(',',$validator->errors()->all()), 422);
        }

        $testimonial = Testimonial::create([
            'nama' => $request->get('name'),
            'isi' => $request->get('content',''),
            'visibility' => $request->get('visibility',0),
            'akunId' => $request->user()->account->id,
        ]);
        return $this->response->item($testimonial, new TestimonialsTransformer());
    }

    /**
     * Update Testimonial.
     *
     * update testimonial
     *
     * @Put("/id")
     * @Versions({"v1"})
     *
     * @Transaction({
     *     @Request({"name","content","visibility"}),
     * })
     * @Parameters({
     *      @Parameter("id", description="testimonial id"),
     * })

     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'content' => 'required',
            'visibility' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(),422);
        }
        $testimonial = Testimonial::find($id);

        $testimonial->update([
            'nama' => $request->get('name'),
            'isi' => $request->get('content',''),
            'visibility' => $request->get('visibility'),
        ]);
        return $this->response->item($testimonial, new TestimonialsTransformer());
    }

    /**
     * Delete Testimonial.
     *
     * delete testimonial
     *
     * @Delete("/id")
     * @Versions({"v1"})
     *
     * @Parameters({
     *      @Parameter("id", description="testimonial id"),
     * })

     */
    public function destroy($id)
    {
        Testimonial::destroy($id);
        return $this->response->message(trans('message.success.delete',['obj' => 'testimonial']),200);
    }
}