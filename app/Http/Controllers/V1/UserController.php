<?php

namespace App\Http\Controllers\V1;

use App\Http\Transformers\ProductTransformer;
use App\Http\Transformers\UserTransformers;
use App\Product;
use App\ProductOption;
use App\ProductSku;
use App\User;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Dingo\Api\Routing\Helpers;

/**
 * @Resource("Users",uri="/users")
 */
class UserController extends BaseController
{
    use Helpers;

    /**
     * Display shop users.
     *
     * Get a JSON representation of all users.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("type", description="user type, 0 = guest, 1 = member, 2 = mitra"),
     * })
     */
    public function index(Request $request)
    {

        $users = User::where('akunId', $request->user()->account->id);

        if ($request->has('type')) {
            $users = $users->where('tipe', 1);
        }

        $users = $users->paginate();

        return $this->response->paginator($users, new UserTransformers());
    }

    /**
     * Add new shop users.
     *
     * add new shop user.
     *
     * @Post("/")
     * @Versions({"v1"})
     * @Transaction({
     *     @Request({"name","email","password","address","country_id","province_id","city_id","telp",
     *     "postcode","type","birth_of_date"}),
     * })
     */
    public function store(Request $request)
    {
        $rules = [
            'name'      => 'required|max:80',
            'email'     => 'required',
            'password'  => 'confirmed',
            'address'  => 'required',
            'country_id'    => 'required',
            'province_id'    => 'required',
            'city_id'    => 'required',
            'telp'      => 'numeric|digits_between:9,15',
            'postcode'      => 'numeric|digits_between:5,6',
            'type'      => 'required',
        ];

        $validation = Validator::make($request->all(),$rules);
        if($validation->fails()){
            return $this->response->error(implode(',',$validation->errors()->all()),422);
        }
        $data = array(
            'nama' => strip_tags($request->get('name')),
            'email'    => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'kodepos' => strip_tags($request->get('postcode')),
            'perusahaan' => strip_tags($request->get('company_name','')),
            'telp' => strip_tags($request->get('telp')),
            'alamat' => strip_tags($request->get('address','')),
            'negara' => $request->get('country_id'),
            'provinsi' => $request->get('province_id'),
            'kota' => $request->get('city_id'),
            'tglLahir' => $request->get('date_of_birth',''),
            'catatan' => strip_tags($request->get('note','')),
            'tags' => $request->get('tags',''),
            'tipe' => $request->get('type'),
            'tanggalMasuk' => date("Y-m-d"),
            'akunId' => $request->user()->akunId,
            'activated' => 1
        );
        $user = User::create($data);

        return $this->response->item($user, new UserTransformers());
    }

    /**
     * Display 1 user.
     *
     * Get a JSON representation of detail the user.
     *
     * @Get("/{id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("id", description="user id"),
     * })
     */
    public function show(Request $request, $id)
    {
        $user = User::find($id);
        return $this->response->item($user, new UserTransformers());
    }

    /**
     * update users.
     *
     * update shop user.
     *
     * @Put("/{id}")
     * @Versions({"v1"})
     * @Transaction({
     *     @Request({"name","email","password","address","country_id","province_id","city_id","telp",
     *     "postcode","type","birth_of_date"}),
     * })
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'      => 'required|max:80',
            'email'     => 'required',
            'address'  => 'required',
            'country_id'    => 'required',
            'province_id'    => 'required',
            'city_id'    => 'required',
            'phone'      => 'numeric|digits_between:9,15',
            'postcode'      => 'numeric|digits_between:5,6',
            'type'      => 'required',
        ];

        if($request->get('password') != '')
        {
            array_add($rules,'password','confirmed');
        }

        $validation = Validator::make($request->all(),$rules);
        if($validation->fails()){
            return $this->response->error(implode(',',$validation->errors()->all()),422);
        }
        $data = array(
            'nama' => strip_tags($request->get('name')),
            'email'    => $request->get('email'),
            'kodepos' => strip_tags($request->get('postcode')),
            'perusahaan' => strip_tags($request->get('company_name','')),
            'telp' => strip_tags($request->get('telp')),
            'alamat' => strip_tags($request->get('address','')),
            'negara' => $request->get('country_id'),
            'provinsi' => $request->get('province_id'),
            'kota' => $request->get('city_id'),
            'tglLahir' => $request->get('date_of_birth',''),
            'catatan' => strip_tags($request->get('note','')),
            'tags' => $request->get('tags',''),
            'tipe' => $request->get('type'),
            'tanggalMasuk' => date("Y-m-d"),
            'akunId' => $request->user()->akunId,
            'activated' => 1
        );
        if($request->get('password') != '')
        {
            array_add($data,'password',Hash::make($request->get('password')));
        }

        $user = User::find($id);
        $user->update($data);

        return $this->response->item($user, new UserTransformers());
    }

    /**
     * Delete users.
     *
     * update shop user.
     *
     * @Delete("/{id}")
     * @Versions({"v1"})

     */
    public function destroy($id)
    {
        User::destroy($id);
        return $this->response->error(trans('message.success.delete', ['obj' => 'user']), 200);
    }
}
