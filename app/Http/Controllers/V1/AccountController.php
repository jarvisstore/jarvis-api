<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 1/11/17
 * Time: 11:35 AM
 */

namespace App\Http\Controllers\V1;
use App\Http\Transformers\AccountTransformers;
use Laravel\Lumen\Routing\Controller as BaseController;
use Dingo\Api\Routing\Helpers;
/**
 * @Resource("Account",uri="/account")
 */
class AccountController extends BaseController
{
    use Helpers;

    public $account;
    /**
     * Display shop information.
     *
     * Get a JSON representation of account
     *
     * @Get("/")
     * @Versions({"v1"})
     */
    public function index()
    {
        $this->account = app()->make('App\Account');
        return $this->response()->item($this->account,new AccountTransformers());
    }
}