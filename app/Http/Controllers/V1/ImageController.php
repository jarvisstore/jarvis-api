<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 9/20/17
 * Time: 3:10 PM
 */

namespace App\Http\Controllers\V1;

use App\AWS;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
/**
 * @Resource("Image",uri="/images")
 */
class ImageController extends BaseController
{
    use Helpers;
    /**
     * Store new image
     *
     * Api to upload image.
     *
     * @Post("/")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("file", description="product name", required=true),
     *
     *
     * })
     * @Response(200, body={"data":{"filename":"20170926-131620.jpg"}})
     */
    public function store(Request $request)
    {
        $aws = new AWS();
        $rs = $aws->uploadImageProduct($request->file('file'));
        return response()->json([
           'data' => [
               'filename' => $rs['filename']
           ]
        ]);
    }

}