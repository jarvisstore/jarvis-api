<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 8/25/17
 * Time: 9:27 AM
 */

namespace App\Http\Controllers\V1;

use App\Discount;
use App\Http\Transformers\DiscountTransformers;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Validator;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @Resource("Discount",uri="/discounts")
 */
class DiscountController extends BaseController
{
    use Helpers;
    private $account;

    /**
     * Get all discount.
     *
     * Get a JSON representation of all the category.
     *
     * @Get("/")
     * @Versions({"v1"})
     */
    public function index(){

        $this->account = app()->make('App\Account');
        $discounts = Discount::account($this->account)->get();
        return $this->response->collection($discounts, new DiscountTransformers());

    }

    /**
     * Get 1 category.
     *
     * Get a JSON representation of 1 discount.
     *
     * @Get("/{id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("id", description="discount id"),
     * })
     */
    public function show($id){
        $discount = Discount::find($id);
        return $this->response->item($discount, new DiscountTransformers());
    }

    /**
     * Store Discount.
     *
     * add new product Discount
     *
     * @Post("/")
     * @Versions({"v1"})
     *
     * @Transaction({
     *     @Request({"code","total","discount","type","minimal_order","start_date","end_date","product_id",
     *     "category_id","collection_id","status"}),
     * })
     * */

    public function store(Request $request){

        $rules = array(
            'code' => 'required|unique:diskon,kode',
            'total' => 'required|integer',
            'discount' => 'required|integer',
            'type' => 'required|integer',
            'minimal_order' => 'required|integer|min:'.$request->get('discount'),
            'start_date' => 'required',
            'end_date' => 'required',
            'product_id' => 'required',
            'category_id' => 'required',
            'collection_id' => 'required',
            'status' => 'required',
        );
        if($request->get('type')=='2'){
            $rules['discount'] = 'required|integer|between:0,100';
        }

        $validation = Validator::make($request->all(),$rules);
        if($validation->fails()){
             return $this->response->error(implode(';',$validation->errors()->all()),422);
        }

        $discount = new Discount();
        $discount->kode = strip_tags($request->get('code'));
        $discount->jenisPotongan = $request->get('type');
        $discount->besarPotongan = strip_tags($request->get('discount'));
        $discount->tglMulai = ($request->get('start_date'));
        $discount->tglBerakhir = ($request->get('end_date'));
        $discount->jumlah = strip_tags($request->get('total'));
        $discount->minBuy = strip_tags($request->get('minimal_order'));
        $discount->produkId = $request->get('product_id');
        $discount->kategoriId = $request->get('category_id');
        $discount->koleksiId = $request->get('collection_id');
        $discount->akunId = $request->user()->akunId;
        $discount->status = $request->get('status');
        $discount->save();
        if($discount){
            return $this->response->message(trans('message.success.create',['obj' => 'Discount']));
        }else{
            return $this->response->error(trans('message.error.create',['obj' => 'Discount']),422);
        }


    }

    /**
     * Update Discount.
     *
     * update product Discount
     *
     * @Put("/{id}")
     * @Versions({"v1"})
     *
     * @Parameters({
     *      @Parameter("id", description="discount id"),
     * })
     *
     * @Transaction({
     *     @Request({"code","total","discount","type","minimal_order","start_date","end_date","product_id",
     *     "category_id","collection_id","status"}),
     * })
     * */
    public function update(Request $request, $id){
        $rules = array(
            'code' => 'required|unique:diskon,kode,'.$id,
            'total' => 'required|integer',
            'discount' => 'required|integer',
            'minimal_order' => 'required|integer|min:'.$request->get('discount'),
            'start_date' => 'required',
            'end_date' => 'required',
        );
        if($request->get('type')=='2'){
            $rules['discount'] = 'required|integer|between:0,100';
        }

        $validation = Validator::make($request->all(),$rules);
        if($validation->fails()){
            return $this->response->error(implode(',',$validation->errors()->all()),422);
        }

        $discount = Discount::find($id);
        $discount->kode = strip_tags($request->get('code'));
        $discount->jenisPotongan = $request->get('type');
        $discount->besarPotongan = strip_tags($request->get('discount'));
        $discount->tglMulai = ($request->get('start_date'));
        $discount->tglBerakhir = ($request->get('end_date'));
        $discount->jumlah = strip_tags($request->get('total'));
        $discount->minBuy = strip_tags($request->get('minimal_order'));
        $discount->produkId = $request->get('product_id');
        $discount->kategoriId = $request->get('category_id');
        $discount->koleksiId = $request->get('collection_id');
        $discount->akunId = $request->user()->akunId;
        $discount->status = $request->get('status');
        $discount->save();
        if($discount){
            return $this->response->message(trans('message.success.update',['obj' => 'Discount']));
        }else{
            return $this->response->error(trans('message.error.update',['obj' => 'Discount']),422);
        }
    }

    /**
     * Delete Discount.
     *
     * delete product discount
     *
     * @Delete("/id")
     * @Versions({"v1"})
     *
     * @Parameters({
     *      @Parameter("id", description="discount id"),
     * })

     */
    public function destroy($id){
        $discount = Discount::find($id);
        $discount->delete();
        return $this->response->message(trans('message.success.delete',['obj' => 'Discount']));
    }


}