<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 8/16/17
 * Time: 2:22 PM
 */

namespace App\Http\Controllers\V1;


use App\Category;
use App\Http\Transformers\CategoryTransformer;
use Dingo\Api\Contract\Http\Request;
use Dingo\Api\Routing\Helpers;
use Validator;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @Resource("Categories",uri="/categories")
 */
class CategoryController extends BaseController
{
    use Helpers;

    /**
     * Display all category.
     *
     * Get a JSON representation of all the category.
     *
     * @Get("/")
     * @Versions({"v1"})
     */
    public function index(Request $request)
    {
        $categories = Category::where('akunId', $request->user()->account->id)->get();
        return $this->response->collection($categories, new CategoryTransformer());
    }

    /**
     * Display 1 category.
     *
     * Get a JSON representation of 1 the product category.
     *
     * @Get("/{id}")
     * @Versions({"v1"})
     * @Parameters({
     *      @Parameter("id", description="category id"),
     * })
     */
    public function show($id)
    {
        $category = Category::find($id);
        return $this->response->item($category, new CategoryTransformer());
    }

    /**
     * Store Category.
     *
     * add new product category
     *
     * @Post("/")
     * @Versions({"v1"})
     *
     * @Transaction({
     *     @Request({"name","parent","slug"}),
     * })

     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'parent' => 'required',
            'slug' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return $this->response->error(implode(',',$validator->errors()->all()), 422);
        }
        $order = 1;
        $check = Category::where('parent', '=', $request->get('parent'))->where('akunId', '=', $request->user()->account->id)->orderBy('urutan', 'desc')->first();
        if ($check != null) {
            $order = $check->urutan + 1;
        }

        $category = Category::create([
            'nama' => $request->get('name'),
            //'description' => $request->get('description',''),
            'slug' => $request->get('slug'),
            'parent' => $request->get('parent'),
            'urutan' => $order,
            'akunId' => $request->user()->account->id,
        ]);
        return $this->response->item($category, new CategoryTransformer());
    }

    /**
     * Update Category.
     *
     * update product category
     *
     * @Put("/id")
     * @Versions({"v1"})
     *
     * @Transaction({
     *     @Request({"name","parent","slug"}),
     * })
     * @Parameters({
     *      @Parameter("id", description="product category"),
     * })

     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'parent' => 'required',
            'slug' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(),422);
        }
        $category = Category::find($id);

        $category->update([
            'nama' => $request->get('name'),
            //'description' => $request->get('description',''),
            'slug' => $request->get('slug'),
            'parent' => $request->get('parent'),
            'urutan' => $request->get('order'),
            'akunId' => $request->user()->account->id,
        ]);
        return $this->response->item($category, new CategoryTransformer());
    }

    /**
     * Delete Category.
     *
     * delete product category
     *
     * @Delete("/id")
     * @Versions({"v1"})
     *
     * @Parameters({
     *      @Parameter("id", description="product category"),
     * })

     */
    public function destroy($id)
    {
        Category::destroy($id);
        return $this->response->error(trans('message.success.delete',['obj' => 'product category']),200);
    }
}