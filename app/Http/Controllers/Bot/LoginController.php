<?php
namespace App\Http\Controllers\Bot;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 2/11/17
 * Time: 12:52 PM
 */
class LoginController extends Controller
{

    public function store(Request $request)
    {
        $rs = Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'tipe'  => 1
        ]);
        if($rs){
            return response()->json([
                'message' => 'success',
                'user' => Auth::user(),
                'token'   => base64_encode($request->get('email').':'.$request->get('password'))
            ]);
        }
        return response()->json([
            'message' => 'failed'
        ],401);

    }

    public function user(Request $request)
    {
        $email = $request->header('PHP_AUTH_USER');
        $password = $request->header('PHP_AUTH_PW');

        $rs = Auth::attempt([
            'email' => $email,
            'password' => $password,
            'tipe'  => 1
        ]);
        return response()->json([
            'user' => Auth::user()
        ]);
    }
}