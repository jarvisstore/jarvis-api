<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 9/12/17
 * Time: 1:46 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $guarded = array();
    public static $rules = array();
    protected $table = 'testimonial';
    protected $fillable = [
        'nama','isi','visibility','akunId'
    ];

}