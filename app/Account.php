<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 12/7/16
 * Time: 10:46 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    public static $rules = array();
    protected $table = 'akun';

    public function setting()
    {
        return $this->hasOne(Setting::class,'akunId');
    }
    public function jarvisPacket()
    {
        return $this->belongsTo(JarvisPackage::class,'jenisPaket');
    }
    public function logo()
    {
        return 'https://d2kl9mvmw5l7p9.cloudfront.net/'.$this->folder.'/galeri/';
    }
}