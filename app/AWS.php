<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 9/22/17
 * Time: 12:29 PM
 */

namespace App;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Intervention\Image\Facades\Image;

class AWS {

    public $client;
    public $bucket;
    public $key;
    public $akun;

    public function __construct(){

        $client  = new S3Client([
            'version'     => 'latest',
            'region'  => config('app.aws.region'),
            'endpoint' => config('app.aws.endpoint'),
            'credentials' => [
                'key'    => config('app.aws.key'),
                'secret' => config('app.aws.secret'),
            ]
        ]);

        $this->client = $client;
    }

    public function uploadImage($bucket,$key,$img,$content_type,$extension){

        try {
            $result = $this->client->putObject(array(
                'Bucket'     => $bucket,
                'Key'        => $key,
                'Body' => $img->encode($extension),
                'ContentType' => $content_type,
                'ACL'    => 'public-read',
                'CacheControl' => 'max-age=2419200, public'
            ));

            $this->client->waitUntil('ObjectExists', array(
                'Bucket' => $bucket,
                'Key'    => $key
            ));


            $size = strlen((string) $img->encode($extension));
            $akun = app()->make('App\Account');
            $akun->used_storage = (($akun->used_storage * 1000000)  + $size) / 1000000;
            $akun->save();

            return $result;
        } catch (S3Exception $e) {
            return false;
        }

    }

    public function deleteImage($bucket,$key){

        try {

            try {
                $url = config('app.storage.base').$key;

                stream_context_set_default(
                    array(
                        'http' => array(
                            'method' => 'HEAD'
                        )
                    )
                );

                $headers = get_headers($url, 1);
                $headers = array_change_key_case($headers);

                $size = trim($headers['content-length'],'"');

                $akun = app()->make('App\Account');
                $akun->used_storage = (($akun->used_storage * 1000000)  - $size) / 1000000;
                $akun->save();

                $result = $this->client->deleteObject(array(
                    'Bucket'     => $bucket,
                    'Key'        => $key,
                ));

                return $result;
            } catch (Exception $e) {
                return false;
            }
        } catch (S3Exception $e) {
            return false;
        }
    }

    public function uploadImageProduct($file,$product=null){
        $akun = app()->make('App\Account');

        $bucket               = config('app.aws.bucket');
        $extension            = $file->getClientOriginalExtension();
        $product_image_config = \config('app.storage.product');

        // cek config tema yang digunakan
        $produk_width_original = 750;

        if($product){
            $filename = $product->slug.'-'.time().'.'.$extension;
        }else{
            $filename = date('Ymd-His').'.'.$extension;
        }

        $images = array(
            'filename' => $filename,
        );

        foreach ($product_image_config as $key => $value) {

            if($key == 'original')
            {
                $img = Image::make($file->getRealPath());
                $image = getimagesize($file);

                // validasi untuk image
                if($image[0] < $produk_width_original && $image[1] < $produk_width_original){
                    $img->resize($produk_width_original, $produk_width_original, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $img->resizeCanvas($produk_width_original, $produk_width_original, 'center');
                }elseif($image[0] < $produk_width_original){
                    $img->resize($produk_width_original, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $img->resizeCanvas($produk_width_original, null, 'center');
                }elseif($image[1] < $produk_width_original){
                    $img->resize(null, $produk_width_original, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $img->resizeCanvas(null, $produk_width_original, 'center');
                }else{
                    $img->resize($produk_width_original, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                }

                $path = $akun->folder.'/'.\config('app.storage.product_path').$value['path'] . $filename;
                $images[$key] = $rs = $this->uploadImage($bucket,$path,$img,'image/png','png');

            }
            else
            {
                $img = Image::make($file->getRealPath());
                $img->resize($value['width'], null , function ($constraint) use ($file, $value)  {
                    $constraint->aspectRatio();

                    $image = getimagesize($file);

                    if($image[0] > $value['width'] || $image[1] > $value['height']){
                        $constraint->upsize();
                    }

                });
                // set a background-color for the emerging area
                $img->resizeCanvas($value['width'], $value['height'], 'center');
                $path = $akun->folder.'/'.\config('app.storage.product_path').$value['path'] . $filename;
                $images[$key] = $rs = $this->uploadImage($bucket,$path,$img,'image/png','png');
            }

        }
        return $images;
    }

    public function deleteImageProduct($img){

        $akun = App::make('akun');
        $bucket = config('app.aws.bucket');
        $product_image_config = \config('app.storage.product');
        $rs = array();
        foreach ($product_image_config as $key => $value) {

            $path = $akun->folder.'/'.\config('app.storage.product_path').$value['path'] . $img;
            $rs[$key] = $this->deleteImage($bucket,$path);

        }
        return $rs;
    }

    public function uploadKoleksiImage($file){

        $akun = App::make('akun');
        $bucket = config('app.aws.bucket');
        $extension =$file->getClientOriginalExtension();
        $filename = date('Ymd-His').'.'.$extension;

        $koleksi_image_config = \config('app.storage.koleksi');
        $images = array(
            'filename' => $filename
        );

        foreach ($koleksi_image_config as $key => $value) {
            $img = Image::make($file->getRealPath());
            $img->resize($value['width'], null , function ($constraint) {
                $constraint->aspectRatio();
            });
            // set a background-color for the emerging area
            $img->resizeCanvas($value['width'], $value['height'], 'center');
            $path = $akun->folder.'/'.\config('app.storage.koleksi_path').$value['path'] . $filename;
            $images[$key] = $this->uploadImage($bucket,$path,$img,'image/png','png');
        }
        return $images;

    }

    public function deleteKoleksiImage($img){

        $akun = App::make('akun');
        $bucket = config('app.aws.bucket');
        $product_image_config = \config('app.storage.koleksi');
        $rs = array();
        foreach ($product_image_config as $key => $value) {

            $path = $akun->folder.'/'.\config('app.storage.koleksi_path').$value['path'] . $img;
            $rs[$key] = $this->deleteImage($bucket,$path);

        }
        return $rs;
    }

    public function uploadSlideImage($file,$name){
        $akun 			= App::make('akun');
        $bucket 		= config('app.aws.bucket');
        $extension 		= 'jpg';
        $filename 		= $name;
        $image_config 	= \config('app.storage.slides');

        // cek settingan width slideshow di tema yang digunakan
        $theme          = \Tema::where('akunId', '=', $akun->id)->where('status', '=', '1')->first();
        $dirTema        = (string) $akun->id . '-tema/' . $theme->temaJarvis->nama;
        $tema           = \Theme::theme($dirTema);
        $config         = $tema->getConfig();
        $original_width = array_key_exists('num_display', $config) ? (array_key_exists('image_slide', $config['num_display']) ? $config['num_display']['image_slide'] : $image_config['original']['width']) : $image_config['original']['width'];

        $images = array(
            'filename' => $filename
        );

        foreach ($image_config as $key => $value) {
            if ($key == 'original') {
                $width  = $original_width;
                $height = $value['height'];
            } else {
                $width  = $value['width'];
                $height = 85;
            }

            $img = Image::make($file->getRealPath());
            $img->resize($width, null , function ($constraint) {
                $constraint->aspectRatio();
            });
            // set a background-color for the emerging area
            $img->resizeCanvas($width, $height, 'center', false, 'ffffff');
            $path = $akun->folder.'/'.\config('app.storage.slides_path').$value['path'] . $filename;
            $images[$key] = $this->uploadImage($bucket,$path,$img,'image/png','png');
        }

        return $images;
    }

    public function deleteImageSlide($img){

        $akun = App::make('akun');
        $bucket = config('app.aws.bucket');
        $image_config = \config('app.storage.slides');
        $rs = array();
        foreach ($image_config as $key => $value) {

            $path = $akun->folder.'/'.\config('app.storage.slides_path').$value['path'] . $img;
            $rs[$key] = $this->deleteImage($bucket,$path);

        }
        return $rs;
    }

    public function uploadBannerImage($file,$type){
        $akun         = App::make('akun');
        $bucket       = config('app.aws.bucket');
        $extension    = $file->getClientOriginalExtension();
        $filename     = date('Ymd-His').'.'.$extension;
        $image_config = \config('app.storage.banners');

        // cek width banner di config tema yang digunakan
        $theme              = \Tema::where('akunId', '=', $akun->id)->where('status', '=', '1')->first();
        $dirTema            = (string) $akun->id . '-tema/' . $theme->temaJarvis->nama;
        $tema               = \Theme::theme($dirTema);
        $config             = $tema->getConfig();
        $sidebanner_width   = array_key_exists('num_display', $config) ? (array_key_exists('image_sidebanner', $config['num_display']) ? $config['num_display']['image_sidebanner'] : $image_config['sidebar']['width']) : $image_config['sidebar']['width'];
        $mainbanner_width   = array_key_exists('num_display', $config) ? (array_key_exists('image_mainbanner', $config['num_display']) ? $config['num_display']['image_mainbanner'] : $image_config['mainbar']['width']) : $image_config['mainbar']['width'];

        $images = array(
            'filename' => $filename
        );

        if ($type == 'mainbar'){
            $width = $mainbanner_width;
        } else {
            $width = $sidebanner_width;
        }

        $img = Image::make($file->getRealPath());
        $img->resize($width, $image_config[$type]['height'] , function ($constraint) {
            $constraint->upsize();
        });
        // set a background-color for the emerging area
        $img->resizeCanvas($width, $image_config[$type]['height'], 'center');
        $path = $akun->folder.'/'.\config('app.storage.slides_path').$image_config[$type]['path'] . $filename;

        $images[$type] = $this->uploadImage($bucket,$path,$img,'image/png','png');

        $img2 = Image::make($file->getRealPath());
        $img2->resize($image_config[$type]['thumb']['width'], $image_config[$type]['thumb']['height'] , function ($constraint) {
            $constraint->aspectRatio();
            //$constraint->upsize();
        });
        // set a background-color for the emerging area
        $img2->resizeCanvas($image_config[$type]['thumb']['width'], $image_config[$type]['thumb']['height'], 'center');
        $paths = $akun->folder.'/'.\config('app.storage.slides_path').$image_config[$type]['thumb']['path'] . $filename;

        $images['thumb'] = $this->uploadImage($bucket,$paths,$img2,'image/png','png');

        return $images;
    }

    public function deleteBannerImage($img){

        $akun = App::make('akun');
        $bucket = config('app.aws.bucket');
        $image_config = \config('app.storage.banners');
        $rs = array();

        $path = $akun->folder.'/'.\config('app.storage.slides_path').$image_config['thumb']['path'] . $img;
        $rs['thumb'] = $this->deleteImage($bucket,$path);

        $path = $akun->folder.'/'.\config('app.storage.slides_path'). $img;
        $rs['main'] = $this->deleteImage($bucket,$path);

        return $rs;
    }

    public function uploadLogoImage($file){
        $akun = App::make('akun');
        $bucket = config('app.aws.bucket');
        $extension =$file->getClientOriginalExtension();
        $filename = date('Ymd-His').'.'.$extension;

        $image_config = \config('app.storage.logo');
        $images = array(
            'filename' => $filename
        );

        foreach ($image_config as $key => $value) {
            $img = Image::make($file->getRealPath());
            $img->resize($value['width'], $value['height'] , function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            // set a background-color for the emerging area
            $img->resizeCanvas($value['width'], $value['height'], 'center');
            $path = $akun->folder.'/'.\config('app.storage.slides_path').$value['path'] . $filename;
            $images[$key] = $this->uploadImage($bucket,$path,$img,'image/png','png');
        }
        return $images;
    }

    public function deleteLogoImage($img){

        $akun = App::make('akun');
        $bucket = config('app.aws.bucket');
        $image_config = \config('app.storage.logo');
        $rs = array();
        foreach ($image_config as $key => $value) {

            $path = $akun->folder.'/'.\config('app.storage.slides_path').$value['path'] . $img;
            $rs[$key] = $this->deleteImage($bucket,$path);

        }
        return $rs;
    }

    public function uploadFaviconImage($file){

        $akun = App::make('akun');
        $bucket = config('app.aws.bucket');
        $extension = strtolower($file->getClientOriginalExtension()); //if you need extension of the file

        $filename = 'favicon.jpg';

        $image_config = \config('app.storage.favicon');
        $images = array(
            'filename' => $filename
        );

        $img = Image::make($file->getRealPath());
        $img->resize($image_config['width'], null , function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        // set a background-color for the emerging area
        $img->resizeCanvas($image_config['width'], $image_config['height'], 'center');
        $path = $akun->folder.'/'.\config('app.storage.slides_path') . $filename; //extension
        $images['favicon'] = $this->uploadImage($bucket,$path,$img,$file->getMimeType(),$extension); //extension
        return $images;
    }

    public function uploadMasterDirectory(){
        $bucket = 'uploads';
        $dir = public_path().'/upload';
        $bucket = config('app.aws.bucket');
        $data = $this->recurse_copy($dir,$dst);

        try {
            $result = array();
            foreach ($data as $key => $value) {
                $result[] = $this->client->putObject(array(
                    'Bucket'     => $bucket,
                    'Key'        => $value['key'],
                    'SourceFile' => $value['source'],
                    'ACL'    => 'public-read'
                ));
            }

            return $result;
        } catch (S3Exception $e) {
            return false;
        }
    }
    public function uploadClientDirectory($akun){
        $bucket = config('app.aws.bucket');
        $dst = 'yusida-upload';
        $data = $this->getMasterObject();
        try {
            $result = array();
            foreach ($data as $key => $value) {

                $key = str_replace('upload/', $dst.'/', $value);
                $result[] = $this->client->copyObject(array(
                    'Bucket'     => $bucket,
                    'Key'        => $key,
                    'CopySource' => $bucket.'/'.$value,
                    'ACL'    => 'public-read'
                ));
            }

            return $result;
        } catch (S3Exception $e) {
            return false;
        }
    }

    private function recurse_copy($src,$dst){

        $rs = array();
        $dir = opendir($src);
        @mkdir($dst);
        //chmod($dst,0755);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' ) && $file!='.DS_Store') {
                if ( is_dir($src . '/' . $file) ) {
                    $rs2 = $this->recurse_copy($src . '/' . $file,$dst . '/' . $file);
                    $rs = array_merge($rs, $rs2);
                }
                else {
                    array_push($rs, array(
                        'source' => $src.'/'.$file,
                        'key' => $dst . '/' . $file
                    ));
                }
            }
        }
        closedir($dir);
        return $rs;
    }

    private function getMasterObject(){
        $bucket = config('app.aws.bucket');
        $iterator = $this->client->getIterator('ListObjects', array(
            'Bucket' => $bucket,
            'Prefix' => 'upload/',
        ));
        $rs = array();
        foreach ($iterator as $object) {
            $rs[] = $object['Key'];
        }
        return $rs;
    }

    public function uploadFolder($key,$bucket=''){

        try {
            $bucket = config('app.aws.bucket');
            $result = $this->client->uploadDirectory($key, $bucket, '', array(
                'params'      => array('ACL' => 'public-read'),
                'concurrency' => 20,
            ));

            return $result;
        } catch (S3Exception $e) {
            return false;
        }

    }

    public function downloadFolder($key) {

        try {

            $bucket = config('app.aws.bucket');
            $result = $this->client->downloadBucket(public_path(), $bucket, $key);
            return $result;
        } catch (S3Exception $e) {
            return false;
        }

    }

    public function uploadFile($bucket,$key,$file,$options){
        try {
            $result = $this->client->putObject(array(
                'Bucket'     => $bucket,
                'Key'        => $key,
                'SourceFile' => $file,
                'ACL'    => 'public-read',
                'CacheControl' => 'max-age=2419200, public'
            ));

            $this->client->waitUntil('ObjectExists', array(
                'Bucket' => $bucket,
                'Key'    => $key
            ));

            return $result;
        } catch (S3Exception $e) {
            return false;
        }
    }

    public function deleteFolder($key) {

        try {

            $bucket = config('app.aws.bucket');
            $result = $this->client->deleteMatchingObjects($bucket,$key);
            return $result;
        } catch (S3Exception $e) {
            return false;
        }

    }

    public function deleteSelectedFile($path) {
        try {
            $akun = App::make('akun');
            $bucket = config('app.aws.bucket');
            $result = array();
            $result[0] = $this->deleteImage($bucket,$path);
            return $result;
        } catch (S3Exception $e) {
            return false;
        }

    }

    public function getClientFolder(){
        $akun = App::make('akun');
        if(!$akun){
            if($product){
                $akun = \Akun::find($product->akunId);
                App::instance('akun', $akun);
            }
        }
        $bucket = config('app.aws.bucket');
        $iterator = $this->client->getIterator('ListObjects', array(
            'Bucket' => $bucket,
            'Prefix' => $akun->folder,
        ));
        $rs = array();
        foreach ($iterator as $object) {
            $rs[] = $object['Key'];
        }
        return $rs;
    }

    public function getSizeAWS(){
        $akun = App::make('akun');
        if(!$akun){
            if($product){
                $akun = \Akun::find($product->akunId);
                App::instance('akun', $akun);
            }
        }
        $bucket = config('app.aws.bucket');
        $iterator = $this->client->getIterator('ListObjects', array(
            'Bucket' => $bucket,
            'Prefix' => $akun->folder
        ));
        $rs = array();
        foreach ($iterator as $key => $object) {
            for ($i=0; $i < 2; $i++) {
                if ($i==0) {
                    $rs[$key]['file'] = $object['Key'];
                }else{
                    $rs[$key]['size'] = $object['Size'];
                }
            }
        }
        return $rs;
    }
    public function uploadPembayaranImage($file){
        $akun = App::make('akun');
        $bucket = config('app.aws.bucket');
        $extension =$file->getClientOriginalExtension();
        $filename = date('Ymd-His').'.'.$extension;

        $images = array(
            'filename' => $filename
        );

        $img = Image::make($file->getRealPath());
        $path = $akun->folder.'/laporan/'. $filename;
        $images = $this->uploadImage($bucket,$path,$img,'image/png','png');

        return $images;
    }
}
