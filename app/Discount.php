<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 12/7/16
 * Time: 10:46 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'diskon';

    public function scopeAccount($query, $account)
    {
        return $query->where('akunId', $account->id);
    }

}