<?php
/**
 * Created by PhpStorm.
 * User: yusida
 * Date: 12/7/16
 * Time: 10:46 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $guarded = array();
    public $timestamps = true;
    public static $rules = array();
    protected $table = 'blog';


    public function category()
    {
        return $this->belongsTo(BlogCategory::class, 'kategoriBlogId');
    }

    public function authors()
    {
        return $this->belongsTo(User::class, 'author');
    }

    public function url()
    {
        $account = app()->make('App\Account');

        if ($account->alamatWeb != '') {
            if (!empty($this->category)) {
                return url('http://' . $account->alamatWeb . '/blog/' . str_slug($this->category->nama) . '/' . $this->slug);
            }
            return url('http://' . $account->alamatWeb . '/blog/' . $this->slug);
        }

        if (!empty($this->category)) {
            return url('http://' . $account->alamatJarvis . '.jstore.co' . '/blog/' . str_slug($this->category->nama) . '/' . $this->slug);
        }
        return url('http://' . $account->alamatJarvis . '.jstore.co' . '/blog/' . $this->slug);
    }
}