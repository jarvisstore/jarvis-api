<?php

return array(
    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY', 'SomeRandomString!!!'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */
    'locale' => env('APP_LOCALE', 'en'),
    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */
    'fallback_locale' => env('APP_FALLBACK_LOCALE', 'en'),

    'aws' => [
        'key'    => 'AKIAIUUEEFJJYLNVXELQ', // Your AWS Access Key ID
        'secret' => 'T2vX8YZNPy2TUyVj7eLRt/yuSDpnCa/75PYX6jJe', // Your AWS Secret Access Key
        'endpoint' => 'https://s3-ap-southeast-1.amazonaws.com/cdn.jarvis-store.com',
        'endpoint_old' => 'https://s3-ap-southeast-1.amazonaws.com/cdn.jarvis-store.com',
        'bucket' => 'cdn.jarvis-store.com',
        'region' => 'ap-southeast-1',
    ],

    'cdn_status' => 1,
    'url' => 'https://jarvis-store.com/',
    'subdomain' => 'jstore.co',
    'maindomain' => 'jarvis-store.com',
    'domain' => 'jstore.co',
    'cpanel' => 'direct.jstore.co',
    'username' => 'jstore77',
    'password' => 'BKd-qNK-XYE-QT4',
    'telkomsel-email' => 'agusyusida@gmail.com',

    'sandbox' => 'sandbox.jarvis-store.com',

    'partnerships' => array(
        'city-marketplace.com'
    ),

    'whmcs' => [
        'url' => 'http://member.jarvis-server.com/includes/api.php',
        'username' => 'jarvisstore',
        'password' => 'jarvisapi2314'
    ],
    'github' => array(
        'username' => 'jarvisstore',
        'password' => 'jarvisse2314',
    ),


    'storage' => array(
        'base' => 'https://s3-ap-southeast-1.amazonaws.com/cdn.jarvis-store.com/',
        'loadurl' => '//d2kl9mvmw5l7p9.cloudfront.net/',
        'product_path' => 'produk/',
        'koleksi_path' => 'koleksi/',
        'slides_path' => 'galeri/',
        'favicon_path' => array(
            'path' => 'galeri/',
            'height' => 16,
            'width' => 16

        ),
        'banners' => array(
            'thumb' => array(
                'path' => 'thumb/',
                'height' => null,
                'width' => 75
            ),
            'mainbar' => array(
                'path' => '',
                'height' => null,
                'width' => 980,

                'thumb' => array(
                    'path' => 'thumb/',
                    'height' => null,
                    'width' => 200
                ),
            ),
            'sidebar' => array(
                'path' => '',
                'height' => null,
                'width' => 250,

                'thumb' => array(
                    'path' => 'thumb/',
                    'height' => null,
                    'width' => 50
                ),
            )
        ),
        'favicon' => array(
            'height' => 16,
            'width' => 16,
        ),
        'logo' => array(
            'thumb' => array(
                'path' => 'thumb/',
                'height' => 75,
                'width' => 75
            ),
            'original' => array(
                'path' => '',
                'height' => 150,
                'width' => null
            )
        ),
        'product' => array(
            'thumb' => array(
                'path' => 'thumb/',
                'width' => 150,
                'height' => 150
            ),
            'medium' => array(
                'path' => 'medium/',
                'width' => 240,
                'height' => 240,
            ),
            'large' => array(
                'path' => 'large/',
                'width' => 600,
                'height' => 600
            ),
            'original' => array(
                'path' => '',
                'height' => null,
                'width' => 800
            ),
        ),

        'koleksi' => array(

            'thumb' => array(
                'path' => 'thumb/',
                'width' => 150,
                'height' => 150
            ),
            'medium' => array(
                'path' => 'medium/',
                'width' => 240,
                'height' => null,
            ),
            'large' => array(
                'path' => 'large/',
                'width' => 600,
                'height' => null
            ),
            'original' => array(
                'path' => '',
                'height' => null,
                'width' => 800
            ),
        ),

        'slides' => array(

            'thumb' => array(
                'path' => 'thumb/',
                'width' => 150,
                'height' => null
            ),
            'original' => array(
                'path' => '',
                'height' => null,
                'width' => 940
            ),
        ),
    ),

);
