var Vue = require('vue');
var VueRouter = require('vue-router');

import Login from './components/Login.vue';

Vue.use(VueRouter);

const Dashboard = { template: '<div>bar</div>' }

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// Vue.extend(), or just a component options object.
// We'll talk about nested routes later.
const routes = [
    { path: '/login', component: Login },
    { path: '/dashboard', component: Dashboard }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
    routes // short for routes: routes
})

new Vue({
    el : '#app',
    data : {
        message : 'Hello world'
    },
    router
})