<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| such as the size rules. Feel free to tweak each of these messages.
	|
	*/

	"accepted"         => ":attribute 必须被接受。",
	"active_url"       => ":attribute 没有有效的URL ",
	"after"            => ":attribute 必须后日期 :date",
	"alpha"            => ":attribute 只能包含字母。",
	"alpha_dash"       => ":attribute 只能包含字母，数字和下划线。",
	"alpha_num"        => ":attribute 只能包含字母和数字。",
	"array"            => ":attribute 应的阵列。",
	"before"           => ":attribute 应该是之前的日期:date",
	"between"          => array(
		"numeric" => ":attribute 必须介于 :min - :max ",
		"file"    => ":attribute 必须介于 :min - :max Kilobytes",
		"string"  => ":attribute 必须介于:min - :max characters",
		"array"   => ":attribute 必须介于:min - :max items",
	),
	"confirmed"        => ":attribute 确认不匹配。",
	"date"             => ":attribute 不是有效的日期。",
	"date_format"      => ":attribute 不按照格式 :format",
	"different"        => ":attribute 和 :others 必须是不同",
	"digits"           => ":attribute 必须 :digits Digits.",
	"digits_between"   => ":attribute 必须介于 :min - :max Digits",
	"email"            => ":attribute 格式无效。",
	"exists"           => "Selected :attribute 无效。",
	"image"            => ":attribute 必须是一个图像。",
	"in"               => "Selected :attribute 无效。",
	"integer"          => ":attribute 必须是一个数字。",
	"ip"               => ":attribute 必须是一个有效的IP地址。",
	"max"              => array(
		"numeric" => ":attribute 必须不大于:max",
		"file"    => ":atrribute 必须不大于:max Kilobytes",
		"string"  => ":atrribute 必须不大于:max Characters",
		"array"   => ":atrribute 必须不大于:max Items",
	),
	"mimes"            => ":attribute 必须使用的文件类型：:values",
	"min"              => array(
		"numeric" => ":attribute 必须至少 :min",
		"file"    => ":attribute 必须至少 :min Kilobytes",
		"string"  => ":attribute 必须至少 :min characters",
		"array"   => ":attribute 必须至少 :min items",
	),
	"not_in"           => ":attribute 选择是无效的。",
	"numeric"          => ":attribute 必须是一个数字。",
	"regex"            => ":attribute 格式无效。",
	"required"         => ":attribute 仍然是空的。",
	"required_if"      => ":attribute 这需要同 :other 时:value",
	"required_with"    => ":attribute 这是需要时 :values 可用。",
	"required_without" => ":attribute 这是必要的，当它是 :value 不可用的。",
	"same"             => ":attribute 并且:other 应该是相同的。",
	"size"             => array(
		"numeric" => ":attribute 应该值得 :size",
		"file"    => ":attribute 应该测量 :size kilobytes",
		"string"  => ":attribute 必须 :size characters",
		"array"   => ":attribute 必须包含 size items",
	),
	"unique"           => ":attribute 已在使用，请选择其他。.",
	"url"              => ":attribute 格式无效。",
	'ceknamatoko' => '对不起，您选择的店名已被使用，请选择其他。',
    'alpha_num_space' => '请确保您的商店的名字也只是字母数字和空格。',
    'alpha_num_without_space' => '贾维斯您的域名是不允许的。',
	'domain'			=> '您的域名地址不正确。',
	"unique_cateogry"           => ":attribute 相同的父类中已经存在。",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(
		'namatoko' => '商店名称',
		'email' => '您的电子邮件',
		'password' => '密码',
		'g-recaptcha-response' => '验证码',
		'unique_category' => '类别'
		),

);
