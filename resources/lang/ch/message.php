﻿<?php
return array(
    'register'     => array(
        'success' => array(
            'title'  => '你的店铺已成功完成！',
            'body'   => '恭喜你，你已经成功地开设专卖店！',
            'button' => '点击这里进入到你的店！'
        ),
        'error'   => array(
            'ip-limit' => '对不起，请尽量在5分钟内重新注册。'
        ),
    ),
    'login-member' => array(
        'error'   => array(
            'email'     => "很抱歉，我们没有发现此电子邮件帐户。",
            'password'  => '对不起，您输入的密码不正确',
            'not_found' => '对不起，账户没有被发现。',
        ),
        'success' => '<h3 class="lead">Welcome</h3> <h1>:obj</h1><br><img src="https://s3-ap-southeast-1.amazonaws.com/cdn2.jarvis-store.com/frontend/assets/images/icon/selamat-datang-login.png"><br><br><p class="lead" style="text-transform: lowercase;">欢迎您的商店帐户。</p>',
        'auto_logout' => "你被自动取消，请重新登录。."
    ),

    'success'      => array(
        'create'               => '成功创建了一个新的 :obj 。 ',
        'upload'               => '成功上传新:obj 。 ',
        'update'               => '已成功更新:obj ',
        'delete'               => '已成功删除 :obj',
        'install_theme'        => '这个主题成功安装在您的商店。',
        'install_power_up'     => '插件安装成功在您的帐户。.',
        'payment_confirmation' => '付款确认已发送。',
        'non_activated'        => '成功禁用 :obj',
        'activated'            => '成功激活:obj 。',
        'register'             => 'Horay 。您的网站已成功创建，进入下一个步骤中，您现在可以在网上卖！',
        'forget-password'      => '恭喜你，重置您的密码已发送到您的邮箱！',
        'reset-password'       => '您的新密码已更改，请重新登录！',
        'login' => '恭喜你，你已经成功登录到系统中。',
        'logout' => '谢谢您，已经被取消，请稍后再回来...',
        'login_member' => '恭喜你，你已经成功登录',
        'logout_member' => '你已经出来了',
        'reset_pass'               => '成功地更新您的密码。',
    ),

    'error'        => array(
        'create'               => 'Woops ，未能创造新 :obj 。',
        'update'               => 'Woops ，无法更新 :obj 。',
        'delete'               => 'Woops ，无法删除 :obj。',
        'exist'                => 'Woops ，这样 :obj 是已经在系统中。',
        'size'                 => 'Woops ，没有可用的足够的空间 :obj',
        'install_theme'        => '无法安装这个主题，请重试。',
        'install_power_up'     => '无法安装此电，请重试。',
        'payment_confirmation' => '付款确认无法对发送，请重试！',
        'login' => array(
            'email' => '我们无法找到您的电子邮件',
            'password' => '您输入了错误的密码。',
            'account' => '与此电子邮件帐户没有被发现。',
            'not_active' => '此帐户未激活',
            'permission' => '您无权访问此页面 。'
        ),
        'reset_pass'               => 'Woops ，你的密码重置代码没有找到。',
        'fail_pass'               => 'Woops ，无法重置您的密码。',
        'fail_recover'               => 'Woops ，你的恢复代码是不存在，请返回忘记密码的页面。.',

    ),
    'admin'      => array(
        'set_template'               => '对不起，您只能安装两个主题。请升级您的帐户可以安装更多的主题。',
        'upload'               => '成功上传新:obj 。 ',
        'update'               => '成功地更新:obj。 ',
        'delete'               => '成功删除:obj',
        'file_notfound'               => 'This file is not found (404)',
        'required'               => '请填写所有的输入，它不能为空',
        'blog' => array(
            'required'               => '请填写所有的输入，它不能为空',
            'unique'               => '已经存在的标题，请尝试其他',
            'category'         => '博客分类',
        ),
        'discount' => array(
            'discount'         => '折扣',
            'unique'         => '折扣代码已经存在，请尝试使用其他代码。',
            'between'         => '大块不应超过100％',
        ),
        'shipping' => array(
            'package'         => '远征礼包',
            'set_package'         => '设置远征',
            'wrong_package'         => '唉呀 ，错发货包.....'
        ),
        'pages' => array(
            'page'         => '页面',
        ),
        'link' => array(
            'max'         => '链路组的最大数量允许在免费包 :obj ，请升级包。',
        ),
        'category' => array(
            'cat'         => '类别',
            'sort'         => '类别顺序',
            'max'         => '类别名称不能超过200个字符',
            'alpha_num_space'         => '类别名称只能被填充有字母和数字。',
        ),
        'collect' => array(
            'col'         => '集',
            'create'         => '收藏已创建。请将本产品加入到这个收藏',
            'edit'         => '收藏已成功保存。请检查产品在此集合',
        ),
        'costumer' => array(
            'order'         => '秩序',
            'cos'         => '负荷消费',
            'wrong'         => '客户数据不正确，请再试一次',
            'unique'         => '您使用的电子邮件已经注册为会员/合作伙伴。',
        ),
        'setting' => array(
            'mall'         => '升级包能够使用的功能贾维斯商城',
            'laporan'         => '请写报告/应答消息传达！',
        ),
        'powerup' => array(
            'trusklik_success'         => '您的网站TrustKlik成功整合！',
            'trusklik_error'         => '有一个在你的域名错误。',
            'trusklik_domain'         => '您的域名已被注册在TrustKlik ！!',
            'trusklik_email'         => '您的电子邮件地址不正确。',
        ),
        'product' => array(
            'img'         => '产品图片',
            'pro'         => '产品',
            'mall_pro'         => '商城产品',
            'name'         => '产品名称必需！',
            'csv'         => '请填写的CSV您的产品的产品',
            'max'         => '哎呀，产品数量已达到最大的产品为您的商店的包',
            'laporan'         => '请写报告/应答消息传达！',
            'image'         => '哎呀，你不选择你的产品图片',
            'max_image'         => '你只能有一个最大的单位产品4张图片',
        ),
        'slide' => array(
            'max'         => '最大幻灯片只5件。',
        ),
        'rates' => array(
            'rate'         => '率',
        ),
        'close' => '关',
        'upgrade' => '升级',
    ),
    'frontend'      => array(
        'shopcart' => array(
            'coupon_notfound'         => '对不起，没有找到优惠券折扣。',
            'coupon_expired'          => '对不起，您的折扣券有效期到了。',
            'coupon_notavail'         => '对不起，该券是不允许的。',
            'coupon_minimum'          => '对不起，订单号会见的最低支出 :obj',
            'shipping_notfound'       => '未发现远征',
            'destination'             => '到目的地',
            'shipping_list'           => '远征名单',
            'here'                    => '这里',
            'info_shipping'               => '交付信息，请与我们联系',
            'member_email'               => '电子邮件地址已被使用。尝试另一个或请登录。',
        ),
        'contact' => array(
            'success'         => '您的留言已成功发送。',
            'failed'         => '没有成功发送您的消息。',
        ),
        'testimonial' => array(
            'success'         => '你已经成功添加恭喜推荐。当由管理员激活就会出现。',
        ),
    ),
);
