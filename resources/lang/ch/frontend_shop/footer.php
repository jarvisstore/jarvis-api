﻿<?php

return array(

	'shop' => array(

			'menu_utama' => array(
                    'title' => 'Menu Utama',
					'feature' => '贾维斯店的特色',
					'knowledgebase_1' => '什么是网上商店',
					'knowledgebase_2' => '网上商店交易',
					'knowledgebase_3' => '安全',
					'knowledgebase_4' => '营销和搜索引擎优化',
					'knowledgebase_5' => '什么是响应式网站',
					'knowledgebase_6' => 'GO在线业务',
					'knowledgebase_7' => '录影关于响应式网站',
					'knowledgebase_8' => '条款域ID',
					'knowledgebase_9' => '指数谷歌，雅虎和Bing TIPS',
				),
			'quick_link' => array(

					'team' => '关于我们',
					'career' => '事业',
					'press' => '按',
					'portfolio' => '投资组合',
					'term' => '术语',
					'sitemap' => '网站地图',
					'partner' => '伙伴',
					'support' => '支持',

				),
			'my_account' => array(

				'shop_demo' => '示范店',
				'admin_demo' => '演示管理员',

				),

			'alamat' => array(
				'title' => '电子邮件通讯',
				'content'	=> '获取惊人的交易，信息，提示和技巧有关直接网上商店和电子商务到您的电子邮件',
				'input' => '您的电子邮件..'
				),
		),


	);
