﻿<?php
return array(

	'title' 				=> '创建即时在线存储，控制从掌上电脑',
	'title_description' 	=> '设计为桌面，为便于人办理片剂和Smartphone无处不在，无时不',
	'register_btn' 			=> '创建帐号',
	'register_description' 	=> '现在免费注册。不到10分钟你可以在网上卖！',
    'register_btn'          => '创建帐号',
	'register_term' 		=> '通过创建一个帐户，这意味着你对我们的服务政策达成一致 <a href="'.URL::to('kebijakan').'">service policy</a>.',
	'video_btn' 			=> '查看视频介绍',
	'products_title' 		=> '一些受欢迎的产品从用户',

);
