﻿<?php

return array(

    'title' => '这里重置您的密码',
    'title_description' 	=> '请输入您的网站地址和登录电子邮件',
    'place_email' => '你的邮件',
    'place_akun' => '您的网站，例如： example.jstore.co',
    'button_success' => '点击此处继续你的店。'
);
