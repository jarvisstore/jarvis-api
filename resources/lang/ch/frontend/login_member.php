﻿<?php

return array(

    'title' => '登录到你的商店在这里！',
    'title_description' 	=> '输入您的电子邮件地址和密码！',
    'place_email' => '你的邮件',
    'place_password' => '你的密码',
    'button_success' => '点击此处继续你的店铺。',
	'click_password' 		=> '忘记密码了吗？ <a href="'.URL::to("forget-password").'">点击这里</a>'
);
