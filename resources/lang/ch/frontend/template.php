﻿<?php

return array(
		'seo' => array(
			'title' => 'Jarvis Store - Berbagai pilihan template menarik yang gratis',
	        'description' => 'Jarvis Store memiliki template responsif yang siap dipakai dan diubah sesuai keinginan.',
	        'keywords' => 'jarvis store template, responsif template, responsif design, template toko online gratis, template toko online murah',
		),
		'title' => '模板贾维斯',
		'title_description' 	=> '',
		'demo_btn' => '演示 &rarr;',
		'info_btn' => '更多信息',
		'template_footer' => '注意！仅主题/从Themeforest是在当前页的设计，被允许在贾维斯商店中使用。',

	);
