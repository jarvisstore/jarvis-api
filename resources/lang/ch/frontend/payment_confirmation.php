﻿<?php

return array(

    'title' => '付款确认',
    'title_description'     => '',
    'domain_name' => '域',
    'email' =>  '电子邮件',
    'payment_method' => '付款方法',
    'destination_account' => '银行账户',
    'total_payment' => '支付金额',
    'payment_date' => '付款日期（年/月/年）',
    'account_owner' => "账户的持有人",
    'account_no'    => '帐号',
    'konfirmsi' => '确认',

);
