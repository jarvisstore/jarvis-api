﻿<?php

return array(
		'seo' => array(
			'title' => 'Jarvis Store - Harga Paket Jarvis Store Mulai 39ribu',
	        'description' => 'Berbagai pilihan paket jarvis store sesuai kebutuhan toko online anda.',
	        'keywords' => 'harga pembuatan toko online, toko online murah, jasa pembuatan toko online, toko online instan, harga jarvis store, toko online instan'
		),
		'title' => '价格',
		'title_description' 	=> '尝试免费的功能有限。如果你像我们一样，选择了全功能的访问下列所有包',

		'price_title' => '订阅一年并获得免费2个月',
        	'price_title2' => '试着个月的订阅',
		'price_footer' => '需要规范和定制为您的网上商店？联系方式进行咨询。 <a href="mailto:info@jarvis-store.com">Contact Us</a>.',
		'domain' => '免费域名',
		'7day' => '首先7天',
		'30day' => '首先30天',
		'1year' => '至少1年认购',
		'package' => array(
				'ligth' => array(

						'title' 		=> '微型',
						'description' 	=> '最佳迷你在线商店提供产品的豆蔻量',
						'price_monthly'	=> '<small>IDR</small> 39.000 <small>/月</small>',
            			'price_yearly'	=> '<small>IDR</small> 390.000 <small> /年</small>',
						'total_product'	=> '50产品',
						'total_storage' => '150MB存储',
						'reporting'		=> '报告全文订单，产品和付款',
						'analytic'		=> '统计报表（分析）',
						'support'		=> '优先支持3'

					),
				'starter' => array(

						'title' 		=> '起动机',
						'description' 	=> '最适合小规模的网上商店，提供免费模板和电。',
						'price_monthly'	=> '<small>IDR</small> 99Rb <small>/月</small>',
                        'price_yearly'	=> '<small>IDR</small> 990.000 <small> /年</small>',
						'total_product'	=> '200产品',
						'total_storage' => '500MB存储',
						'reporting'		=> '报告全文订单，产品和付款',
						'analytic'		=> '统计报表（分析）',
						'support'		=> '优先支持3'

					),

				'premium' => array(

						'title' 		=> '额外费用',
						'description' 	=> '最适合中等规模的网上商店',
						'price_monthly'	=> '<small>IDR</small> 199RB <small>/月</small>',
                        'price_yearly'  => '<small>IDR</small> 1.990.000 <small>/年</small>',
						'total_product'	=> '1000产品',
						'total_storage' => '1.5 GB存储',
						'reporting'		=> '报告全文订单，产品和付款',
						'analytic'		=> '统计报表（分析）',
						'support'		=> '优先支持2'

					),

				'ultimate' => array(

						'title' 		=> '最终',
						'description' 	=> '对于大的网上商店和优先服务，与costum模板。',
                        'price_monthly'	=> '<small>IDR</small> 499RB <small> /月</small>',
                        'price_yearly'  => '<small>IDR</small> 4.990.000 <small> /年</small>',
						'total_product'	=> '无限产品',
						'total_storage' => '5 GB存储',
						'reporting'		=> '报告全文订单，产品和付款',
						'analytic'		=> '统计报表（分析）',
						'support'		=> '优先支持1'

					),
				'telkomsel' => array(

						'title' 		=> 'TELKOMSEL 包',
						'description' 	=> '为Telkomsel公司客户贾维斯商店一揽子计划',
						'total_product'	=> '200产品',
						'total_storage' => '500MB存储',
						'internet' 		=> '2千兆互联网数据*',
						'group_call' 	=> '免费通话*',
						'sms' 			=> 'Free SMS *',
						'telkomsel_call'=> '百分钟Telkomsel公司数字之间服用*',
						'reporting'		=> '完整报告',
						'analytic'		=> '统计报告',
						'support'		=> '无限支持',
						'syarat'		=> '在功能套餐形式Telkomsel公司*服务和政策基础'

					),
				'free' => array(

						'title' 		=> '现在免费试用',
						'description' 	=> "让我们开始在网上商店销售",
						'price_monthly'	=> '<small>IDR</small> 0 <small>/月</small>',
						'total_product'	=> '10个产品',
						'total_storage' => '35MB存储',
						'support'		=> '有限支持'

					),

				'enterprise' => array(

						'title' 		=> '企业',
						'description' 	=> '规模大的网店有没有限制的完全自定义',
						'price_monthly'			=> '-- CALL --',
					),

			),
		'bandwidth' => '无限带宽',
		'discount' => '免费折扣代码供电',
		'html' => 'HTML/ CSS编辑',
		'support' => '支持HTML/ CSS编辑',
		'premium_theme' => '免费溢价模板',
		'free_logo' => '免费设计徽标',
		'costum_theme' => '自定义模板',
		'package_btn' => '现在订单',
		'package_btn_toko' => '创店',
		'satisfaction' => array(

				'title'	=> '满意感保证',
				'content' 	=> '您的成功就是我们的成功。'
			),

		'question_answer' => array(

				array(
					'title' => '安装成本',
					'content' => '没有'
 					),

				array(
					'title' => '成本受审贾维斯商店',
					'content' => '没有'
 					),

				array(
					'title' => '关闭您的帐户，并停止用户包',
					'content' => '您可以通过电子邮件我们关闭您的帐户'
 					),

				array(
					'title' => '多长时间的合同？',
					'content' => '一个月，一年。如果超过一年只是告诉我们，获得一个特殊的价格:)'
 					),

				array(
					'title' => '折扣？',
					'content' => '是。我们给你10 ％-50％的折扣，如果你订购高级版或旗舰包了一年多。'
 					),

				array(
					'title' => '域？',
					'content' => '是。您可以使用自己的域名，或者你已经有了域。'
 					),

				array(
					'title' => '带宽？',
					'content' => '无。所有的包贾维斯商店无限的带宽。'
 					),

				array(
					'title' => '虚拟主机？',
					'content' => '您不必到托管为您的网站的想法。'
 					),

				array(
					'title' => '升级包',
					'content' => '是。你可以升级你想你的包随时随地。'
 					),

				array(
					'title' => '如果我有我的网上商店特殊的要求？',
					'content' => '只需致电我们行 <a href="tel:+622261835535">+6222 618 355 35</a> 或电子邮件，我们将帮助您为您的要求做出网店。<a href="mailto:info@jarvis-store.com">info@jarvis-store.com</a>'
 					),


			),


	);
