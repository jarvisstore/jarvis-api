<?php
return array(

    'title'                 => '创建在线商店',
    'title_second'          => '卖任何设备，任何时间',
    'title_description'     => '永远免费',
    'register_btn'          => '创建商店',
    'register_description'  => '不到3分钟卖出产品上线！',
    'register_btn'          => '创建商店',
    'register_term'         => '通过创建账户，这意味着您与我们的服务政策达成一致 <a href="http://jarvis-store.com/kebijakan">Service Policy</a>.',
    'video_btn'             => '查看介绍视频',
    'testimonial'           => '什么我们的客户说关于我们',
    'products_title'        => '一些受欢迎的产品',
    'cta_title' 		    => '准备开始自己的网上商店？',
    'cta_description' 		=> "现在，让我们开始没有任何费用",
    'intro'                 => array(
            'main_title'     => '你需要的一切，使网上商店',
            'main_description'     => '现在，你不必费心再创建一个在线商店。随着贾维斯商店, 为您的网上商店的所有需求可以在这里找到。因为我们始终？听到你需要的东西。',
            'first_option'     => '很容易和快速',
            'first_desctiption'     => '不到3分钟，你可以已经有了自己的网上商店。, 不需要是一个技术高手还是程序员，都设计有干净和简单。',
            'second_option'     => '付款方式为您的商店',
            'second_desctiption'     => '银行转帐，信用卡，PayPal ， Ipaymu和Dokuku. 所有我们提供方便的付款方式。你可以更专注于销售你的产品。',
            'thrid_option'     => '酷设计模板',
            'thrid_desctiption'     => '你可以选择在你的风格在线商店的模板。 想更个性化的模板？我们可以帮助做到这一点。',
            'fourth_option'     => '自由',
            'fourth_desctiption'     => '永久免费！是的，免费模板，免费电，并建立你的网上商店免费永远。',
    ),
    'progress_setup'        => '创建你的店..',

);
