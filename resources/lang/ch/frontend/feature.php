﻿<?php
return array(
	'seo' => array(
		'title' => '贾维斯存储：即时在线商店与全功能服务',
        'description' => 'fitur : template responsive, SEO, ongkir otomatis, member, reseller, diskon kode dan kupon, laporan produk, laporan pelanggan, laporan transaksi, inport ekspor data, statistoc web',
        'keywords' => 'jasa toko online murah, jasa toko online instan, fitur responsive, SEO, fitur toko online, fitur toko online gratis, fitur jarvis store, toko online terintegrasi jne, toko online responsif, toko online responsive, laporan toko online, pembayaran toko online, testimonial toko online, statistik toko online, statistik web, email marketing,  toko online blog, notifikasi toko online, '
	),
	'title' 				=> '特点贾维斯',
	'title_description' 	=> '从不采取轻松的在线商店，只要将此',
	'cta_title' 		=> '您准备开网店？',
    	'cta_description' 	=> "现在就来试试不收取任何费用",
	'responsive_design' 	=> array(

		'title' 		=> '响应式设计',

		'feature_1' 	=> array(
				'title' 	=> '控制在你的手中',
				'content' 	=> '我们的每一个网上商店模板设计的响应，使客户访问和买东西在你的网上商店的任何地方，通过平板电脑甚至是智能手机。'
				),

		'feature_2' 	=> array(
					'title' 	=> '在任何地方和任何时间',
					'content' 	=> ' 除了你的网上商店，贾维斯商店的管理页面还使用了响应模板允许您控制，输入您的货物，甚至随时随地管理您的订单通过笔记本电脑，平板电脑或智能手机的。'
				),

		'feature_3' 	=> array(
				'title' 	=> '你的店铺的任何设备',
				'content' 	=> '经由平板电脑和智能手机网购的迅速增加使你必须有良好的界面在网页此设备。我们的界面将会给你的客户一个简单而有趣的在线购物体验。'
				),

		'feature_4' 	=> array(
				'title' 	=> '网站设计专家',
				'content' 	=> '自定义网页设计，我们的团队将能够帮助你找到你的网上商店最好的设计。只需发电子邮件至 <a href="mailto:info@jarvis-store.com">info@jarvis-store.com</a>.'
				),
	),

	'power_up'				=> array(

		'title'		=> '加电系统',

		'feature_1' => array(
			'title' => '现代化您的附加功能网站',
			'content' => "贾维斯Store的供电系统，可以让你的各种新功能，轻松地添加到您的网站。通过使用电，您可以添加新的营销功能，搜索引擎优化功能，折扣制度，等等。"
			),

		'feature_2' => array(
			'title' 	=> '集成综合博客平台',
		'content'	=> '此功能允许您直接从你的网上商店创建博客。文章将您的客户需要给他们有关产品的信息和知识。'
			)

	),

	'marketing_seo'			=> array(

			'title'		=> '营销和搜索引擎优化',

			'feature_1' => array(

				'title' => '搜索引擎优化',

				'content' => '你的客户应该能够通过像谷歌，雅虎和Bing搜索引擎来寻找你的网上商店。我们的CMS是支持SEO优化和搜索引擎友好的URL。'
				),

			'feature_2'	=> array(

				'title' => '优惠码和挂特点',
				'content' => "每个贾维斯Store的包已经集成券制度给予折扣为您的客户，以帮助您的产品推广。"
				),

			'feature_3'	=> array(

				'title' => '社会媒体整合',
				'content' => '在贾维斯商店的每一个网站已经集成了社交媒体，让您的客户分享并推荐你的产品在他们的社交媒体帐户。'
				),

			'feature_4'	=> array(

				'title' => '电子邮件营销',
				'content' => '贾维斯存储整合与Mailchimp电子邮件营销服务。它允许您创建美丽的电子邮件营销提供方便。'
				),

			'feature_5'	=> array(

				'title' => '完全统计量',
				'content' => '贾维斯商店拥有完整的分析功能来分析你的销售业绩和网站访问。这些信息可以让你决定要提高你的销售是最好的策略。'
				),

		),

	'management_member'		=> array(

			'title'		=> '客户管理',

			'feature_1'		=> array(

				'title' => '完整的客户管理系统',
				'content' => '贾维斯商店拥有完整的客户管理系统，可以让你看到关于客户交易，客户档案的所有报告，也可以创建发票。'

				),

			'feature_2'		=> array(

				'title' => '通知系统',
				'content' => "贾维斯Store的通知系统​​可以让您足不出户更新订单和付款确认。此外，该管理系统内部通知系统​​将帮助你从你的客户处理订单"

				),

			'feature_3'		=> array(

				'title' => '成员和转售商',
				'content' => '贾维斯商店有两种客户类型，它们的成员和经销商。如果您有与其他客户不同的价格基础经销商这会成材。'

				),

			'feature_4'		=> array(

				'title' => "客户推荐",
				'content' => '见证酷似您的客户满意度。贾维斯商店有这个系统，它允许您显示您的客户推荐你的网站。'

				),

			'feature_5'		=> array(

				'title' => '客户类别分析',
				'content' => '贾维斯Store允许您为您的客户集群分为几类。您可以根据地点，交易，或任何你需要的分类。'

				),
		),

	'shopping_cart'			=> array(

			'title'		=> '购物车',

			'feature_1' => array(

					'title' => '轻松购物',
					'content' => '在贾维斯店购物是超级容易和超级安全。您的客户可以在信心迅速开店'
				),

			'feature_2' => array(

					'title' => '定制送货送货',
					'content' => '除了JNE ，蒂基，和POS印度尼西亚，你还可以创建你用它来将产品交付给客户的costum运费。'
				),
			'feature_3' => array(

					'title' => '付款可通过银行汇款，支付宝',
					'content' => "贾维斯Store支持通过银行转账付款，也PayPal交易。这很容易从你的管理页面，激活"
				),
			'feature_4' => array(

					'title' => 'SSL认证',
					'content' => '如果你需要SSL（安全套接字层）证书，贾维斯商店提供SSL 128位，以保证您的信息加密。这SSL一样的是什么世界各地的主要银行使用。'
				),
		),

	'unlimited_hosting'		=> array(

			'title' 	=> '无限主机',

			'feature_1' => array(

					'title'	=> '使用自己的域名',
					'content'	=> '贾维斯商店允许您注册域名为您的网站。您可以使用我们的域名注册服务，用你拥有的域名，或者使用我们的免费域名。'

				),

			'feature_2' => array(

					'title'	=> '无限带宽',
					'content'	=> '无限带宽，无限数据传输。'

				),

			'feature_3' => array(

					'title'	=> '洒脱',
					'content'	=> "您的网站是由我们的系统自动创建的。你不必应付太多的事情。"

				),

			'feature_4' => array(

					'title'	=> '99.95 ％的正常运行',
					'content'	=> "我们正在与拥有最好的口碑正常运行的服务器的服务提供商合作，以确保您的网站保持在线"

				),

		),

	'support'				=> array(

			'title' => '24/7技术支持',

			'feature_1'	=> array(

					'title' => '聚焦客户端和产品',
					'content' 	=> "我们听你的。我们将提供给我们的团队你的回应。我们总是试图给我们最好的服务和支持，只为你。因为你值得无非是最好的。"
				),

			'feature_2'	=> array(

					'title' => '知识库',
					'content' 	=> '知识库贾维斯商店提供您需要了解系统，电子商务和互联网业务的各种信息。'
				),

			'feature_3'	=> array(

					'title' => '教程',
					'content' 	=> "不迷路！除了其易于使用的服务，贾维斯商店还提供了一步一步的tuorial是准备来指导你"
				),

			'feature_4'	=> array(

					'title' => '社区',
					'content' 	=> '我们有一个论坛，您可以讨论并与其他用户更改您的信息。'
				),
			'feature_5'	=> array(

					'title' => '支持渠道',
					'content' 	=> '有迹象表明，我们使用类似网络聊天和即时通讯应用程式，让您轻松与我们的团队，以交际许多支持渠道。'
				),
		),

	'reporting'				=> array(

			'title' => '报告和发票',

			'feature_1' 	=> array(

					'title' => '基于时间的报告',
					'content' => '此功能可让您根据您要时间是否每日，每周选择和创造交易报告，每月，甚至每年。.'

				),

			'feature_2' 	=> array(

					'title' => '进出口数据',
					'content' => '简化您的数据导入和备份使用.CSV我们的数据导入和导出系统。'

				),

			'feature_3' 	=> array(

					'title' => '一切的报告',
					'content' => '客户，产品，交易，收藏，还有更多报告。你需要的一切可以创建和备份只点击几下。'

				),

			'feature_4' 	=> array(

					'title' => '统计网站报告',
					'content' => '统计网站报告与信息图表关于你的整个网站的细节。'

				),
		),

);
