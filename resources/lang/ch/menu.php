﻿<?php

return array(

	'frontend' => array(

			'home' => '',
			'feature' => '特征',
            'portfolio' =>  '我们的客户',
            'blog'      =>  '博客',
			'pricing' => '价格',
			'service' => '服务',
			'template' => '模板',
			'power_up' => '充电',
			'tutorial' => '教程',
			'contact' => '联系',
			'partner' => '伙伴',
			'payment_confirmation' => '确认付款',
			'login' => '登录',

		),

	);
