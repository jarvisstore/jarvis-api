﻿<?php
return array(

	'title' 			=> '你的管理员登入账户',
	'input_email' 		=> '您的电子邮件',
	'input_password' 	=> '您的密码',
	'forget' 			=> '忘记',
	'forget_password' 	=> '忘记密码',
	'forget_button' 	=> '点击此处更改密码',
	'remember' 			=> '记住电子邮件地址和密码',
	'login_button' 		=> '登入'

);
