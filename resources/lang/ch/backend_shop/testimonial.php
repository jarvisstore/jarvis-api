﻿<?php
return array(

        'title'                 => '优惠数据',
        'list'                 => '优惠列表',
        'breadcrumb'    => array(
                'main' => '优惠',
                'create' => '增加新的优惠',
                'edit' => '编辑优惠',
        ),
        'form_input'  => array(
                'first_title' => '优惠数据',
                'first_description' => '优惠的标准数据',
                'name' => '客户名称',
                'testimonial' => '优惠',
                'display' => '显示',
        ),
        'no_data'       => '没有优惠数据'
);
