﻿<?php
return array(

        'title'                 => '收藏数据',
        'list'                 => '收藏列表',
        'breadcrumb'    => array(
                'main' => '收藏',
                'create' => '添加新收藏',
                'edit' => '编辑收藏',
        ),
        'form_input'  => array(
                'first_title' => '收藏数据',
                'first_description' => '标准收藏数据',
                'second_title' => '产品数据',
                'second_description' => '将产品添加到收藏',
                'name' => '收藏名称',
                'select_product' => '选择产品',
                'description' => '描述和Meta标签（SEO）',
                'image' => '图片',
                'url' => '网址与手柄？',
        ),
        'no_data'       => '没有收藏数据'

);
