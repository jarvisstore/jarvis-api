﻿<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "密码应采用六个字符和相同的密码确认。",

	"user"     => "我们不能使用这个电子邮件帐户找到",

	"token"    => "密码重置令牌是不正确的。",

);
