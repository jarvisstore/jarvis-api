﻿<?php

return array(

		'content' => array(
			'title' 	=> '即将推出',
			'description' 	=> '我们的网站将很快面市。欲了解更多信息，请与我们联系：',
			'contact_us' 	=> array(
				'title' 	=> '联系我们',
				'telp' 	=> '电话',
			),
			'find_us' 	=> "我们的位置",
			'address'	=> "地址"
		),
		'back' => '回报'

	);
