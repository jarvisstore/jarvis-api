﻿<?php

return array(

	'frontend' => array(

			'helper' => array(
                    'title' => '你应该知道',
					'feature' => '特点',
					'knowledgebase_1' => '什么是网上商店',
					'knowledgebase_2' => '网上购物交易',
					'knowledgebase_3' => '证券交易',
					'knowledgebase_4' => '营销和搜索引擎优化',
					'knowledgebase_5' => '什么自适应网站',
					'knowledgebase_6' => '在线业务',
					'knowledgebase_7' => '关于响应视频网站',
					'knowledgebase_8' => '.ID TLD注册要求',
					'knowledgebase_9' => '快速搜索引擎索引提示',
				),
			'jarvis_store' => array(

					'team' => '关于我们',
					'career' => '事业',
					'press' => '按',
					'portfolio' => '我们的客户',
					'term' => '服务条款',
					'sitemap' => '网站地图',
					'partner' => '合作伙伴计划',
					'support' => '背书',
					'blog' => '博客',
					'tutorial' => '教程',
					'faq' => '常问问题',
					'press_kit' => '新闻资料袋',

				),
			'quick' => array(

					'quick_contact' => '快速联系',
					'phone' => '电话',
					'chat' => '短信/聊天',
					'bb' => '黑莓脚',
				),
			'service' => array(

					'service' => '服务',
					'template' => '模板',
					'powerup' => '充电',
					'partner' => '伙伴',
				),
			'demo' => array(

				'shop_demo' => '示范店',
				'admin_demo' => '演示管理员',

				),

			'newsletter' => array(
				'title' => '电子邮件通讯',
				'content'	=> '信息，提示，技巧和其他有吸引力的优惠',
				'input' => '你的邮件 ..'
				),
			'payment' => '我们接受的付款',
		),
        'dont_show' => '隐藏这面旗帜？',
        'subscribe' => '订阅',
		

	);
