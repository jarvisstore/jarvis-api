﻿<?php

return array(

	'title' 	=> '完成 - 订单信息',
	'thanks' 	=> '谢谢',
	'buy_to' 	=> "已经逛过我们。",
	'total'	=> "总价格的购物车",
	'id'	=> "ID顺序",
	'notif'	=> "您的订单数据已成功交付。电子邮件，其中包含该订单并应该做下一步的信息，已发送到您的电子邮件地址。",
	'payment_way'	=> "付款方式",
	'bank_trans'	=> "请银行转帐付款到以下帐户之一",
	'confirm_info'	=> "在付款后请在这里确认您的付款",
	'confirm_btn'	=> "付款确认",
	'paypal'	=> '请通过PayPal支付网关使用PayPal支付你在网上。如果付款前（ 2X24小时）取得本次交易适用。点击“支付使用PayPal ”的继续付款流程。',
	'paypal_btn'	=> '用PayPal支付',
	'doku'	=> ' 请通过数独MyShortCart付款。如果付款没有作出1x24小时的交易将被取消。',
	'doku_btn'	=> '与铜工MyShortCart支付',
	'ipaymu'	=> '请跟iPaymu付款。点击“ iPaymu ”的继续付款流程',
	'ipaymu_btn'	=> '与iPaymu支付',
	'bitcoin'	=> '请用比特币付款。点击“用比特币支付”的继续付款流程。您的付款截止时间为1小时。',
	'bitcoin_btn'	=> '通过比特币支付',
	'cod'	=> "请在接收位置的产品准备以现金付款。",
	'veritrans' => '请跟Midtrans付款。点击“Veritrans”的继续付款流程。',
	'veritrans_btn'	=> 'Midtrans',

);
