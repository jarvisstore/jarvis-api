﻿<?php

return array(

	'title' 	=> '付款方式',
	'details' 	=> "选择一项付款方式：",
	'transfer'	=> "银行汇款",
	'cc'	=> "信用卡",
	'bank'	=> "银行",
	'no_account'	=> "帐号",
	'bank_account'	=> "在名称",
	'paypal_done'	=> "请通过PayPal支付网关使用PayPal支付你在网上。如果付款是由最大（ 2X24小时）本次交易适用。 ",
	'ipaymu_done'	=> "请与您的ipaymu一个在线支付。如果付款是由最大（ 2X24小时）本次交易适用。 ",
	'doku_done'	=> ' DOKU MyShortCart是一个支付服务，是快速和容易的产品线上。通过点击一个按钮，提供给处理后续款项将前往安全支付页面。数独付款MyShortCart可通过（任选其一）(<a href="http://doku.com/my-short-cart" target="_blank">Doku MyShortCart</a>)',
	'bitcoin_done'	=> "请与您的比特币付款网上。如果付款是由最大（1小时）本次交易适用。 ",
	'cod_done'	=> "COD代表货到付款是支付交易的产品的定购，当客户收到产品的。请准备在收到产品时支付现金或付款证明。",
	'midtrans' => "通过选择midtrans，可以在作出提供诸如多的支付渠道支付：",
	'continue'	=> "进一步",
	'back'	=> "回报",

);
