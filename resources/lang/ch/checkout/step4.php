﻿<?php

return array(

	'title' 	=> '订单汇总',
	'bank' 	=> "通过银行转账",
	'paypal'	=> "通过Paypal",
	'cc'	=> "通过信用卡",
	'ipaymu'	=> "通过IpayMu",
	'bitcoin'	=> "通过比特币",
	'cod'	=> "通过COD（货到付款）",
	'finish'	=> "完成预定",
	'back'	=> "回报"

);
