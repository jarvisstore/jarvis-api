﻿<?php

return array(

	'title' 	=> '1个产品已成功添加到您的购物车',
	'trash' 	=> '点击离篮筐删除产品',
	'totals' 	=> "总价值购物",
	'cart'	=> "篮",
	'continue'	=> "继续购物",
	'product'	=> "产品"

);
