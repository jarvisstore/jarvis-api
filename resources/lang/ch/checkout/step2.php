﻿<?php

return array(

	'title' 	=> '买家资料和交付',
	'details' 	=> "细节购物中心",
	'name'	=> "名",
	'email'	=> "电子邮件",
	'address'	=> "地址",
	'country'	=> "国家",
	'select_country'	=> "选择国家",
	'state'	=> "省",
	'select_state'	=> "选择省",
	'city'	=> "城市",
	'select_city'	=> "选择城市",
	'town'	=> "街道",
	'select_town'	=> "选择街道",
	'poscode'	=> "邮政",
	'phone'	=> "电话/手机",
	'messege'	=> "信息",
	'recipient'	=> "数据接收",
	'same_recipient'	=> "与数据买方数据接收机",
	'diff_recipient'	=> "数据不同的收件人",
	'name_recipient'	=> "收件人的姓名",
	'address_recipient'	=> "收件人",
	'phone_recipient'	=> "电话听筒",
	'back'	=> "回报",
	'continue'	=> "进一步",

);
