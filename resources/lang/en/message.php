<?php
return array(
    'register'     => array(
        'success' => array(
            'title'  => 'Your shop have made successully!',
            'body'   => 'Congratulations, your store have successfully to setup!',
            'button' => 'Click here to continue to your store!'
        ),
        'error'   => array(
            'ip-limit' => 'Opps, please register again in 5 minutes.'
        ),
    ),
    'login-member' => array(
        'error'   => array(
            'email'     => "Sorry, we could not find an account with this email.",
            'password'  => 'Plese try again, you enter the wrong password.',
            'not_found' => 'Sorry, account not found.',
        ),
        'success' => '<h3 class="lead">Welcome</h3> <h1>:obj</h1><br><img src="https://s3-ap-southeast-1.amazonaws.com/cdn2.jarvis-store.com/frontend/assets/images/icon/selamat-datang-login.png"><br><br><p class="lead" style="text-transform: lowercase;">we found your account</p>',
        'auto_logout' => "You have been logout automaticaly."
    ),

    'success'      => array(
        'create'               => 'Successfully create new :obj. ',
        'upload'               => 'Successfully upload new :obj . ',
        'update'               => 'Successfully update :obj . ',
        'delete'               => 'Successfully delete :obj',
        'reset'                => 'Successfully reset :obj',
        'install_theme'        => 'This theme successfully installed in your store.',
        'install_power_up'     => 'This power up successfully installed in your store.',
        'payment_confirmation' => 'Your payment confirmation has been sent.',
        'non_activated'        => 'This :obj is successfully non active.',
        'activated'            => 'This :obj is successfully active.',
        'register'             => 'Horay.. Your website have successfully created, go to next step and you are ready to selling online now!',
        'forget-password'      => 'Please check your E-mail, the password to reset your password!',
        'reset-password'       => 'Your new password has changed, please login again!',
        'login' => 'Horay, you successfully login to your administrator store.',
        'logout' => 'Thank you, you already logout, please come back later...',
        'login_member' => 'Thanks, you successfully log in',
        'logout_member' => 'Thank you, you already logout, please come back later...',
        'reset_pass'               => 'Your password has been updated',
        'install-theme'               => 'Successfully install the theme .',
        'thanks_msg'               => 'Thank you, your message has been sent..',
        'connect_instagram'               => 'Instagram successfuly connected to your account.',
    ),

    'error'        => array(
        'create'               => 'Woops, failed to create new :obj.',
        'update'               => 'Woops, failed to update :obj.',
        'delete'               => 'Woops, failed to delete :obj.',
        'exist'                => 'Woops, this :obj is already in system.',
        'size'                 => 'Woops, there are not available enough space for the :obj',
        'install_theme'        => 'Failed to install this Theme, please try again.',
        'install_power_up'     => 'Failed to install this Power-up, please try again.',
        'limit_install_power_up'     => 'Please upgrade your account to install powerup.',
        'payment_confirmation' => 'Your payment confirmation could not to sent, please try again!',
        'login' => array(
            'email' => 'We could not find your E-mail',
            'password' => 'you enter the wrong password.',
            'account' => 'This account with this email was not found.',
            'not_active' => 'This account is not active',
            'permission' => 'You are not authorized to accses this page.'
        ),
        'reset_pass'               => 'Woops, your code for reset password is not found.',
        'fail_pass'               => 'Woops, failed to reset your passowrd.',
        'fail_recover'               => 'Woops, your recovery code is not found, plase back to forgot password page.',

    ),
    'admin'      => array(
        'set_template'               => 'Sorry, you only able install 2 themes. Please upgrade your account to able install more themes.',
        'upload'               => 'Successfully upload new :obj. ',
        'update'               => 'Successfully update :obj . ',
        'delete'               => 'Successfully delete :obj',
        'file_notfound'               => 'This file is not found (404)',
        'required'               => 'Please fill all input, it can not be empty',
        'blog' => array(
            'required'               => 'Please fill all input, it can not be empty',
            'unique'               => 'Title already exist, please try another one',
            'category'         => 'Blog Categories',
        ),
        'discount' => array(
            'discount'         => 'Discount',
            'unique'         => 'Discount code already exist, try with another code.',
            'between'         => 'Discount cant more than 100 %',
        ),
        'shipping' => array(
            'package'         => 'Shipping Pack',
            'set_package'         => 'Shipping',
            'wrong_package'         => 'Opss, Wrong shipping pack..'
        ),
        'pages' => array(
            'page'         => 'Pages',
        ),
        'link' => array(
            'max'         => 'The maximum number of link groups are allowed in the free package is: obj , please upgrade your package.',
        ),
        'category' => array(
            'cat'         => 'Category',
            'sort'         => 'Order of Category',
            'max'         => 'Category name maximal 200 char.',
            'alpha_num_space'         => 'Category name contain invalid characters.',
        ),
        'collect' => array(
            'col'         => 'Collection',
            'create'         => 'Successfully create new Collection. <br>Please add product into this collection',
            'edit'         => 'Successfully update this Collection. <br>Please check product in this collection',
        ),
        'costumer' => array(
            'order'         => 'Orders',
            'cos'         => 'Costumer',
            'wrong'         => 'Costumer Data is wrong, please try again',
            'unique'         => 'This email already used as a member/partner',
        ),
        'setting' => array(
            'mall'         => 'Upgrade your Package for use feature Jarvis Mall !',
            'laporan'         => 'Please send the message to us!',
        ),
        'powerup' => array(
            'trusklik_success'         => 'Your website is successfully integrated with TrustKlik',
            'trusklik_error'         => 'Some error found use this domain.',
            'trusklik_domain'         => 'Your domain is already registered in TrustKlik!',
            'trusklik_email'         => 'Your email is invalid.',
        ),
        'product' => array(
            'img'         => 'Image Product',
            'pro'         => 'Product',
            'mall_pro'         => 'Mall Product',
            'name'         => 'Product name must required',
            'csv'         => 'Please complete your data product in csv file at',
            'max'         => 'Opps, Jumlah produk anda sudah mencapai maximal produk untuk paket toko anda',
            'laporan'         => 'Please send the message to us!',
            'image'         => 'Opps, please upload your image product',
            'max_image'         => 'You only can upload 4 image to show your product',
        ),
        'slide' => array(
            'max'         => 'You only can max upload 5 slideshow.',
        ),
        'rates' => array(
            'rate'         => 'Rate',
        ),
        'close' => 'Close',
        'upgrade' => 'Upgrade',
    ),
    'frontend'      => array(
        'shopcart' => array(
            'coupon_notfound'         => 'Please check again your code, your coupon is not found.',
            'coupon_expired'          => 'Sorry, your coupon has expired.',
            'coupon_notavail'         => 'Sorry, your coupon is not avaiable.',
            'coupon_minimum'          => 'This coupon need total order :obj',
            'new_error'                 => 'Sorry, this coupon only can used by New Member.',
            'once_error'                => 'Sorry, this coupon only can used once.',
            'exsis_error'               => 'Sorry, this coupon only can used by Member with have order before.',
            'user_error'                => 'Sorry, please compleate buyer data first.',
            'shipping_notfound'       => 'Shipping not found from',
            'destination'             => 'to',
            'shipping_list'           => 'Shopping list',
            'here'                    => 'here',
            'info_shipping'               => 'for information for shipping plese contact us',
            'member_email'               => 'your email address has registered. Please login or use other email.',
        ),
        'contact' => array(
            'success'         => 'Your message has been sent.',
            'failed'         => 'Opps, failed to sent the message.',
        ),
        'testimonial' => array(
            'success'         => 'Congratulations, your testimonial is successfully added and will displayed by admin.',
        ),
    ),
);