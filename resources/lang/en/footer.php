<?php

return array(

	'frontend' => array(

			'helper' => array(
                    'title' => 'INFORMATION',
					'feature' => 'JARVIS STORE FEATURES',
					'knowledgebase_1' => 'What is online store',
					'knowledgebase_2' => 'Online transaction',
					'knowledgebase_3' => 'Online security',
					'knowledgebase_4' => 'Online Ads and SEO',
					'knowledgebase_5' => 'What is responsive site',
					'knowledgebase_6' => 'Lets GO online',
					'knowledgebase_7' => 'Video about responsive site',
					'knowledgebase_8' => '.id TLD registration requirement',
					'knowledgebase_9' => 'Search engine indexing tips',
				),
			'jarvis_store' => array(

					'team' => 'About Us',
					'feature' => 'Features',
					'career' => 'Career',
					'press' => 'Press',
					'portfolio' => 'Portfolio',
					'term' => 'Service Policy',
					'sitemap' => 'Sitemap',
					'partner' => 'Partner Programs',
					'support' => 'Supports',
					'blog' => 'Blogs',
					'tutorial' => 'Tutorial',
					'faq' => 'F.A.Q',
					'press_kit' => 'Press Kit',
				),
			'quick' => array(

					'quick_contact' => 'Quick Contact',
					'phone' => 'Phone',
					'chat' => 'SMS/Chat',
					'bb' => 'Pin Blackberry',
				),
			'service' => array(
					'service' => 'Service',
					'template' => 'Templates',
					'powerup' => 'Power Up / Apps',
					'partner' => 'Affiliation / Partner',
				),
			'footer' => array(
				'price' => 'Pricing',
				'template' => 'Web Design',
				'powerup' => 'Powers Up / Apps',
				'partner' => 'Affiliation / Partner',
				'mobile_apps' => 'Mobile Apps',
			),
			'demo' => array(

				'shop_demo' => 'Demo Store',
				'admin_demo' => 'Demo Administrator',

				),

			'newsletter' => array(
				'title' => 'EMAIL NEWSLETTER',
				'content'	=> 'Get special offer, information, tips and trick',
				'input' => 'Your Email..'
				),
			'payment' => 'Payment We Accept :',
		),
        'dont_show' => 'Hide this banner?',
        'subscribe' => 'Subscribe',


	);
