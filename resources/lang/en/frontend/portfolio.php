<?php

return array(
		'seo' => array(
			'title' => 'Jarvis Store Portfolio',
	        'description' => 'Sebagian toko online yang menggunakan jarvis store - kisah sukses para pengguna jarvis store.',
	        'keywords' => 'jarvis store user, portfolio, Klien Jarvis Store, Jarvis Store Portfolio, contoh toko online, portfolio jarvis, toko online responsif, toko online jarvis store',
		),
		'title' => 'FEATURED PORTFOLIO',
		'title_description' 	=> 'Some of our clients.',
		'cta_title' => 'ARE YOU READY<br> TO OPEN<br> ONLINE STORE?',
		'cta_btn' => 'Start Now',
		'portfolio_title' => 'OUR COSTUMERS:',
		'portfolio_footer' => '... AND OTHER 40.000+ ONLINE STORES ...',
		'other_client' => 'Other Jarvis Store Client',

	);
