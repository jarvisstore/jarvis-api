<?php

return array(

		'title' => 'About Us',
		'title_description' 	=> 'JARVIS STORE is an e-commerce platform designed to work across devices',
		'description' 	=> 'Our services includes the development of e-commerce platform, education, digital strategy, design, technology and media. Online store solution that has full features and make your brand into the digital business.',
		'description_1' 	=> 'Jarvis Store (​www.jarvis-store.com​) is an e-commerce platform to create online store designed to work across device like Desktop, Tablet, and Smartphone. Jarvis Store is user friendly with many features to support online business.',
		'description_2' 	=> 'With tagline “Every One Can Sell Online”, our comitment is to provide the best online store for online seller. With Jarvis Store, every body can sell on internet, upgrade their bussiness and create wider opportunity.',


	);
