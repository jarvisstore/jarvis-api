<?php

return array(

		'403' => array(
			'title' 	=> 'Opps, this page cannot accessed.',
			'content' 	=> "We are so sorry you don't have authorized to access this page, please contact us for more information about this page."
			),
		'404' 	=> array(
			'title' 	=> 'Opps, this page is no longer available.',
			'content' 	=> "We couldn't find the page you requested on our servers. We're really sorry
						about that. It's our fault, not yours. We'll work hard to get this page
						back online as soon as possible."
			),
		'500' 	=> array(
			'title' 	=> 'Opps, something went wrong.',
			'content' 	=> "We're working on getting this fixed as soon as we can."
			),
		'back' => 'Go Back'

	);
