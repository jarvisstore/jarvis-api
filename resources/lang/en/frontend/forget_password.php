<?php

return array(

    'title' => 'RESET PASSWORD',
    'title_description' 	=> 'Please type your website address and your email to find your account',
    'place_email' => 'Your email',
    'place_akun' => 'Your Website Address, eg: example.jstore.co',
    'button_success' => 'Go to Admin Store',

    'reset' => 'Reset Password',
    'reset_description' => 'Please type your new password',
    'new_password' => 'New Password',
    'confirm_new_password' => 'Re-type New Password',
);
