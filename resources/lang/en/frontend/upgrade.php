<?php
return array(
    'title' => 'Upgrade Your Package',
    'description' => 'Please fill your Jarvis Store account, email and choose your package.',
    'thanks' => 'Thank You :)',
	'heading_thanks' => 'Your Store and Account already closed',
	'heading_close' => 'We are sad your store has close, if you have comments and suggestions please send it via email to us at <a href="mailto:info@jarvis-store.com">info@jarvis-store.com</a>.',
);