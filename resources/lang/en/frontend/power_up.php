<?php

return array(
		'seo' => array(
			'title' => 'Jarvis Store - Power-up sebagai fitur tambahan untuk toko online anda',
	        'description' => 'Jarvis Store memiliki template responsif yang siap dipakai dan diubah sesuai keinginan.',
	        'keywords' => 'jarvis store template, responsif template, responsif design, template toko online gratis',
		),
		'title' => 'POWER-UP Store',
		'title_description' 	=> 'List Power-Up / Apps Jarvis Store',
		'powerup_title' => 'Power-up / Apps are additional plugins or features that can be embedded to your online shop. Various power-ups, from free to payable, are here.',
		'powerup_footer' => 'MORE POWER-UP / APPS COMING SOON',

	);
