<?php

return array(

    'title' => 'LOGIN TO YOUR STORE',
    'title_description' 	=> 'Enter your email and password',
    'place_email' => 'Your Email',
    'place_password' => 'Your Password',
    'button_success' => 'Click here to continue to your shop.',
	'click_password' 		=> 'Forgot your password? <a href="'.URL::to("forget-password").'">Click here</a>',

	'setup_info' 		=> 'Please fill all data below to continue to next step',
    'store_name' 		=> 'Your Shop Name',
    'owner_name' 		=> 'Your Name',
    'info_address' 		=> 'This data will show as shop contact information ',
    'shop_phone' 		=> 'Phone',
    'code_post' 		=> 'Post Code',
    'shop_address' 		=> 'Address',
    'shop_category' 		=> 'Shop Categories',
    'select_category_shop' 		=> '-- Select catagory --',
    'agent_code' 		=> 'Agent Code (Optional)',
    'go_to_shop' 		=> 'Go to Shop Dashboard',
    'term_condition' 		=> 'By continue this step, you will agree with our<a target="_blank" href="http://www.jarvis-store.com/kebijakan"> term and privacy</a>.',

);
