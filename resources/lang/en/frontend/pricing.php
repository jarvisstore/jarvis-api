<?php

return array(
		'seo' => array(
			'title' => 'Jarvis Store - Harga Paket Jarvis Store Mulai 39ribu',
	        'description' => 'Berbagai pilihan paket jarvis store sesuai kebutuhan toko online anda.',
	        'keywords' => 'harga pembuatan toko online, toko online murah, jasa pembuatan toko online, toko online instan, harga jarvis store, toko online instan'
		),
		'title' => 'PRICE',
		'title_description' 	=> 'Upgrade Your Online Shop Whenever You Want',
		'choose_plan' 	=> 'Choose Your Plan',

		'price_title' => 'SUBSCRIBE FOR A YEAR AND GET FREE 2 MONTH',
        	'price_title2' => 'Subscribe When Your Shop is Grow',
		'price_footer' => 'Need Specifications and Customization for your Online Store ? <a href="mailto:info@jarvis-store.com">Contact Us</a> for consultation.',
		'domain' => 'Free Domain',
		'7day' => 'First 7 day',
		'30day' => 'First 30 day',
		'1year' => 'Minimum 1 year subscription',
		'all_feature' => 'See all features',
		'package' => array(
			'priority' => 'Priority Support',
			'qty_product' => 'Product Capacity',
			'qty_storage' => 'Storage Capacity',
				'ligth' => array(

						'title' 		=> 'MINI',
						'description' 	=> 'Best for mini online store with a litte amount of product',
						'price_monthly'	=> '<small>IDR</small> 39.000 <small>/ Month</small>',
            			'price_yearly'	=> '<small>IDR</small> 390.000 <small>/ Year</small>',
						'total_product'	=> '50 Produk',
						'total_storage' => '150MB Storage',
						'reporting'		=> 'Full Report For Orders, Product and Payment',
						'analytic'		=> 'Statistic Report (Analytics)',
						'support'		=> 'Priority Support 3'
					),
				'starter' => array(

						'title' 		=> 'STARTER',
						'description' 	=> 'Best for small scale online store.',
						'price_monthly'	=> '<small>IDR</small> 99.000 <small>/ Month</small>',
                        'price_yearly'	=> '<small>IDR</small> 990.000 <small>/ Year</small>',
						'total_product'	=> '200 Product',
						'total_storage' => '500MB Storage',
						'reporting'		=> 'Full Report Orders',
						'analytic'		=> 'Statistic Report (Analytics)',
						'support'		=> '<small>Priority Support 3</small>',
						'admin'			=> '1 additional Admin'
					),
				'premium' => array(

						'title' 		=> 'PREMIUM',
						'description' 	=> 'Best for medium online store.',
						'price_monthly'	=> '<small>IDR</small> 199.000 <small>/ Month</small>',
                        'price_yearly'  => '<small>IDR</small> 1.990.000 <small>/ Year</small>',
						'total_product'	=> '1000 Product',
						'total_storage' => '1.5GB Storage',
						'reporting'		=> 'Full Report For Orders, Product and Payment',
						'analytic'		=> 'Statistic Report (Analytics)',
						'support'		=> '<small>Priority Support 2</small>',
						'template' 		=> 'Free Premium Template',
						'admin'			=> '9 additional Admin'
					),
				'ultimate' => array(

						'title' 		=> 'ULTIMATE',
						'description' 	=> 'Best for large online store.',
                        'price_monthly'	=> '<small>IDR</small> 499.000 <small>/ Month</small>',
                        'price_yearly'  => '<small>IDR</small> 4.990.000 <small>/ Year</small>',
						'total_product'	=> 'Unlimited Product',
						'total_storage' => '4GB Storage',
						'reporting'		=> 'Full Report For Orders, Product and Payment',
						'analytic'		=> 'Statistic Report (Analytics)',
						'support'		=> '<small>Priority Support 1</small>',
						'admin'			=> 'Unlimited Admin'
					),
				'telkomsel' => array(

						'title' 		=> 'TELKOMSEL PACKAGE',
						'description' 	=> 'Jarvis Store package plan for Telkomsel costumers',
						'total_product'	=> '200 Product',
						'total_storage' => '500MB Storage',
						'internet' 		=> '2 Gb Internet Data *',
						'group_call' 	=> 'Free talk *',
						'sms' 			=> 'Free SMS *',
						'telkomsel_call'=> '100 minutes taking between Telkomsel numbers *',
						'reporting'		=> 'Complete report',
						'analytic'		=> 'Statistics report',
						'support'		=> 'Unlimited Support',
						'syarat'		=> '* service & policy base on pakage form Telkomsel'
					),
				'free' => array(

						'title' 		=> 'FREE',
						'description' 	=> "Let's start selling in your online store",
						'tag_line' 		=> 'Learning to Sell Online',
						'price_monthly'	=> '<small>IDR</small> 0 <small>/ Month</small>',
						'price_yearly'	=> '<small>IDR</small> 0 <small>/ Year</small>',
						'total_product'	=> '10 Product',
						'total_storage' => '35MB Storage',
						'support'		=> '<small>Limited Support</small>'
					),
				'enterprise' => array(

						'title' 		=> 'ENTERPRISE',
						'description' 	=> 'Big scale online shop with full customization without limits',
						'price_monthly'			=> '-- CALL --',
					),
			),
		'compare' => 'Package Comparison',
		'bandwidth' => 'Unlimited Bandwidth',
		'discount' => 'Free Discount Code Power-Up',
		'html' => 'HTML/CSS Editor',
		'support' => 'Support HTML/CSS Edit',
		'premium_theme' => 'Free Premium Template',
		'free_logo' => 'Free Design Logo',
		'costum_theme' => 'Custom Template',
		'package_btn' => 'Order Now',
		'buy_btn' => 'Buy',
		'package_btn_register' => 'Register Now',
		'change_btn' => 'Subscribe',
		'continue_btn' => 'Continue',
		'package_btn_toko' => 'Create Shop',
		'faq' => 'Frequently asked questions of JARVIS-STORE.COM',
		'more_question' => 'More Question',
		'satisfaction' => array(

				'title'	=> 'SATISFATION GUARANTEED',
				'content' 	=> 'YOUR SUCCESS IS OUR SUCCESS ALSO.'
			),

		'question_answer' => array(
				'faq_1' => array(
					'title' => 'How much cost for installation ?',
					'content' => 'No cost for instalation.'
 				),

				'faq_2' => array(
					'title' => 'Must I paid to using Jarvis Store ?',
					'content' => 'No, you can start Jarvis Store from Free .'
 				),

				'faq_3' => array(
					'title' => 'How long my website online after fisnish order ?',
					'content' => 'After create order check our email to see the invoice and make a payment, and <a target="_blank" href="//jarvis-store.com/konfirmasi-pembayaran">Confirmation your payment in our website</a> and your website will online after we receive the payment.'
 				),

				'faq_4' => array(
					'title' => 'How to get Discount ?',
					'content' => 'Yes. We have event every month please check our blog <a target="_blank" href="//jarvis-store.com/artikel/info/8/Event/">event</a>, and also if you order for PREMIUM or ULTIMATE package for a year and more.'
 				),

				'faq_5' => array(
					'title' => 'How to using Domain (.com or etc) ?',
					'content' => 'You can use your own domain if you have, or Order Domain in Jarvis Store.'
 				),

				'faq_6' => array(
					'title' => 'How to create online store in Jarvis Store ?',
					'content' => 'Click button Crete Store in bottom and fill your Shop Name, Email Address, and Password then follow next step to manage your shop.'
 					),

				'faq_7' => array(
					'title' => 'What is the difference each package in Jarvis Store ?',
					'content' => 'Each package have difference product storage, file storage, active feature and support priority.'
 					),

				'faq_8' => array(
					'title' => 'How to Upgrade Package ?',
					'content' => 'Any time and Any where just tell us and we will give you special price :)'
 				),

				array(
					'title' => 'Close your account and stop package subscriber',
					'content' => 'you can email us for close your account'
 					),

				array(
					'title' => 'How to order package',
					'content' => 'you can choose for subscriber each Month or Year. If more than a Year just tell us and get a Special Price :)'
 					),

				array(
					'title' => 'How to get my own email Domain ?',
					'content' => 'You can get user email of your domain if you order from us, contact us for user email you will used.'
 					),

				array(
					'title' => 'How about Bandwidth?',
					'content' => 'Unlimited bandwidth for all package.'
 					),

				array(
					'title' => 'Web hosting?',
					'content' => 'You do not have to thinking hosting for your website, we will handle it and you can focus on selling :)'
 					),

				array(
					'title' => 'Have a Special Request ?',
					'content' => 'Just call us in line <a href="tel:+6282216655507">+6282216655507</a> or email <a href="mailto:info@jarvis-store.com">info@jarvis-store.com</a>, we will help you for your request to make a Online Shop.'
 					),


			),


	);
