<?php

return array(

	'title' 	=> 'Shopping Details',
	'empty_cart'	=> "<h1>No Product is added in your cart.</h1><br><br> <a class='btn btn-primary' href='../produk'>View our collection product</a>.",
	'address'	=> "Address",
	'img' => 'Images',
	'product' => 'Product',
	'qty' => 'Quantitiy',
	'price' => 'Price',
	'subtotal' => 'Subtotals',
	'shipping' => 'Shipping cost',
	'free_ship' => 'Free Shipping',
	'next_ship' => 'We will inform you for shipping',
	'discount' => 'Discount',
	'tax' => 'Tax',
	'tax_active' => 'Active',
	'tax_nonactive' => 'Non Active',
	'code' => 'Unique Code',
	'totals' => 'Totals',
	'coupons' => 'Have Coupon ?',
	'input_coupons' => 'Please insert your Code Coupon',
	'place_coupons' => 'Coupon',
	'use_coupons' => 'Apply',
	'shipping_cost' => 'Shipping Cost',
	'des_destination' => 'Select your destination to check shipping cost',
	'cancel' => 'Cancel',
	'search' => 'Search',
	'city_not_found' => 'If your destinantion is not found, you can select the nearest city or please <a href="../kontak">contact us</a>.',
	'free_ship_des' => 'You no need to pay shipping cost.',
	'login' => 'Continue as ?',
	'login_des' => 'Already member, sign in to easier make your order.',
	'forget_password' => 'Forget Password?',
	'new_member' => 'New costumer ? <a href="../member/create">Start here.</a>',
	'reg_member_des' => 'Continue as Member',
	'new_member_des' => 'Click button below to continue checkout!',
	'view_other' => 'View other Products',
	'back' => 'Back',
	'continue_as_guest' => 'Continue as Guest',
	'or_register_member' => 'or',
	'continue' => 'Next',
	'express_des' => 'Express Button help you to make your checkout more faster by auto filling your data base on your checkout history',
	'not_found' =>'Sorry, shipping not found',
	'not_found_shipping' =>'<center><h2>Sorry, shipping to your destination still not avaiable</h2><p><i>for more information please contact us <a href=":obj">here</a></i></p></center>',
	'tag_cancel' => 'Cancel',
	'tag_apply' => 'Apply',
);
