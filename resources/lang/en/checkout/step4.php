<?php

return array(

	'title' 	=> 'Order Summary',
	'bank' 	=> "Via Bank Transfer",
	'paypal'	=> "Via Paypal",
	'cc'	=> "Via Credit Card",
	'ipaymu'	=> "Via IpayMu",
	'bitcoin'	=> "Via Bitcoin",
	'cod'	=> "Via COD (Cash on Delivery)",
	'finish'	=> "Finish Order",
	'back'	=> "Back"

);
