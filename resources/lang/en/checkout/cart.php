<?php

return array(

	'title' 	=> 'This Product successfuly added to your cart',
	'trash' 	=> 'Click here to remove this product from your cart',
	'totals' 	=> "Totals",
	'cart'		=> "Go to Cart",
	'buy'		=> "Cart & Checkout",
	'continue'	=> "Continue Shopping",
	'product'	=> "Product"

);
