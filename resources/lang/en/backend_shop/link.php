<?php
return array(
        
        'title'                 => 'Data Link',
        'list'                 => 'List of Link',
        'add'                 => 'Add Link ',
        'page'                 => 'from Pages',
        'blog'                 => 'from Blogs',
        'url'                 => 'use URL',
        'helper'    => array(
                '1' => 'Select Name of Menu first',
                '2' => 'Or click here to add a new Menu Link',
                '3' => 'Click here to edit selected Menu',
                '4' => 'Select reference of Link (Pages, Blog or other URL) will added',
                '5' => 'edit the link have been created, to changes the order of link by click and drag to wanted posisiton.',
        ),
        'form_input'  => array(
                'page' => 'Add link from Pages',
                'blog' => 'Add link from Blogs',
                'url' => 'Add link with Other URL',
                'name' => 'Link Name',
                'add_group' => 'Add a new Menu',
                'update_group' => 'Update Menu',
                'name_group' => 'Menu Name',
                'select_page' => 'Select Pages',
                'select_blog' => 'Select Blogs',
                'delete_group' => 'Delete this menu ?',
                'delete_link' => 'Delete this link ?',
        ),
        'no_data'       => 'This Menu has no data link, please add a new link with 3 choice : from pages, from blogs, or by add other url.',
        'limit'         => 'A maximum number of link groups are: Obj, upgrade your package to add a link group more'
);
