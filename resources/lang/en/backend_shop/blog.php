<?php
return array(
        
        'title'                 => 'Data Blogs',
        'list'                 => 'List of Blog',
        'category'                 => '<i class=" fa-icon-list"></i> <span class="hidden-phone hidden-tablet"> Blog Category</span>',
        'link'                 => '<i class=" fa-icon-pushpin"></i> <span class="hidden-phone hidden-tablet"> Add Link to My Menu</span>',
        'author'                 => 'Author',
        'date'                 => 'Publish Date',
        'breadcrumb'    => array(
                'main' => 'Blog',
                'create' => 'Add new Posts',
                'edit' => 'Edit Blog',
                'category' => 'Edit Blog Categories',
        ),
        'form_input'  => array(
                'first_title' => 'Data Blogs',
                'first_description' => 'Basic Data of blog',
                'second_title' => 'Data Blog Categories',
                'title' => 'Title',
                'content' => 'Content',
                'category' => 'Category',
                'category_name' => 'Category Name',
                'add_category' => 'Add Category',
                'edit_category' => 'Edit Category',
                'tags' => 'Tags <span class="muted"> separated by comma (,)</span>',
        ),
        'no_data'       => 'No data Blogs'

);
