<?php
return array(
	'title' => 'Power-up / Apps (Plugin)',
	'powerup' => 'Power-up / Apps',
	'open_url' => 'Go to Website',
	'btn_powerup_market' => 'More Power-up ',
	'enable_power_up' => 'Enable this Power-up ?',
	'disable_power_up' => 'Disable this Power-up ?',
	'remove_power_up' => 'Remove this Power-up ?',
	'no_data' => 'No Power-up instaled in Your online store',
	'open_powerup' => 'Find',
);