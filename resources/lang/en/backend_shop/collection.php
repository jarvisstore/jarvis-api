<?php
return array(
        
        'title'                 => 'Data Collection',
        'list'                 => 'List of Collection',
        'breadcrumb'    => array(
                'main' => 'Collection',
                'create' => 'Add a new Collection',
                'edit' => 'Edit Collection',
        ),
        'form_input'  => array(
                'first_title' => 'Data Collection',
                'first_description' => 'Basic data of Collection',
                'second_title' => 'Data Product',
                'second_description' => 'Add product in to this Collection',
                'name' => 'Collection Name',
                'select_product' => 'Select Product',
                'description' => 'Discription & Meta tag (SEO)',
                'image' => 'Image',
                'url' => 'URL & Handle',
        ),
        'no_data'       => 'No data Collection'

);
