<?php
return array(
	
	'month_stat' 			=> 'Monthly Statistics',
	'visit' 			=> 'Visits',
	'pageview' 	=> 'Pageviews',
	'visitor' 			=> 'Visitors',
	'new_visitor' 	=> 'New Visitors',
	'welcome' 		=> 'Welcome to your Dashboard',
	'welcome_info' 			=> 'one more step and you are ready to sell in your online store',
	'step1' 		=> '1. Add Product',
	'step1_info' 		=> 'First step is add the product you want to sell in your online store',
	'step1_button' 		=> 'Add Product',
	'step2' 		=> '2. Update Display',
	'step2_info' 		=> 'The next step is to upload a logo image , slideshow , and banner',
	'step2_button' 		=> 'Update Content',
	'step3' 		=> '3. Choose Your Payment',
	'step3_info' 		=> 'Choose your payment method used bank transfers, paypal, or another payment',
	'step3_button' 		=> 'Setup Payment',
	'step4' 		=> 'Launching Online Shop',
	'step4_info' 		=> 'Online store you are ready to sell and start to accept a new orders',
	'step4_button' 		=> 'View Online Shop',
	'finish' 		=> 'FINISH',
	'quick_navigation' 		=> 'Quick Menu',
		'costumer' 		=> 'Costumers',
		'orders' 		=> 'Orders',
		'product' 		=> 'Product',
		'statistic' 		=> 'Statistics',
		'report' 		=> 'Report',
		'testimonial' 		=> 'Testimonials',
		'add_order' 		=> '+ Order',
		'add_category' 		=> '+ Category',
		'add_product' 		=> '+ Product',
		'add_discount' 		=> '+ Discount',
		'add_costumer' 		=> '+ Costumer',
		'add_blog' 		=> '+ Blog',
		'themes' 		=> 'Theme',
		'setting' 		=> 'Setting',
	'new_order' 		=> 'New Order',
	'order_id' 		=> 'Order ID',
	'name' 		=> 'Costumer Name',
	'date' 		=> 'Date',
	'total' 		=> 'Totals',
	'status' 		=> 'Status',
		'pending' 		=> 'Unpaid',
		'confirmation' 		=> 'Approved',
		'paid' 		=> 'Paid',
		'send' 		=> 'Sent',
		'cancel' 		=> 'Cancel',
	'no_order' 		=> 'There are <b>0</b> Orders for today...',
	'help'    => array(
                'step1' => 'Changes the order of category product by click and drag to adjust the position',
                'step2' => 'Click here to edit selected product',
                'step3' => 'Click here to delete selected category',
                'step4' => 'Click here to add a new category product',
                'step5' => 'This button to manage your product',
                'step6' => 'Ini button untuk melakukan pengaturan produk',
                'step7' => 'Click here to delete selected product',
                'step8' => 'Click here to add a new product',
        ),

);
