<?php
return array(
        
        'title'                 => 'Data Category',
        'list'                 => 'List of Category',
        'breadcrumb'    => array(
                'main' => 'Category Product',
                'create' => 'Add a new Category',
                'edit' => 'Edit Category',
        ),
        'form_input'  => array(
                'first_title' => 'Category Data',
                'first_description' => 'Basic information of Category',
                'name' => 'Name of Category',
                'parent' => 'Select Parent of Category',
                'main_parent' => 'As Main Category',
                'description' => 'Description & Meta tag (SEO)',
                'url' => 'URL & Handle',
        ),
        'help'    => array(
                'step1' => 'Changes the order of Category Product by click and drag',
                'step2' => 'Click to edit selected Category',
                'step3' => 'Click to delete selected Category',
                'step4' => 'Click to add a new Category',
                'step5' => 'Click to adjust the position of Category',
        ),
        'no_data'       => 'No data to display'

);
