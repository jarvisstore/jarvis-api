<?php
return array(
	
	'header' => array(
                    'tour' => 'Tour',
					'help' => 'Help',
					'system_overview' => 'System Overview',
					'disk_usage' => 'Disk Usage',
					'product_quota' => 'Product Quota',
					'goto_shop' => 'View Site',
					'profile' => 'Profile',
					'account_billing' => 'Account & Billing',
					'logout' => 'Logout'
				),
	'helper' => array(
                    'title' => 'Need help ?',
					'tutorial' => 'Tutorial',
					'video' => 'Video',
					'report_email' => 'Send to us',
					'report_deskription' => 'Report for bug, errors, etc',
					'live_chat' => 'Chat with us',
					'cancel' => 'Cancel',
				),
	'menu' => array(

			'dashboard' => 'Dashboard',
			'order' => 'Orders',
			'costumer' => 'Costumers',
			'catalog' => 'Catalog',
			'category' => 'Category',
			'product' => 'Product',
			'collection' => 'Collection',
			'discount' => 'Discount',
			'expedition' => 'Shipping',
			'write' => 'Write',
			'blog' => 'Blogs',
			'pages' => 'Pages',
			'link' => 'Links',
			'testimonial' => 'Testimonials',
			'report' => 'Report',
			'statistic' => 'Statistics',
			'power_up' => 'Power ups',
			'themes' => 'Themes',
			'payment' => 'Payments',
			'setting' => 'Setting',
		),
	'message' => array(
                    'free_pack' => 'Your Store Account using Free Pack',
					'info_pack' => 'Subscribe information, Please click here',
					'upgrade_pack' => 'Upgrade your pack ? Click here',
					'trial_pack' => 'Upgrade your package for full version',
					'reminder_pack' => 'Your official online store package has ended, you online store will only display 10 products and you can only use limited feature',
					'link_reminder_pack' => 'Extend your package and continue your full feature online store here',
					'notification' => 'March, 11 2016 on 21:00 until 24:00 WIB server will maintenance. <a href="https://jarvis-store.com/artikel/jadwal-maintenance-jumat-11-maret-2016">more info..</a>',
					'verify1' => '<h4>Notification!</h4>Hello, your email account is unverified, please check your email',
					'verify2' => ' in your inbox/spam and click url in email to verify your account.',
					'resend' => 'Resend verification email!',
        			'reminder_msg'  => 'Your plan will expired in next :obj days. Please go to Client Area to check Invoice, or you can chat with us <a id="247" class="blue"><strong>here</strong></a>. <br><br><a id="hide_notif" class="pull-right" style="cursor:pointer"><em>(close)</em></a>',
        			'notification'  => 'Notification!',
				),
	'button' => array(
                    'create' => '<i class="fa-icon-plus"></i> <span class="hidden-phone hidden-tablet">  Create</span>',
					'delete' => '<i class="fa-icon-trash"></i> <span class="hidden-phone hidden-tablet"> Delete</span>',
					'save' => 'Save',
					'export' => 'Export',
					'import' => 'Import',
					'reset' => 'Reset',
					'creating' => 'Add',
					'cancel' => 'Cancel',
					'download' => 'Download',
					'send' => ' Send',
					'see_tutorial' => 'Tutorial',

				),
	'report' => array(
        'report' => 'Report',
        'product' => 'Product Report',
	        'views' => 'Views ',
	        'sold' => 'Sold ',
	        'top_sell' => 'TOP 10 Sold',
	        'top_view' => 'TOP 10 Views',
        'order' => 'Order Report',
	        'order_total' => 'Total Orders',
	        'order_status' => 'Orders Status',
	        'top_city' => 'TOP 5 Order Area',
	        'top_costumer' => 'TOP 10 Costumer',
	        'top_shipping' => 'TOP 5 Shipping',
	        'no_costumer' => '<small>This costumer has been deleted</small>',
        'payment' => 'Payment Report',
        	'payment_method' => 'Payment',
        	'top_payment' => 'Popular Payment Method',
        'statistic' => 'Web Statistic Report',
	),
);
