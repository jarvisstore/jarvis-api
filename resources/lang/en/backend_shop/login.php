<?php
return array(
	
	'title' 			=> 'Your Login Account',
	'input_email' 		=> 'Your email',
	'input_password' 	=> 'Your password',
	'forget' 			=> 'Forgot',
	'forget_password' 	=> 'Forgot My Password',
	'forget_button' 		=> 'Click here to change password',
	'forget_button' 	=> 'Click here to get a new password.',
	'remember' 			=> 'Remember Email & Password',
	'login_button' 		=> 'Login',
	'forget_email' 	=> 'Forgot My Email',
	'get_email' 	=> 'Tell us your shop name !!',

);
