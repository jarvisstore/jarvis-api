<?php
return array(
        
        'title'                 => 'Data Testimonials',
        'list'                 => 'List of Testimonials',
        'breadcrumb'    => array(
                'main' => 'Testimonials',
                'create' => 'Add a new Testimonials',
                'edit' => 'Edit Testimonials',
        ),
        'form_input'  => array(
                'first_title' => 'Data Testimonials',
                'first_description' => 'Basic data of Testimonials',
                'name' => 'Costumers Name',
                'testimonial' => 'Testimonials',
                'testimonial_show' => 'Change to show status',
                'testimonial_invis' => 'Change to hidden status',
                'display' => 'Display',
        ),
        'no_data'       => 'No data Testimonials'
);
