<?php
return array(
        
        'title'                 => 'Shipping',
        'title_1'                 => 'Manage Shipping',
                'sub_title_1'                 => 'Expedition / Shipping System',
                'opsi_1'                 => 'Enable Shipping',
                'opsi_2'                 => 'Free Shipping',
                'opsi_3'                 => 'Manual Shipping',
                'sub_title_2'                 => 'Notes for Manual Shipping',
                'sub_title_3'                 => 'Select courier',
                'sub_title_4'                 => 'Origin (City)',
                'opsi_4'                 => 'Only use Costum Shipping (data shipping on right)',
                'opsi_5'                 => 'To display the sub-district choice, please update District Address on Setting Menu.',
                'btn_save_expedisi'                 => 'Save Shipping',
        'title_2'                 => 'Create Own Shipping',
                'add_expedisi'                 => 'Add costum shipping',
                'country'                 => 'Select Country',
                'title_tbl'                 => 'Costum shipping',
                'exspedisi_name'                 => 'Shipping Name',
                'create_new'                 => 'new name',
                'pack_name'                 => 'Package Name',
                'edit_price'                 => 'Edit Rates',
        'breadcrumb'    => array(
                'main' => 'Shipping',
                'create' => 'Add a new Shipping',
                'edit' => 'Edit Costum Shipping',
        ),
        'form_input'  => array(
                'first_title' => 'Data Costum Shipping ',
                'second_title' => 'Data Shipping',
                'thrid_title' => 'Set Rates',
                'to_city' => 'Destination (City)',
                'price_kg' => 'price per Kilogram',
                'write' => 'write your message here!',
                'add_price' => 'Add',
        ),
        'no_data_costum'       => 'No data Costum Shipping',
        'no_data_price'       => 'No data Shipping Rates',
        'use_shiping' =>        'Your online shop active using Local Shipping Courier, please check avaiable courier below !',
        'manual_shiping' =>        'Notification for cost shipping will pending send by admin online shop',
        'free_shiping' =>        'Free Shipping active in your online shop to all product dan all costumer'

);
