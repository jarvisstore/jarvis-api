<?php
return array(
        
        'title'                 => 'Data Pages',
        'list'                 => 'List of Pages',
        'link'                 => '<i class=" fa-icon-pushpin"></i> <span class="hidden-phone hidden-tablet"> Add Link to My Menu</span>',
        'breadcrumb'    => array(
                'main' => 'Pages',
                'create' => 'Add new page',
                'edit' => 'Edit Pages',
        ),
        'form_input'  => array(
                'first_title' => 'Data Page',
                'first_description' => 'Title and Content of Page',
                'title' => 'Title',
                'content' => 'Content',
                'date' => 'Last Updated',
                'description' => 'Description & Meta tag',
        ),
        'no_data'       => 'No data Blogs'

);
