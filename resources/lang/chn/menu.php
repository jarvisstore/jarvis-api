<?php

return array(

	'frontend' => array(

			'home' => '',
			'feature' => 'Feature',
            'portfolio' =>  'Portfolio',
            'blog'      =>  'Blogs',
			'pricing' => 'Pricing',
			'service' => 'Service',
			'template' => 'Template',
			'power_up' => 'Power Up',
			'tutorial' => 'Tutorials',
			'contact' => 'Contact',
			'partner' => 'Partner',
			'payment_confirmation' => 'Payment Confirmation',
			'login' => 'Login',

		),

	);
