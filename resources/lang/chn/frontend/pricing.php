<?php
return array(
	
	'seo' => array(
		'title'				=> 'Jarvis Store - Harga Paket Jarvis Store Mulai 39ribu',
		'description'		=> 'Berbagai pilihan paket jarvis store sesuai kebutuhan toko online anda.',
		'keywords'			=> 'harga pembuatan toko online, toko online murah, jasa pembuatan toko online, toko online instan, harga jarvis store, toko online instan'
	),
	'title'					=> 'PRICE',
	'title_description'		=> 'TRY FREE FOR LIMITED FEATURE. IF YOU LIKE US, SELECT THE FOLLOWING PACKAGE FOR ACCESS ALL FULL FEATURES',

	'price_title'			=> 'SUBSCRIBE FOR A YEAR AND GET FREE 2 MONTH',
		'price_title2'		=> 'TRY SUBSCRIBE A MONTH',
	'price_footer'			=> 'Need Specifications and Customization for your Online Store ? <a href="mailto:info@jarvis-store.com">Contact Us</a> for consultation.',
	'domain'				=> 'Free Domain',
	'7day'					=> 'First 7 day',
	'30day'					=> 'First 30 day',
	'1year'					=> 'Minimum 1 year subscribe',
	'package' => array(
		'ligth' => array(

			'title' 		=> 'MINI',
			'description' 	=> 'Best for mini online store with a litte amount of product',
			'price_monthly'	=> '<small>IDR</small> 39.000 <small>/Month</small>',
			'price_yearly'	=> '<small>IDR</small> 390.000 <small>/Year</small>',
			'total_product'	=> '50 Produk',
			'total_storage' => '150MB Storage',
			'reporting'		=> 'Full Report For Orders, Product and Payment',
			'analytic'		=> 'Statistic Report (Analytics)',
			'support'		=> 'Prioritas Support No. 3'

		),
		'starter' => array(

			'title' 		=> 'STARTER',
			'description' 	=> 'Best for small scale online store, with free template and power up.',
			'price_monthly'	=> '<small>IDR</small> 99Rb <small>/Month</small>',
			'price_yearly'	=> '<small>IDR</small> 990.000 <small>/Year</small>',
			'total_product'	=> '200 Product',
			'total_storage' => '500MB Storage',
			'reporting'		=> 'Full Report For Orders, Product and Payment',
			'analytic'		=> 'Statistic Report (Analytics)',
			'support'		=> 'Priority Support 3'

		),

		'premium' => array(

			'title' 		=> 'PREMIUM',
			'description' 	=> 'Best for medium scale online store',
			'price_monthly'	=> '<small>IDR</small> 199RB <small>/Month</small>',
			'price_yearly'  => '<small>IDR</small> 1.990.000 <small>/Year</small>',
			'total_product'	=> '1000 Product',
			'total_storage' => '1.5GB Storage',
			'reporting'		=> 'Full Report For Orders, Product and Payment',
			'analytic'		=> 'Statistic Report (Analytics)',
			'support'		=> 'Priority Support 2'

		),

		'ultimate' => array(

			'title' 		=> 'ULTIMATE',
			'description' 	=> 'Best for large online store and priority service, with costum template.',
			'price_monthly'	=> '<small>IDR</small> 499RB <small>/Month</small>',
			'price_yearly'  => '<small>IDR</small> 4.990.000 <small>/Year</small>',
			'total_product'	=> 'Unlimited Product',
			'total_storage' => '4GB Storage',
			'reporting'		=> 'Full Report For Orders, Product and Payment',
			'analytic'		=> 'Statistic Report (Analytics)',
			'support'		=> 'Priority Support 1'

		),
		'telkomsel' => array(

			'title' 		=> 'TELKOMSEL PACKAGE',
			'description' 	=> 'Jarvis Store package plan for Telkomsel costumers',
			'total_product'	=> '200 Product',
			'total_storage' => '500MB Storage',
			'internet' 		=> '2 Gb Internet Data *',
			'group_call' 	=> 'Free talk *',
			'sms' 			=> 'Free SMS *',
			'telkomsel_call'=> '100 minutes taking between Telkomsel numbers *',
			'reporting'		=> 'Complete report',
			'analytic'		=> 'Statistics report',
			'support'		=> 'Unlimited Support',
			'syarat'		=> '* service & policy base on pakage form Telkomsel'

		),
		'free' => array(

			'title' 		=> 'Try now for FREE',
			'description' 	=> "Let's start selling in your online store",
			'price_monthly'	=> '<small>IDR</small> 0 <small>/Month</small>',
			'total_product'	=> '10 Product',
			'total_storage' => '35MB Storage',
			'support'		=> 'Limited Support'

		),
		'enterprise' => array(

			'title' 		=> 'ENTERPRISE',
			'description' 	=> 'Big scale online shop with full customization without limits',
			'price_monthly'	=> '-- CALL --',

		),

	),
	'package_btn'			=> 'Orders Now',
	'package_btn_toko'		=> 'Create Shop',
	'faq'					=> 'Frequently asked questions of JARVIS-STORE.COM',
	'more_question'			=> 'More Question',
	'satisfaction' => array(
		'title'				=> 'SATISFATION GUARANTEED',
		'content'			=> 'YOUR SUCCESS IS OUR SUCCESS ALSO.'
	),
	'question_answer' => array(
		array(
			'title'			=> 'Cost installation',
			'content'		=> 'No.'
		),
		array(
			'title'			=> 'Cost for use Jarvis Store',
			'content'		=> 'Start from Free.'
		),
		array(
			'title'			=> 'How to create online store in Jarvis Store ?',
			'content'		=> 'Click button Crete Store in bottom and fill your Shop Name, Email Address, and Password then follow next step to manage your shop.'
		),
		array(
			'title'			=> 'Close your account and stop package subscriber',
			'content'		=> 'you can email us for close your account'
		),
		array(
			'title'			=> 'How to order package',
			'content'		=> 'you can choose for subscriber each Month or Year. If more than a Year just tell us and get a Special Price :)'
		),
		array(
			'title'			=> 'How to get Discount ?',
			'content'		=> 'Yes. We have event every month please check our blog, and also if you order for PREMIUM or ULTIMATE package for a year and more.'
		),
		array(
			'title'			=> 'How long my website online after fisnish order ?',
			'content'		=> 'After create order check our email to see the invoice and make a payment,  and Confirmation your payment in our website and your website will online after we receive the payment.'
		),
		array(
			'title'			=> 'How to get Domain ?',
			'content'		=> 'You can use your own domain if you have, or Order Domain in Jarvis Store.'
		),
		array(
			'title'			=> 'How to get my own email Domain ?',
			'content'		=> 'You can get user email of your domain if you order from us, contact us for user email you will used.'
		),
		array(
			'title'			=> 'How about Bandwidth?',
			'content'		=> 'Unlimited bandwidth for all package.'
		),
		array(
			'title'			=> 'Web hosting?',
			'content'		=> 'You do not have to thinking hosting for your website, we will handle it and you can focus on selling :)'
		),
		array(
			'title'			=> 'How to Upgrade Package',
			'content'		=> 'Any time and Any where just tell us and we will give you special price :)'
		),
		array(
			'title'			=> 'Have a Special Request ?',
			'content'		=> 'Just call us in line <a href="tel:+6282216655507">+6282216655507</a> or email <a href="mailto:info@jarvis-store.com">info@jarvis-store.com</a>, we will help you for your request to make a Online Shop.'
		),
	),

);