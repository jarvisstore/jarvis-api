<?php
return array(
	
	'title'						=> 'CREATE ONLINE STORE',
	'title_second'				=> 'SELLING ON ANY DEVICE, ANYTIME',
	'title_description'			=> 'FREE FOREVER',
	'register_btn'				=> 'Create store',
	'register_description'		=> 'LESS THAN 3 MINUTES TO SELL PRODUCT ONLINE!',
	'register_btn'				=> 'Create store',
	'register_term'				=> 'By create account, means that you agree with our <a href="http://jarvis-store.com/kebijakan">Service Policy</a>.',
	'video_btn'					=> 'View Introduction Video ',
	'testimonial'				=> 'What our client says about us',
	'products_title'			=> 'SOME POPULER PRODUCT',
	'cta_title'					=> 'READY TO START YOUR OWN ONLINE STORE?',
	'cta_description'			=> "Let's start now without any cost",
	'intro' => array(
		'main_title'			=> 'Everything you needed to make online store',
		'main_description'		=> 'Now you do not have to bother anymore to create an online store. With Jarvis Store, all requirements for your online store is available here. Because we always ? to hear what you needs.',
		'first_option'			=> 'Easy & Fast',
		'first_desctiption'		=> 'Less than 3 minutes, you can already have your own online store. No need to be a technical whiz or a programmer, all designed with clean and simple.',
		'second_option'			=> 'Payment for your store',
		'second_desctiption'	=> 'Bank Transfer, Credit Card, Paypal, Ipaymu and Dokuku. All we provide to facilitate the payment method. And you can focus more on selling your product.',
		'thrid_option'			=> 'Cool Design Template',
		'thrid_desctiption'		=> 'You can choose a template for the online store in your style. Want more personalized template? We can help make it happen.',
		'fourth_option'			=> 'FREE',
		'fourth_desctiption'	=> 'Free Forever! Yes, free template, free power up, and create your online store for free forever.',
	),
	'progress_setup'			=> 'Creating your store..',

);