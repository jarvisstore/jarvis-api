<?php
return array(
	
	'seo' => array(
		'title'			=> 'Jarvis Store - Beberapa Pengguna Jarvis Store',
		'description'	=> 'Sebagian toko online yang menggunakan jarvis store - kisah sukses para pengguna jarvis store.',
		'keywords'		=> 'Klien Jarvis Store, Jarvis Store Portfolio, contoh toko online, portfolio jarvis, toko online responsif, toko online jarvis store',
	),
	'title'				=> 'FEATURED PORTFOLIO',
	'title_description'	=> '',
	'cta_title'			=> 'ARE YOU READY<br> TO OPEN<br> ONLINE STORE?',
	'cta_btn'			=> 'Start Now',
	'portfolio_title'	=> 'OUR COSTUMERS:',
	'portfolio_footer'	=> '... and many more ...',

);