<?php
return array(

	'seo' => array(
		'title'			=> 'Jarvis Store - Berbagai pilihan template menarik yang gratis',
		'description'	=> 'Jarvis Store memiliki template responsif yang siap dipakai dan diubah sesuai keinginan.',
		'keywords'		=> 'jarvis store template, responsif template, responsif design, template toko online gratis, template toko online murah',
	),
	'title'				=> 'TEMPLATES OF JARVIS',
	'title_description'	=> '',
	'all_filter'		=> 'Show all',
	'general_filter'	=> 'General',
	'filter_8'			=> 'Other',
	'filter_1'			=> 'Fashion',
	'filter_2'			=> 'Handicraft',
	'filter_3'			=> 'Food',
	'filter_4'			=> 'Book',
	'filter_5'			=> 'Hobby',
	'filter_6'			=> 'Electronic',
	'filter_7'			=> 'Healty',
	'demo_btn'			=> 'Demo &rarr;',
	'info_btn'			=> 'More Information',
	'template_footer'	=> 'Attention! Only themes/designs from themeforest that are in this page, are allowed to be used in Jarvis Store.',

);