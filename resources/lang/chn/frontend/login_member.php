<?php
return array(

	'title'				=> 'LOGIN TO YOUR SHOP HERE!',
	'title_description'	=> 'Enter your E-mail Address and your Password!',
	'place_email'		=> 'Your Email',
	'place_password'	=> 'Your Password',
	'button_success'	=> 'Click here to continue to your shop.'

);