<?php
return array(
	
	'seo' => array(
		'title'			=> 'Jarvis Store - Power-up sebagai fitur tambahan untuk toko online anda',
		'description'	=> 'Jarvis Store memiliki template responsif yang siap dipakai dan diubah sesuai keinginan.',
		'keywords'		=> 'jarvis store template, responsif template, responsif design, template toko online gratis',
	),
	'title'				=> 'JARVIS POWER-UP',
	'title_description'	=> 'MORE POWE-UPS COMING SOON',
	'powerup_title'		=> 'Power-ups are additional plugins or features that can be embedded to your online shop. Various powerups, from free to payable, are here.',
	'powerup_footer'	=> '',

);