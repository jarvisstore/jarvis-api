<?php
return array(
	
	'title'					=> '博客数据',
	'list'					=> '博客列表',
	'category'				=> '分类博客',
	'link'					=> '加入链接',
	'breadcrumb' => array(
		'main'				=> '博客',
		'create'			=> '添加新文章',
		'edit'				=> '编辑博客',
		'category'			=> '编辑博客分类',
	),
	'form_input' => array(
		'first_title'		=> '博客数据',
		'first_description'	=> '标准博客数据',
		'second_title'		=> '博客分类数据',
		'title'				=> '博客标题',
		'content'			=> '填写博客',
		'category'			=> '类别',
		'category_name'		=> '类别名称',
		'add_category'		=> '添加类别',
		'edit_category'		=> '编辑分类',
		'tags'				=> 'Tags <span class="muted"> 用逗号分隔 (,)</span>',
	),
	'no_data'				=> 'No data Blogs'

);
