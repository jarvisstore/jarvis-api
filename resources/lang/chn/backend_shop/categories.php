<?php
return array(
	
	'title'					=> '分类数据',
	'list'					=> '类别列表',
	'breadcrumb' => array(
		'main'				=> '产品分类',
		'create'			=> '添加新类别',
		'edit'				=> '编辑分类',
	),
	'form_input' => array(
		'first_title'		=> '数据分类',
		'first_description'	=> '标准分类数据',
		'name'				=> '类别名称',
		'parent'			=> '选择母分类',
		'main_parent'		=> '作为主要分类',
		'description'		=> '描述和Meta标签（SEO）',
		'url'				=> '网址与手柄？',
	),
	'help' => array(
		'step1'				=> '产品类别的顺序，单击并拖拽来调整类别的顺序',
		'step2'				=> '点击更新选定的类别',
		'step3'				=> '点击删除选定的类别',
		'step4'				=> '点击添加一个新的类别',
		'step5'				=> '点击重新排列类别的顺序',
	),
	'no_data'				=> '没有分类数据'

);
