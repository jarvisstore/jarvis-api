<?php
return array(
	
	'header' => array(
		'tour'					=> 'Tour',
		'help'					=> 'Help',
		'system_overview'		=> 'System Overview',
		'disk_usage'			=> 'Disk Usage',
		'product_quota'			=> 'Product Quota',
		'goto_shop'				=> 'View Online Shop',
		'profile'				=> 'Profile',
		'account_billing'		=> 'Account & Billing',
		'logout'				=> 'Logout'
	),
	'helper' => array(
		'title'					=> 'Need help ?',
		'tutorial'				=> 'Tutorial',
		'video'					=> 'Video',
		'report_email'			=> 'Send to us',
		'report_deskription' 	=> 'Report for bug, errors, etc',
		'live_chat'				=> 'Chat with us',
		'cancel'				=> 'Cancel'
	),
	'menu' => array(
		'dashboard' 			=> 'Dashboard',
		'order'					=> 'Orders',
		'costumer'				=> 'Costumers',
		'catalog'				=> 'Catalog',
		'category'				=> 'Category',
		'product'				=> 'Product',
		'collection'			=> 'Collection',
		'discount'				=> 'Discount',
		'expedition'			=> 'Shipping',
		'blog'					=> 'Blogs',
		'pages'					=> 'Pages',
		'link'					=> 'Links',
		'testimonial'			=> 'Testimonials',
		'report'				=> 'Report',
		'statistic'				=> 'Statistics',
		'power_up'				=> 'Power ups',
		'themes'				=> 'Themes',
		'payment'				=> 'Payments',
		'setting'				=> 'Setting'
	),
	'message' => array(
		'free_pack'				=> 'Your Store Account using Free Pack',
		'info_pack'				=> 'For Information Jarvis Store Pack, Please check here',
		'upgrade_pack'			=> 'Upgrade your pack ? Click here',
		'trial_pack'			=> 'You use Trial 30 days, from Jarvis Store.',
		'notification'			=> '',
		'verify1'				=> '<h4>Notification!</h4>Hello, your email account is unverified, please check your email',
		'verify2'				=> ' in your inbox/spam and click url in email to verify your account.',
		'resend'				=> 'Resend verification email!'
	),
	'button' => array(
		'create'				=> 'Create',
		'delete'				=> 'Delete',
		'save'					=> 'Save',
		'export'				=> 'Export',
		'import'				=> 'Import',
		'reset'					=> 'Reset',
		'creating'				=> 'Add',
		'cancel'				=> 'Cancel',
		'download'				=> 'Download',
		'send'					=> 'Send'
	),

);