<?php
return array(
	
	'title'				=> '发货',
	'title_1'			=> '发货设置',
	'sub_title_1'		=> '发货系统',
	'opsi_1'			=> '使用发货',
	'opsi_2'			=> '免运费',
	'opsi_3'			=> '人工发运',
	'sub_title_2'		=> '人工发运说明',
	'sub_title_3'		=> '选择快递',
	'sub_title_4'		=> '发运地城市',
	'opsi_4'			=> '仅使用惯用发货方式',
	'opsi_5'			=> '要显示下级区域选项，请在设置菜单更新区域地址',
	'btn_save_expedisi'	=> '保存设置',
	'title_2'			=> '自创发货方式',
	'add_expedisi'		=> '添加惯用发货方式',
	'country'			=> '选择国家',
	'title_tbl'			=> '惯用发货方式',
	'exspedisi_name'	=> '命名发货方式',
	'create_new'		=> '新建',
	'pack_name'			=> '包名称',
	'edit_price'		=> '编辑价格',
	'breadcrumb' => array(
		'main'			=> '发货',
		'create'		=> '添加新的发货',
		'edit'			=> '编辑惯用发货',
	),
	'form_input' => array(
		'first_title'	=> '惯用发货数据',
		'second_title'	=> '发货数据',
		'thrid_title'	=> '设置价格',
		'to_city'		=> '目的地城市',
		'price_kg'		=> '价格每公斤',
		'add_price'		=> '添加价格',
	),
	'no_data_costum'	=> '没有惯用发货数据',
	'no_data_price'		=> '没有发货价格数据'

);