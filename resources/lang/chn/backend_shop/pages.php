<?php
return array(
	
	'title'					=> '页数据',
	'list'					=> '页列表',
	'link'					=> '加入链接',
	'breadcrumb' => array(
		'main'				=> '页',
		'create'			=> '添加新页面',
		'edit'				=> '编辑页面',
	),
	'form_input' => array(
		'first_title'		=> '页数据',
		'first_description'	=> '标准页数据',
		'title'				=> '标题',
		'content'			=> '内容',
		'date'				=> '最后更新',
		'description'		=> '描述和Meta标签',
	),
	'no_data'				=> '没有页数据'

);