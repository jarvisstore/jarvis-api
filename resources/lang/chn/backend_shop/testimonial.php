<?php
return array(
	
	'title'					=> 'Data Testimonials',
	'list'					=> 'List of Testimonials',
	'breadcrumb' => array(
		'main'				=> 'Testimonials',
		'create'			=> 'Add a new Testimonials',
		'edit'				=> 'Edit Testimonials',
	),
	'form_input'  => array(
		'first_title'		=> 'Data Testimonials',
		'first_description' => 'Basic data of Testimonials',
		'name'				=> 'Costumers Name',
		'testimonial'		=> 'Testimonials',
		'display'			=> 'Display',
	),
	'no_data'				=> 'No data Testimonials'
);