<?php
return array(
	
	'title'						=> '产品数据',
	'list'						=> '产品列表',
	'add_product'				=> '添加产品',
	'copy_product'				=> '复制产品',
	'see_product'				=> '查看产品',
	'eport_data'				=> '导出产品数据',
	'import_data'				=> '导入产品数据（ CSV）',
	'input_csv'					=> '上传产品csv文件',
	'update_same_data'			=> '更新具有相同的名称的现有产品',
	'sample_csv'				=> '下载csv文件模板',
	'breadcrumb' => array(
		'main'					=> '产品',
		'create'				=> '增加新产品',
		'edit'					=> '编辑产品',
		'copy'					=> '复制产品',
	),
	'form_input' => array(
		'first_title'			=> '产品详细信息',
		'first_description'		=> '产品数据，包括名称，生产厂家等。',
		'name'					=> '产品名称',
		'description'			=> '描述',
		'category'				=> '类别',
		'brand'					=> '供应商',
		'edit_category'			=> '变更类别',
		'select_category'		=> '选择类别',
		'new_category'			=> 'New Category',
		'second_title'			=> '库存和变型',
		'second_description'	=> '产品库存及变型',
		'price'					=> '售价',
		'before_price'			=> '全价（折扣前正常价格）',
		'mitra_price'			=> '经销商价格',
		'mitra_price_alert'		=> '如果没有设置经销商价格，那么经销商价格为销售价格',
		'des_mitra_price'		=> '该价格将在合作商订单中使用',
		'cek_mitra_price'		=> '经销商使用该价格？',
		'stock'					=> '库存（ SKU）',
		'gram'					=> '重量（克）',
		'barcode'				=> '条形码',
		'optional'				=> '有选择和变型？ 最大3个选项',
		'option_name'			=> '名称选项',
		'option_value'			=> '价值与库存',
		'select'				=> '选择选项...',
		'color'					=> '颜色',
		'size'					=> '大小',
		'material'				=> '材料',
		'new_opsi'				=> '创建一个新的选择',
		'add_opsi'				=> '添加选项',
		'variant'				=> '变型/选项',
		'add_variant'			=> '添加变型',
		'third_title'			=> '产品图片',
		'third_description'		=> '点击上传本产品的图片，或在这里放入图像',
		'upload'				=> '放入/点击上传',
		'main_img'				=> '主图片',
		'opt_img_1' 			=> '可选图片1',
		'opt_img_2' 			=> '可选图片2',
		'opt_img_3' 			=> '可选图片3',
		'delete'				=> '删除',
		'fourth_title'			=> '标签与收藏',
		'fourth_description'	=> '用于为产品属性分类的标签，例如颜色，尺寸，材质等',
		'tags'					=> '用<span class ="mute" >做标记，用逗号分隔',
		'collection'			=> '收藏',
		'fiveth_title'			=> 'SEO',
		'fiveth_description'	=> '使用搜索引擎优化 (google, yahoo, bing, etc)',
		'descrip_tag'			=> '描述元标记',
		'keyword_tag'			=> '关键字元标记',
		'keyword'				=> '关键词 SEO',
		'url'					=> '网址：手柄',
		'sixth_title'			=> '可见性（显示？ ）',
		'sixth_description'		=> '这款产品的现状',
		'home_page'				=> '首页产品，本产品将出现在首页（如果主题是支持）',
		'show'					=> '首页本产品将出现在特殊的主页（如果主题是支持）',
		'not_show'				=> '不显示，此产品将不会出现在所有网页',
		'only_one'				=> '你只能选择一个',
		'hot'					=> '最热门的产品，如果该产品是最畅销请点击',
		'new'					=> '新产品，如果该产品是新的请点击',
	),
	'help' => array(
		'step1'					=> '这是列出你店铺的产品表',
		'step2'					=> '查看产品和编辑产品',
		'step3'					=> '您可以通过复制现有产品来添加新的产品',
		'step4'					=> '点击添加一个新的类别',
		'step5'					=> '此按钮用来设置产品',
		'step6'					=> '点击这里选择所有产品',
		'step7'					=> '点击此处删除已选择的产品',
		'step8'					=> '点击此处进入一个新的产品',
		'step9'					=> '请点击此处筛选出通过配额限制的产品',
	),
	'mall' => array(
		'main'					=> '产品',
		'create'				=> '增加新产品',
		'edit'					=> '编辑产品',
	),
	'no_data'					=> '没有分类数据'

);