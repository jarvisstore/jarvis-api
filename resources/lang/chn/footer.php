<?php

return array(

	'frontend' => array(

			'helper' => array(
                    'title' => 'Helper',
					'feature' => 'JARVIS STORE FEATURE',
					'knowledgebase_1' => 'WHAT IS ONLINE STORE',
					'knowledgebase_2' => 'ONLINE TRANSACTION',
					'knowledgebase_3' => 'SECURITY',
					'knowledgebase_4' => 'ADVERTISE AND SEO',
					'knowledgebase_5' => 'WHAT IS RESPONSIVE WEB',
					'knowledgebase_6' => 'BUSINNES GOING ONLINE',
					'knowledgebase_7' => 'VIDEO ABOUT RESPONSIVE WEB',
					'knowledgebase_8' => '.id TLD Registration Requirement',
					'knowledgebase_9' => 'fast search engine indexing tips',
			),
			'jarvis_store' => array(

					'team' => 'About Us',
					'career' => 'Career',
					'press' => 'Press',
					'portfolio' => 'Portfolio',
					'term' => 'Service Policy',
					'sitemap' => 'Sitemap',
					'partner' => 'Partner Programs',
					'support' => 'Supports',
			),
			'demo' => array(

				'shop_demo' => 'Demo Store',
				'admin_demo' => 'Demo Administrator',

				),

			'newsletter' => array(
				'title' => 'EMAIL NEWSLETTER',
				'content'	=> 'Get special offer, information, tips and trick',
				'input' => 'Your Email..'
				),
			'payment' => 'Payment We Accept :',
		),


	);
