<?php
return array(

	'title' 		=> 'Payment Method',
	'details' 		=> "Select Payment will be used:",
	'transfer'		=> "Bank Transfer",
	'cc'			=> "Credit Card",
	'bank'			=> "Bank",
	'no_account'	=> "Bank Account",
	'bank_account'	=> "Bank Owner",
	'paypal_done'	=> "Please pay with your Paypal by online via Paypal payment gateway. This transaction will expire in 2x24 hours. ",
	'ipaymu_done'	=> "Please pay with your iPaymu by online via iPaymu Payment gataway. This transaction will expire in 2x24 hours. ",
	'doku_done'		=> '<p>DOKU MyShortCart is the easiest and fastest way to sell your services and products online. It offers a simpler way to sell directly to your audience via platforms like emails, Facebook, twitter, Instagram, blogs, or YouTube, by simply clicking on a link that will lead to a secure payment page. (<a href="http://doku.com/my-short-cart" target="_blank">Doku MyShortCart</a>)</p><hr>Please chose payment which will you used :',
	'bitcoin_done'	=> "Please pay with your BitCoin by online. This transaction will expire in one hour (1 hour). ",
	'cod_done'		=> "<strong>Cash on Delivery (COD)</strong> payment for a good is made at the time of delivery. Payment can be made by cash.",
	'midtrans'		=> "By choosing midtrans, you can make payments to multiple payment channels provided such as:",
	'continue'		=> "Next",
	'back'			=> "Back",

);