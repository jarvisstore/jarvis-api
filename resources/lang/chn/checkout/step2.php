<?php
return array(

	'title' 			=> 'Costumer & Shipping',
	'details' 			=> "Shopping Details",
	'name'				=> "Name",
	'email'				=> "Email",
	'address'			=> "Address",
	'country'			=> "Country",
	'select_country'	=> "Select Negara",
	'state'				=> "Provinsi",
	'select_state'		=> "Select Province",
	'city'				=> "Kota",
	'select_city'		=> "Select City",
	'town'				=> "Subdistric",
	'select_town'		=> "Select Subdistrict",
	'poscode'			=> "Post Code",
	'phone'				=> "Phone Number",
	'messege'			=> "Notes",
	'recipient'			=> "Recipient",
	'same_recipient'	=> "Data Recipient is same with Buyer",
	'diff_recipient'	=> "Data Recipient is difference (Sent a Gift)",
	'name_recipient'	=> "Name",
	'address_recipient'	=> "Address",
	'phone_recipient'	=> "Phone",
	'back'				=> "Back",
	'continue'			=> "Next",

);