<?php
return array(

	'title' 		=> 'Finish - Order Informasi',
	'thanks' 		=> 'Thank you,',
	'buy_to' 		=> "for shopping at our store.",
	'total'			=> "Total Purchases",
	'id'			=> "ID ORDER",
	'notif'			=> "Your order data have been sucessfully send by email, which contains information on these order and the next step you should be done.",
	'payment_way'	=> "Payment Method",
	'bank_trans'	=> "For Bank Transfer, please transfer to one of following account",
	'confirm_info'	=> "After making the payment please confirm your payment here",
	'confirm_btn'	=> "Payment Confirmation",
	'paypal'		=> 'Please make payment with your Paypal account online via paypal payment gateway. This transaction applies if payment is made before (2x24 hours). Click "Pay with Paypal" button below to continuethe payment process.',
	'paypal_btn'	=> 'Pay with Paypal',
	'doku'			=> 'Please make payment with Doku MyShortCart by click "Pay with Doku MyShortCart" button below to continue the payment process. This transaction will canceled if not paid within 1x24 hours.',
	'doku_btn'		=> 'Pay with Doku MyShortCart',
	'ipaymu'		=> 'Please make payment with iPaymu by click "Pay with iPaymu" button below to continue the payment process.',
	'ipaymu_btn'	=> 'Pay with iPaymu',
	'bitcoin'		=> 'Please make payment with BitCoin, by click "Pay with Bitcoin" button below to continue the payment process. Your payment will expire in 12 hour.',
	'bitcoin_btn'	=> 'Pay with Bitcoin',
	'cod'			=> "Payment can be made by cash, certified check or money order, depending on what is stipulated in the shipping contract",

);