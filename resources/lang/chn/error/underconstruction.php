<?php
return array(

	'content' => array(
		'title'			=> 'Coming Soon',
		'description'	=> 'Our website is coming soon. In the mean time to connect with us with the information below .',
		'contact_us'	=> array(
			'title'		=> 'Contact us',
			'telp'		=> 'Phone',
		),
		'find_us'		=> "Find us",
		'address'		=> "Address"
	),
	'back'				=> 'Go Back'

);