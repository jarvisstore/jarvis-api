<?php

return array(

    'title' => 'Ganti Password',
    'title_description' 	=> 'Silahkan masukan alamat (URL) Website <br> kemudian Email Login kamu',
    'place_email' => 'Email kamu',
    'place_akun' => 'Website kamu, contoh: example.jstore.co',
    'button_success' => 'Klik disini untuk melanjutkan ke toko kamu.',

    'reset' => 'Reset Password',
    'reset_description' => 'Silakan ganti password kamu yang lama dengan yang baru',
    'new_password' => 'Password baru',
    'confirm_new_password' => 'Masukan ulang Password baru',
);
