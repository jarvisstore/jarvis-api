<?php

return array(
		'seo' => array(
			'title' => 'Jarvis Store - Buat toko online murah, harga mulai 39rb',
	        'description' => 'Jasa pembuatan toko online murah, pilih paket harga sesuai dengan kebutuhan Anda.',
	        'keywords' => 'buat toko online murah, jasa buat toko online murah, paket jarvis-store, harga jarvis-store, harga buat toko online,  mini, starter, premium, ultimate',
		),
		'title' => 'HARGA PAKET',
		'title_description' 	=> 'Upgrade Toko Onlinemu Kapanpun Anda Inginkan',
		'choose_plan' 	=> 'Pilih Paket Toko Online ',

		'price_title' => 'GRATIS 2 BULAN JIKA BERLANGGANAN 1 TAHUN',
        	'price_title2' => 'Pilih Paket Yang Sesuai Dengan Kebutuhan Usaha Anda',
		'price_footer' => 'Bingung dalam memilih paket sesuai kebutuhan Anda?<br> Butuh spesifikasi dan kustomisasi yang lebih besar?',
		'domain' => 'Mendapatkan domain gratis ',
		'7day' => '7 hari pertama',
		'30day' => '30 hari pertama',
		'1year' => 'min kontrak 1 th',
		'all_feature' => 'Lihat semua fitur',
		'free_template' => 'Unlimited install Tema Gratus',
		'package' => array(
			'priority' => 'Prioritas Support',
			'qty_product' => 'Jumlah Kapasitas Produk',
			'qty_storage' => 'Besar Kapasitas <br>Penyimpanan',
				'ligth' => array(

						'title' 		=> 'MINI',
						'description' 	=> 'Cocok untuk toko online yang tidak memiliki banyak produk',
						'price_monthly'	=> '<small>IDR</small> 39.000 <small>/Bulan</small>',
            			'price_yearly'	=> '<small>IDR</small> 390.000 <small>/Tahun</small>',
						'total_product'	=> '50 Produk',
						'total_storage' => '150MB Storage',
						'reporting'		=> 'Laporan Lengkap (Order,Produk,Pembayaran)',
						'analytic'		=> 'Laporan Statistik Lengkap (Analytics)',
						'support'		=> '<small>Prioritas No. 4</small>'

					),
				'starter' => array(

						'title' 		=> 'STARTER',
						'description' 	=> 'Yang baru memulai usaha Toko Online',
						'price_monthly'	=> '<small>IDR</small> 99.000 <small>/ Bulan</small>',
            			'price_yearly'	=> '<small>IDR</small> 990.000 <small>/ Tahun</small>',
						'total_product'	=> '200 Produk',
						'total_storage' => '500MB <br>Penyimpanan',
						'reporting'		=> 'Laporan Order Lengkap',
						'analytic'		=> 'Laporan Statistik Lengkap',
						'support'		=> '<small>Prioritas No. 3</small>',
						'admin'			=> '1 Tambahan Admin'

					),

				'premium' => array(

						'title' 		=> 'PREMIUM',
						'description' 	=> 'Toko Online dengan tampilan keren',
						'price_monthly'	=> '<small>IDR</small> 199.000 <small>/ Bulan</small>',
            			'price_yearly'  => '<small>IDR</small> 1.990.000 <small>/ Tahun</small>',
						'total_product'	=> '1000 Produk',
						'total_storage' => '1.5GB <br>Penyimpanan',
						'reporting'		=> 'Laporan Lengkap (Order,Produk,Pembayaran)',
						'analytic'		=> 'Laporan Statistik Lengkap (Analytics)',
						'support'		=> '<small>Prioritas No. 2</small>',
						'template' 		=> 'Gratis 1 Tema Premium',
						'admin'			=> '9 Tambahan Admin'

					),

				'ultimate' => array(

						'title' 		=> 'ULTIMATE',
						'description' 	=> 'Layanan Prioritas dan Kostumisasi',
            			'price_monthly'	=> '<small>IDR</small> 499.000 <small>/ Bulan</small>',
            			'price_yearly'  => '<small>IDR</small> 4.990.000 <small>/ Tahun</small>',
						'total_product'	=> 'Unlimited Produk',
						'total_storage' => '5GB <br>Penyimpanan',
						'reporting'		=> 'Laporan Lengkap (Order,Produk,Pembayaran)',
						'analytic'		=> 'Laporan Statistik Lengkap (Analytics)',
						'support'		=> '<small>Prioritas No. 1</small>',
						'template' 		=> 'Gratis 1 Tema Premium',
						'admin'			=> 'Unlimited Admin'


					),

				'telkomsel' => array(

						'title' 		=> 'PAKET TELKOMSEL',
						'description' 	=> 'Paket layanan untuk pelanggan Telkomsel',
						'total_product'	=> '200 Produk',
						'total_storage' => '500MB',
						'internet' 		=> '2 Gb Data Internet *',
						'group_call' 	=> 'Gratis bicara & SMS ke grup *',
						'telkomsel_call'=> 'Bonus bicara sesama Telkomsel*',
						'reporting'		=> 'Laporan Lengkap (Order,Produk,Pembayaran)',
						'analytic'		=> 'Laporan Statistik Lengkap (Analytics)',
						'support'		=> 'Unlimited Support',
						'syarat'		=> '* syarat &amp; ketentuan berlaku sesuai paket dari Telkomsel'

					),

				'free' => array(

						'title' 		=> 'FREE',
						'description' 	=> 'Buat toko online sendiri dan mulai berjualan online dengan <strong>Paket Free</strong> dari <strong>Jarvis Store</strong>',
						'tag_line' 	=> 'Belajar Toko Online sendiri',
						'price_monthly'	=> '<small>IDR</small> 0 <small>/ Bulan</small>',
						'price_yearly'	=> '<small>IDR</small> 0 <small>/ Tahun</small>',
						'total_product'	=> '10 Produk',
						'total_storage' => '35MB',
						'support'		=> '<small>Limited Support</small>'

					),

				'enterprise' => array(

						'title' 		=> 'ENTERPRISE',
						'description' 	=> 'Toko online besar dengan kustomisasi fitur dan desain tanpa batas',
						'price_monthly'			=> '-- CALL --',
					),

			),
		'compare' => 'Perbandingan Paket',
		'bandwidth' => 'Unlimited Bandwidth',
		'discount' => 'Fitur Kupon dan Diskon Toko',
		'html' => 'Fitur HTML/CSS Editor',
		'support' => 'Bantuan HTML/CSS',
		'premium_theme' => 'Mendapat 1 Premium Template',
		'free_logo' => 'Gratis Desain Logo Usaha',
		'costum_theme' => 'Costum Website Design',
		'package_btn' => 'Pesan Sekarang',
		'package_btn_register' => 'Daftar Sekarang',
		'buy_btn' => 'Beli',
		'change_btn' => 'Berlanganan',
		'continue_btn' => 'Lanjut',
		'package_btn_toko' => 'Buat Toko',
		'faq' => 'Pertanyaan tentang Jarvis Store',
		'more_question' => 'Cari Solusi lainnya',

		'satisfaction' => array(

				'title'	=> 'SATISFATION GUARANTEED',
				'content' 	=> 'Kesuksesan toko kamu adalah kesuksesan kami juga, untuk itu kami menjamin kepuasan pengguna JARVIS STORE.'
			),

		'question_answer' => array(

				'faq_1' => array(
					'title' => 'Apakah ada biaya instalasinya?',
					'content' => 'Tidak. Anda tidak dikenakan biaya instalasi pada semua paket.'
 					),

				'faq_2' => array(
					'title' => 'Apakah saya harus membayar untuk mencoba Jarvis Store ?',
					'content' => 'Tidak. Jarvis Store memiliki paket gratis saat pendaftaran pertama kali :) .'
 					),

				'faq_3' => array(
					'title' => 'Jika sudah order berapa lama toko online saya selesai dibuat ?',
					'content' => 'Silahkan melakukan pembayaran dan <a target="_blank" href="//jarvis-store.com/konfirmasi-pembayaran">konfirmasi pembayaran</a>, selanjutnya kami akan segera memproses order Anda, untuk domain biasanya 1x24 jam akan aktif.'
 					),

				'faq_4' => array(
					'title' => 'Apakah Jarvis Store memberikan diskon ?',
					'content' => 'Ya. kami memberikan potongan pada <a target="_blank" href="//jarvis-store.com/artikel/info/8/Event/">event</a> tertentu dan apabila Anda mendaftar untuk paket 2 atau 3 tahunan dengan pembayaran diawal.'
 					),

				'faq_5' => array(
					'title' => 'Bisakah saya menggunakan domain saya sendiri ?',
					'content' => 'Ya. Anda bisa menggunakan domain Anda sendiri atau domain yang sudah Anda miliki.'
 					),

				'faq_6' => array(
					'title' => 'Bagaimana cara order paket di Jarvis Store ?',
					'content' => 'Silahkan pilih paket yang ada di atas, dengan klik tombol Pesan Sekarang. Lebih lengkap silahkan bisa di baca <a target="_blank" href="//jarvis-store.com/artikel/prosedure-pemesanan-paket-di-jarvis-store">disini.'
 					),

				'faq_7' => array(
					'title' => 'Apa perbedaan Paket yang ada di Jarvis Store ?',
					'content' => 'Secara umum perbedaannya ada di kapasitas produk, penympanan file dan juga dari segi fitur dan prioritas support.'
 					),

				'faq_8' => array(
					'title' => 'Apakah saya bisa merubah paket saya nantinya ?',
					'content' => 'Ya. Anda bisa mengubah paket anda kapanpun anda mau.'
 					),

				array(
					'title' => 'Berapa biaya dan besar bandwidth Jarvis Store?',
					'content' => 'Tidak Ada. Semua paket Jarvis Store unlimited bandwidth.'
 					),

				array(
					'title' => 'Apakah saya membutuhkan web hosting?',
					'content' => 'Tidak. Semua paket Jarvis Store menyediakan hosting yang aman dan cepat. Kami menggunakan server terbaik untuk menjamin situs Anda diakses dengan cepat.'
 					),

				array(
					'title' => 'Apakah saya bisa merubah paket saya nantinya?',
					'content' => 'Ya. Anda bisa mengupgrade paket Anda kapanpun.'
 					),

				array(
					'title' => 'Bagaimana jika saya mempunyai permintaan khusus ?',
					'content' => 'Anda bisa menghubungi kami di <a href="tel:0222015090">(022) 201 5090 </a> atau di <a href="mailto:info@jarvis-store.com">info@jarvis-store.com</a>, kami akan membantu Anda atau membuatkan paket khusus yang sesuai dengan request Anda.'
 					),

				array(
					'title' => 'Bagaimana saya membuat email domainnya?',
					'content' => 'Silahkan anda bisa menambahkan note pada saat memesan domain atau bisa langsung request ke email kita di info@jarvis-store.com, dan infokan akun email domain yang di inginkan'
 					),

				

			),


	);
