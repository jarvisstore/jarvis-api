<?php
return array(
    'title' => 'Upgrade Paket Toko Anda',
    'description' => 'Silakan masukkan akun jarvis kamu, email akun dan pilih paket yang ingin di pakai.',
    'thanks' => 'Terima Kasih :)',
	'heading_thanks' => 'Toko online dan akun Anda sudah ditutup',
	'heading_close' => 'Terima Kasih sudah menggunakan Jarvis Store. Untuk kritik dan saran silahkan kirim ke tim Jarvis Store melalui email ke <a href="mailto:info@jarvis-store.com">info@jarvis-store.com</a>.',
);
