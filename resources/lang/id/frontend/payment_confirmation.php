<?php

return array(

    'title' => 'KONFIRMASI PEMBAYARAN',
    'title_description' 	=> '',
    'domain_name' => 'Nama Domain',
    'email' =>  'Email',
    'payment_method' => 'Metode Pembayaran',
    'destination_account' => 'Rekening Tujuan',
    'total_payment' => 'Jumlah Yang Dibayar',
    'payment_date' => 'Tanggal Dibayar (tanggal/bulan/tahun)',
    'account_owner' => 'Nama Pemegang Rekening',
    'account_no'    => 'No Rekening',
    'konfirmsi' => 'Kirim konfirmasi Anda',
    'link_admin_confirm' => 'Konfirmasi Pembayaran melalui Admin Toko',
    'or' => '-- atau --',
);
