<?php
return array(
	'seo' => array(
		'title' => 'Jarvis Store : Jasa Toko Online Instan dengan Fitur Lengkap',
        'description' => 'Dengan fitur utama menggunakan template responsive, ongkir otomatis, SEO, member, reseller, diskon kode dan kupon, laporan produk, laporan pelanggan, laporan transaksi, inport ekspor data, statistoc web',
        'keywords' => 'jasa toko online murah, jasa toko online instan, fitur responsive, SEO, fitur toko online, fitur toko online gratis, fitur jarvis store, toko online terintegrasi jne, toko online responsif, toko online responsive, laporan toko online, pembayaran toko online, testimonial toko online, statistik toko online, statistik web, email marketing,  toko online blog, notifikasi toko online, '
	),
	'title' 				=> 'FITUR-FITUR JARVIS STORE',
	'title_description' 	=> 'Membuat Toko Online Tidak Pernah semudah dan secepat ini',
	'cta_title'         => 'SUDAH SIAP BUKA TOKO ONLINE Anda?',
    'cta_description'       => 'Coba sekarang tanpa biaya apapun',
    'other_features' 		=> "Dan Beragam Fitur lainnya",
	'responsive_design' 	=> array(

		'title' 		=> 'Responsive Design',

		'feature_1' 	=> array(
				'title' 	=> 'Toko online dalam genggaman',
				'content' 	=> 'Semua template design toko online yang kami sediakan adalah template responsive, jadi tidak perlu khawatir pelanggan toko dapat mengakses dan berbelanja online di toko Anda dimanapun, baik dengan menggunakan PC, tablet maupun smartphone.'
				),

		'feature_2' 	=> array(
					'title' 	=> ' KONTROL DIMANAPUN',
					'content' 	=> 'Selain toko online Anda, bagian admin dari Jarvis Store juga menggunakan responsive template yang memungkinkan Anda untuk mengatur, menginput barang dimana saja langsung menggunakan perangkat Anda baik laptop, tablet atau smartphone.'
				),
		
		'feature_3' 	=> array(
				'title' 	=> 'DI SEMUA PERANGKAT',
				'content' 	=> 'Semakin banyaknya orang berbelanja online dengan tablet dan smartphone, maka penting untuk toko online yang memiliki tampilan yang baik di tablet dan smartphone, yang memberikan pengalaman berbelanja online menyenangkan dan mudah.'
				),

		'feature_4' 	=> array(
				'title' 	=> ' WEB DESIGN SPECIALIST',
				'content' 	=> 'Menginginkan web design yang dikustomisasi sesuai keinginan Anda? kami menyediakan layanan web design, untuk info silakan email ke <a href="mailto:info@jarvis-store.com">info@jarvis-store.com</a>.'
				),
	),

	'power_up'				=> array(

		'title'		=> 'Power-Up Sistem',

		'feature_1' => array(
			'title' => 'Power Up',
			'content' => 'Sistem Power-Up memungkinkan Anda menambah berbagai macam fitur baru ke website Anda dengan mudah. Power-Up yang bisa Anda tambahkan, seperti fitur marketing, fitur SEO, membuat sistem diskon, dan masih banyak lagi.'
			),

		'feature_2' => array(
			'title' 	=> ' Menulis Artikel / Halaman',
			'content'	=> 'Terdapat sistem blog yang memungkinkan Anda membuat blog langsung di toko online Anda. Artikel dibutuhkan oleh pelanggan untuk menambah wawasan tentang produk Anda dan berguna sebagai sistem marketing di internet.'
			),

		'feature_3' => array(

					'title'	=> ' Share Social Media',
					'content'	=> 'Semua website Jarvis Store sudah terintegrasi dengan social media yag memungkinkan pelanggan kamu untuk menyebarkan atau merekomendasikan produk kamu di sosial media.'

				),

	),

	'marketing_seo'			=> array(

			'title'		=> 'Marketing & SEO',

			'feature_1' => array(

				'title' => 'SEO',

				'content' => 'Pelanggan Anda harus bisa menemukan toko online Anda melalui search engine seperti google atau bing. Jarvis Store mendukung optimasi SEO yang integrasi SEO pada cms kami seperti meta tag dan URL yang SEO friendly.'
				),

			'feature_2'	=> array(

				'title' => 'Diskon dan Kupon',
				'content' => 'Semua paket Jarvis Store sudah terintegrasi dengan sistem kupon untuk memberikan diskon kepada pelanggan yang akan membantu untuk mempromosikan produk Anda.'
				),

			'feature_3'	=> array(

				'title' => 'SOCIAL MEDIA',
				'content' => 'Semua website Jarvis Store sudah terintegrasi dengan social media yang memungkinkan pelanggan Anda untuk menyebarkan atau merekomendasikan produk Anda di media social.'
				),

			'feature_4'	=> array(

				'title' => ' EMAIL MARKETING',
				'content' => 'Jarvis Store terintegrasi dengan layanan email marketing Mailchimp yang memungkinkan Anda membuat email marketing yang indah dan efisien.'
				),

			'feature_5'	=> array(

				'title' => ' FULL STATISTIK',
				'content' => 'Jarvis Store memiliki fitur analytics yang lengkap untuk menganalisa progres penjualan dan pengunjung website. Jadi Anda bisa menentukan pilihan strategi yang tepat untuk meningkatkan penjualan Anda kedepan.'
				),

		),
	
	'management_member'		=> array(

			'title'		=> 'Manajemen Pelanggan',

			'feature_1'		=> array(

				'title' => ' MANAJEMEN PELANGGAN ',
				'content' => 'Jarvis Store memiliki sistem manajemen pelanggan yang lengkap yang memungkin kan Anda untuk mengelompokkan pelanggan dengan mudah, menentukan tipe pelanggan dan melihat profil pelanggan juga membuat invoice.'

				),

			'feature_2'		=> array(

				'title' => ' Sistem Notifikasi',
				'content' => 'Sistem notifikasi yang memunginkan Anda selalu terupdate dengan semua order yang masuk. Sistem notifikasi melalui admin, email, dan android apps yang mempermudah Anda berhubungan dengan pelanggan.'

				),

			'feature_3'		=> array(

				'title' => ' Member dan Reseller',
				'content' => 'Jarvis Store memiliki sistem dua tipe pelanggan, yaitu member biasa dan mitra/reseller dimana hal ini akan berguna jika Anda memiliki reseller yang harga jualnya berbeda dengan pelanggan biasa.'

				),

			'feature_4'		=> array(

				'title' => ' Testimonial',
				'content' => 'Testimonial adalah cerminan kepuasan pelanggan. Memudahkan bagi Anda yang ingin menampilkan testimonial juga jika pelanggan ingin memberikan testimonial di website Anda.'

				),

			'feature_5'		=> array(

				'title' => ' Group Pelanggan',
				'content' => 'Dengan Jarvis Store Anda bisa mengelompokkan pelanggan dengan mudah, bisa mengelompokkan berdasarkan lokasi, transaksi, dan lain-lain.'

				),
		),

	'shopping_cart'			=> array(

			'title'		=> 'Keranjang Belanja',

			'feature_1' => array(

					'title' => ' KEMUDAHAN BERBELANJA',
					'content' => 'Dari pilihan penghitungan pengiriman barang yang dapat dari JNE, Tiki, POS Indonesia, dan kostum ekspedisi, beragam pembayaran gateway, membuat proses berbelanja di toko Anda begitu mudah dan aman dengan Jarvis Store.'
				),
			'feature_2' => array(

					'title' => ' EKSPEDISI PENGIRIMAN',
					'content' => 'Selain menyediakan tarif ekspedisi dari JNE, Tiki, POS Indonesia dan lain-lain .Anda juga bisa menambahkan kostum ekspedisi yang Anda gunakan untuk mengirimkan produk Anda ke pelanggan.'
				),
			'feature_3' => array(

					'title' => ' BERAGAM Jenis PEMBAYARAN',
					'content' => 'Jarvis Store website dapat menerima pembayaran transaksi dengan pembayaran melalui transfer bank, paypal, ipaymu, dan doku myshortcart.'
				),
			'feature_4' => array(

					'title' => ' Sertifikat SSL',
					'content' => 'JIka Anda membutuhkan sertifikat SSL, Jarvis Store menyediakan SSL 128bit untuk menjamin informasi Anda terenkripsi. SSL ini sama dengan yang dipakai oleh bank-bank besar di dunia.'
				),
		),

	'unlimited_hosting'		=> array(

			'title' 	=> 'Unlimited Hosting',

			'feature_1' => array(

					'title'	=> 'Domain Sendiri',
					'content'	=> 'Anda dapat meregister domain untuk website Anda juga bisa memanfaatkan layanan register domain dari Jarvis Store, menggunakan domain yang sudah Anda miliki, atau tetap memakai domain gratis Jarvis Store.'

				),

			'feature_2' => array(

					'title'	=> ' Unlimited Bandwidth',
					'content'	=> 'Unlimited bandwidth untuk data transfer yang tidak terbatas. Kami bekerjasama dengan penyedia layanan server yang memiliki reputasi uptime terbaik untuk menjamin website Anda selalu online 99.95% uptime.'

				),

			'feature_3' => array(

					'title'	=> ' MUDAH DAN GRATIS',
					'content'	=> 'Buat toko online mudah dan gratis di Jarvis Store, jadi tidak perlu repot mengatur banyak hal dan bisa fokus dengan produk dan pelanggan Anda.'

				),

			

		),

	'support'				=> array(

			'title' => '24/7 Support',

			'feature_1'	=> array(

					'title' => ' FOKUS PELANGGAN',
					'content' 	=> 'Kami selalu mendengarkan pelanggan dan menyampaikannya ke tim kami. Kami berusaha memberikan pelayanan dan support terbaik, untuk mendapatkan pelayanan yang terbaik.'
				),

			'feature_2'	=> array(

					'title' => ' KNOWLEDGE BASE',
					'content' 	=> '<a href="/knowledgebase" target="_blank">Knowledge Base</a> dan <a href="/artikel" target="_blank">Blog</a> Jarvis Store berisi berbagai informasi yang ingin Anda ketahui seputar sistem, e-commerce dan bisnis internet.'
				),

			'feature_3'	=> array(

					'title' => ' Tutorial',
					'content' 	=> 'Tidak perlu bingung menggunakan sistem Jarvis Store, kami juga menyediakan <a href="http://jarvisstore.freshdesk.com/support/home" target="_blank">tutorial</a> step-by-step yang siap memandu Anda serta banyak channer bantuan yang bisa membantu dari live chat, email, sosial media, dan video turorial.'
				),

			'feature_4'	=> array(

					'title' => ' KOMUNITAS',
					'content' 	=> 'Kami juga menyediakan forum dimana Anda bisa berdiskusi dan saling bertukar informasi dengan pengguna lainnya.'
				),
			'feature_5'	=> array(

					'title' => ' CHANNEL BANTUAN',
					'content' 	=> 'Beragam channel bantuan dari online chat, applikasi chat, dan lainnya agar Anda bisa dengan mudah berkonsultasi dan memberikan informasi dengan tim Jarvis Store.'
				),
		),

	'reporting'				=> array(

			'title' => 'Reporting & Invoice',

			'feature_1' 	=> array(

					'title' => ' Laporan Lengkap dan Statistik',
					'content' => 'Anda bisa dengan bebas melihat Laporan statistic web, laporan pelanggan, produk, transaksi, koleksi dan lain-lain. Anda juga dapat membuat laporan transaksi berdasarkan waktu yang Anda inginkan.'

				),

			'feature_2' 	=> array(

					'title' => ' Menyimpanan Data',
					'content' => 'Permudah pengisian dan backup data Anda dengan sistem impor dan ekspor data dengan menggunakan data csv.'

				),

			'feature_3' 	=> array(

					'title' => ' LAPORAN SEMUA HAL',
					'content' => 'Laporan pelanggan, produk, transaksi, koleksi dan lain-lain. Semua bisa dibuat dan dibackup hanya dengan beberapa klik.'

				),

			'feature_4' 	=> array(

					'title' => ' LAPORAN STATISTIC WEB',
					'content' => 'Laporan statistic web berupa infografis tentang detail keseluruhan website Anda.'

				),
		),
 
);
