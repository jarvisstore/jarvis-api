<?php
return array(
	
	'title' 			=> 'Login Administrator Toko',
	'input_email' 		=> 'Email Anda',
	'input_password' 	=> 'Password Anda',
	'forget' 			=> 'Lupa',
	'forget_password' 	=> 'Lupa Password',
	'forget_button' 		=> 'Klik disini untuk mengganti password',
	'reset_button' 		=> 'Kirim intruksi pergantian ke email',
	'remember' 			=> 'Ingat email dan password',
	'login_button' 		=> 'Masuk',
	'forget_email' 	=> 'Lupa Email',
	'get_email' 	=> 'Kirim Nama Toko Anda ke info@jarvis-store.com',

);
