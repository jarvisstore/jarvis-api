<?php

return array(

	'frontend' => array(

			'helper' => array(
                    'title' => 'ผู้ช่วย',
					'feature' => 'คุณสมบัติ',
					'knowledgebase_1' => 'อะไรคือร้านค้าออนไลน์',
					'knowledgebase_2' => 'การทำธุรกรรมออนไลน์',
					'knowledgebase_3' => 'การรักษาความปลอดภัย',
					'knowledgebase_4' => 'การโฆษณาและเครื่องมือค้นหา',
					'knowledgebase_5' => 'คืออะไรเว็บตอบสนอง',
					'knowledgebase_6' => 'ธุรกิจไปออนไลน์',
					'knowledgebase_7' => 'วิดีโอเกี่ยวกับเว็บตอบสนอง',
					'knowledgebase_6' => 'ธุรกิจไปออนไลน์',
					'knowledgebase_6' => 'ธุรกิจไปออนไลน์',
				),
			'jarvis_store' => array(

					'team' => 'เกี่ยวกับเรา',
					'career' => 'อาชีพ',
					'press' => 'ข่าวประชาสัมพันธ์',
					'portfolio' => 'ผลงาน',
					'term' => 'นโยบายการให้บริการ',
					'sitemap' => 'แผนผังเว็บไซต์',
					'partner' => 'โปรแกรมพันธมิตร',
					'support' => 'สนับสนุน',

				),
			'demo' => array(

				'shop_demo' => 'สาธิตการจัดเก็บ',
				'admin_demo' => 'สาธิตผู้ดูแลระบบ',

				),

			'newsletter' => array(
				'title' => 'จดหมายข่าวทางอีเมล',
				'content'	=> 'รับข้อเสนอพิเศษข้อมูลเคล็ดลับและเคล็ดลับเกี่ยวกับร้านค้าออนไลน์และอีคอมเมิร์ซโดยตรงไปยังอีเมลของคุณ',
				'input' => 'อีเมล์ของคุณ'
				),
		),


	);
