<?php

return array(

		'title' => 'PRICE',
		'title_description' 	=> 'TRY FREE FOR LIMITED FEATURE. IF YOU LIKE US, SELECT THE FOLLOWING PACKAGE FOR ACCESS ALL FULL FEATURES',

		'price_title' => 'SUBSCRIBE FOR A YEAR AND GET FREE 2 MONTH',
        'price_title2' => 'TRY SUBSCRIBE A MONTH',
		'price_footer' => 'Need Specifications and Customization for your Online Store ? <a href="mailto:info@jarvis-store.com">Contact Us</a> for consultation.',

		'package' => array(

				'starter' => array(

						'title' 		=> 'STARTER',
						'description' 	=> 'Suitable for small scale online shop, with free template and power up.',
						'price_monthly'	=> '<small>IDR</small> 99Rb <small>/Month</small>',
                        'price_yearly'	=> '<small>IDR</small> 990.000 <small>/Year</small>',
						'total_product'	=> '200 Product',
						'total_storage' => '500MB Storage',
						'reporting'		=> 'Full Report For Orders, Product and Payment',
						'analytic'		=> 'Statistic Report (Analytics)',
						'support'		=> 'Priority Support 3'

					),

				'premium' => array(

						'title' 		=> 'PREMIUM',
						'description' 	=> 'Suitable for medium scale online shop, with premium template',
						'price_monthly'	=> '<small>IDR</small> 199RB <small>/Month</small>',
                        'price_yearly'  => '<small>IDR</small> 1.990.000 <small>/Year</small>',
						'total_product'	=> '1000 Product',
						'total_storage' => '1.5GB Storage',
						'reporting'		=> 'Full Report For Orders, Product and Payment',
						'analytic'		=> 'Statistic Report (Analytics)',
						'support'		=> 'Priority Support 2'

					),

				'ultimate' => array(

						'title' 		=> 'ULTIMATE',
						'description' 	=> 'Suitable for online shop that has a lot of products and get number 1 priority service. The design can request on demand.',
                        'price_monthly'	=> '<small>IDR</small> 499RB <small>/Month</small>',
                        'price_yearly'  => '<small>IDR</small> 4.990.000 <small>/Year</small>',
						'total_product'	=> 'Unlimited Product',
						'total_storage' => '4GB Storage',
						'reporting'		=> 'Full Report For Orders, Product and Payment',
						'analytic'		=> 'Statistic Report (Analytics)',
						'support'		=> 'Priority Support 1'

					),

				'free' => array(

						'title' 		=> 'FREE',
						'description' 	=> 'Beginners who want to start selling in Online Store with limited number of products',
						'price_monthly'	=> '<small>IDR</small> 0 <small>/Month</small>',
						'total_product'	=> '20 Produk',
						'total_storage' => '35MB Storage',
						'support'		=> 'Limited Support'

					),

				'enterprise' => array(

						'title' 		=> 'ENTERPRISE',
						'description' 	=> 'Big scale online shop with customization features and design without limits',
						'price_monthly'			=> '-- CALL --',
					),

			),
		'package_btn' => 'Orders Now',

		'satisfaction' => array(

				'title'	=> 'SATISFATION GUARANTEED',
				'content' 	=> 'YOUR SUCCESS IS OUR SUCCESS ALSO.'
			),

		'question_answer' => array(

				array(
					'title' => 'Cost installation',
					'content' => 'No.'
 					),

				array(
					'title' => 'Cost for trial Jarvis Store',
					'content' => 'No.'
 					),

				array(
					'title' => 'Close your account and stop package subscriber',
					'content' => 'Any time you can tell us'
 					),

				array(
					'title' => 'Subscriber',
					'content' => 'a Month, a Year. If more than a Year just tell us and get a Special Price :)'
 					),

				array(
					'title' => 'Discount ?',
					'content' => 'Yes. We give you 10% discount if you order for PREMIUM or ULTIMATE package for a year and more.'
 					),

				array(
					'title' => 'Domain ?',
					'content' => 'You can use your own domain if you have, or Order Domain with us.'
 					),

				array(
					'title' => 'Bandwidth?',
					'content' => 'Unlimited bandwidth for all package.'
 					),

				array(
					'title' => 'Web hosting?',
					'content' => 'You do not have to thinking hosting for your website.'
 					),

				array(
					'title' => 'Upgrade Package',
					'content' => 'Any time and Any where just tell us and we will give you special price :)'
 					),

				array(
					'title' => 'Special Request ?',
					'content' => 'Just call us in line <a href="#">+6222 618 355 35</a> or email <a href="mailto:info@jarvis-store.com">info@jarvis-store.com</a>, we will help you for your request to make a Online Shop.'
 					),


			),


	);
