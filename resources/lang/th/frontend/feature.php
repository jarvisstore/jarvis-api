<?php
return array(
	
	'title' 				=> 'FEATURES OF JARVIS',
	'title_description' 	=> 'SETTING UP AN ONLINE STORE HAS NEVER BEEN THIS FAST AND EASY',

	'responsive_design' 	=> array(

		'title' 		=> 'Responsive Design',

		'feature_1' 	=> array(
				'title' 	=> 'ONLINE SHOP WITHIN YOUR HANDS',
				'content' 	=> 'Each of our online shop template designs is responsive, which allows customers to access and buy things in your online shop anywhere, via tablets or even smartphones.'
				),

		'feature_2' 	=> array(
					'title' 	=> 'CONTROL YOUR SHOP ANYWHERE',
					'content' 	=> 'Beside your online shop, admin page of Jarvis Store also use a responsive template that allow you to control, to input your goods, even to manage your orders anywhere via laptops, tablets, or smartphones.'
				),
		
		'feature_3' 	=> array(
				'title' 	=> 'YOUR SHOP IN EVERY DEVICE',
				'content' 	=> 'The rapid increment of people who shop online via tablets and smartphones (37% of all e-commerce transactions were via mobile device; data of year 2013), cause the ownership of online shop that has good interface in tablets and smartphones became very important. Our interface will give your customers an easy and fun online shopping experience.'
				),

		'feature_4' 	=> array(
				'title' 	=> 'WEB DESIGN SPECIALIST',
				'content' 	=> 'If you want a personal customable web design, our Web Design Specialist definitely can be handy. We have our expert Web Design Specialist ready to help you. Call for your request by sending use an email to <a href="mailto:info@jarvis-store.com">info@jarvis-store.com</a>.'
				),
	),

	'power_up'				=> array(

		'title'		=> 'Power-Up System',

		'feature_1' => array(
			'title' => 'MODERNIZE YOUR WEBSITE WITH ADDITIONAL FEATURES',
			'content' => "Jarvis Store's Power-Up system allows you to add various new features to your website easily. By using Power-up, you can add new marketing features, SEO features, Discount system, and many more."
			),

		'feature_2' => array(
			'title' 	=> 'INTEGRATED WITH COMPREHENSIVE BLOG PLATFORM',
		'content'	=> 'Jarvis Store is itegrated with a blog system that allow you to create blogs directly from your online shop. Articles will be needed by your customers to give them the information and knowledge about your products. This is a good internet marketing system.'
			)

	),

	'marketing_seo'			=> array(

			'title'		=> 'Marketing & SEO',

			'feature_1' => array(

				'title' => 'SEARCH ENGINE OPTIMIZATION',

				'content' => 'Your customers should be able to find your online shop through search engines like Google of Bing. By integrating SEO in CMS, Jarvis Store support SEO optimization like H1 modification, Title, Meta Tag, and SEO friendly URL.'
				),

			'feature_2'	=> array(

				'title' => 'DISCOUNT CODE AND COUPONT FEATURES',
				'content' => "Every Jarvis Store's package has been integrated with coupont system to give discounts for your customers in order to help your products promotion."
				),

			'feature_3'	=> array(

				'title' => 'SOCIAL MEDIA INTEGRATION',
				'content' => 'Every website in Jarvis Store has been integrated with Social Medias that allow your customers to share and recommend your product in their social media account.'
				),

			'feature_4'	=> array(

				'title' => 'EMAIL MARKETING',
				'content' => 'Jarvis Store is integrated with email marketing service by Mailchimp. It allows you to create beautiful email marketing with ease.'
				),

			'feature_5'	=> array(

				'title' => 'COMPLETE STATISTIC',
				'content' => 'Jarvis Store has complete analytics features to analyze your selling performance and website visits. This information will allow you to decide the best strategy to boost your selling.'
				),

		),
	
	'management_member'		=> array(

			'title'		=> 'Customers Management',

			'feature_1'		=> array(

				'title' => 'COMPLETE CUSTOMERS MANAGEMENT SYSTEM',
				'content' => 'Jarvis Store has a complete customers management system that allows you to see all reports about customers transactions, customer profiles, and also to create invoices.'

				),

			'feature_2'		=> array(

				'title' => 'NOTIFICATION SYSTEM',
				'content' => "Jarvis Store's notification system allow you to stay updated for orders and payment confirmation. Moreover, notification system inside the admin system will help you to process orders from your customers."

				),

			'feature_3'		=> array(

				'title' => 'MEMBES AND RESELLERS',
				'content' => 'Jarvis Store has two customer types, they are members and resellers. This will become useful if you have resellers that have different price base with the other customers.'

				),

			'feature_4'		=> array(

				'title' => "CUSTOMERS' TESTIMONIALS",
				'content' => 'Testimonial resembles your customers satisfaction. Jarvis Store has this system, which allow you to show testimonials from your customers in your webisites.'

				),

			'feature_5'		=> array(

				'title' => 'CUSTOMER CATEGORIES',
				'content' => 'Jarvis Store allows you to cluster your customers into several categories. You can categorize them based on locations, transactions, or whatever you need.'

				),
		),

	'shopping_cart'			=> array(

			'title'		=> 'Shopping Cart',

			'feature_1' => array(

					'title' => 'SHOPPING WITH EASE',
					'content' => 'Shopping in Jarvis Store is super easy and super safe. Your customers can shop quickly in confidence.' 
				),

			'feature_2' => array(

					'title' => 'CUSTOMABLE DELIVERY EXPEDITION',
					'content' => 'Beside JNE, Tiki, and POS Indonesia, you can add more expedition that you use to deliver your products to your customers.'
				),
			'feature_3' => array(

					'title' => 'PAYMENT THROUGH BANK TRANSFERS AND PAYPAL',
					'content' => "Jarvis Store supports transaction with payment through bank transfers and also PayPal. It's easy to activate from your admin page"
				),
			'feature_4' => array(

					'title' => 'SSL CERTIFICATION',
					'content' => 'If you need SSL (Secure Socket Layer) certificate, Jarvis Store provide SSL 128bit to guarantee your information encrypted. This SSL is same with what worldwide major banks use.'
				),
		),

	'unlimited_hosting'		=> array(

			'title' 	=> 'Unlimited Hosting',

			'feature_1' => array(

					'title'	=> 'USE YOUR VERY OWN DOMAIN',
					'content'	=> 'Jarvis Store lets you to register your domain for your website. You can use our domain registry service, use your owned domain, or use our free domain.'

				),

			'feature_2' => array(

					'title'	=> 'UNLIMITED BANDWIDTH',
					'content'	=> 'Unlimited bandwidth for limitless data transfer.'

				),

			'feature_3' => array(

					'title'	=> 'SET-UP FREELY WITH EASE',
					'content'	=> "Your website is automatically created by our system. You don't have to deal with too many things."

				),

			'feature_4' => array(

					'title'	=> '99.95% UPTIME',
					'content'	=> "We're working with a server service provider that have the best uptime reputation to guarantee your website stay online"

				),

		),

	'support'				=> array(

			'title' => '24/7 Support',

			'feature_1'	=> array(

					'title' => 'FOCUS ON CLIENT AND PRODCTS',
					'content' 	=> "We're listening to you. We'll deliver your response to our team. We always try to give our best service and support only for you. Because you deserve nothing but the best."
				),

			'feature_2'	=> array(

					'title' => 'KNOWLEDGE BASE',
					'content' 	=> 'Knowledge Base Jarvis Store provides various information that you need about system, e-commerce, and internet business.'
				),

			'feature_3'	=> array(

					'title' => 'JARVIS STORE TUTORIAL',
					'content' 	=> "Don't get lost! Beside its easy-to-use service, Jarvis Store also provides step-by-step tuorial that is ready to guide you."
				),

			'feature_4'	=> array(

					'title' => 'COMMUNITY IN JARVIS STORE FORUM',
					'content' 	=> 'We have a forum where you can discuss and change your information with your fellow users.'
				),
		),

	'reporting'				=> array(

			'title' => 'Reporting And Invoice',

			'feature_1' 	=> array(

					'title' => 'TIME BASED REPORT',
					'content' => 'This feature will allow you to choose and creat transactional report based on time that you want, whether daily, weekly, monthly, or even annual.'

				),

			'feature_2' 	=> array(

					'title' => 'IMPORT AND EXPORT DATA',
					'content' => 'Simplify your data importing and back-up with our import and export system using .CSV data.'

				),

			'feature_3' 	=> array(

					'title' => 'REPOT OF EVERYTHING',
					'content' => 'Report of customers, products, transactions, collections, and many more. Everything you need can be created and backed-up by a few clicks only.'

				),

			'feature_4' 	=> array(

					'title' => 'STATISTIC WEB REPORT',
					'content' => 'Statistic Web report with infographs about your whole web to the details.'

				),
		),
 
);
