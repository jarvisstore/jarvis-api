<?php

return array(

    'title' => 'PAYMENT CONFIRMATION',
    'title_description'     => '',
    'domain_name' => 'Domain',
    'email' =>  'Email',
    'payment_method' => 'Payment Method',
    'destination_account' => 'Bank Account',
    'total_payment' => 'Ammount',
    'payment_date' => 'Payment Date (date/month/year)',
    'account_owner' => "Account's holder",
    'account_no'    => 'Account Number',

);
