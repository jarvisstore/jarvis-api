<?php

return array(

    'title' => 'LOGIN TO YOUR SHOP HERE!',
    'title_description' 	=> 'ENTER YOUR EMAIL ADDRESS AND PASSWORD!',
    'button_success' => 'Click here to continue to your shop.'
);
