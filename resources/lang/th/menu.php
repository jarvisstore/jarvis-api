<?php

return array(

	'frontend' => array(

			'home' => '',
			'feature' => 'คุณสมบัติ',
            'portfolio' =>  'ผลงาน',
            'blog'      =>  'บล็อก',
			'pricing' => 'การตั้งราคา',
			'service' => 'บริการ',
			'template' => 'แม่แบบร้านค้าออนไลน์',
			'power_up' => 'เพิ่มพลัง',
			'tutorial' => 'กวดวิชา',
			'contact' => 'ติดต่อ',
			'partner' => 'หุ้นส่วน',
			'payment_confirmation' => 'ยืนยันการชำระเงิน',
			'login' => 'เข้าสู่ระบบ',

		),

	);
