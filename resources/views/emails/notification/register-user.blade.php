@extends('emails.notification.main-member')

@section('content')
   <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;">
      <tr>
         <td valign="top" id="templateHeader" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 0;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
               <tbody class="mcnTextBlockOuter">
                  <tr>
                     <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <!--[if mso]>
                        <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                           <tr>
                              <![endif]-->
                              <!--[if mso]>
                              <td valign="top" width="600" style="width:600px;">
                                 <![endif]-->
                                 <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                                    <tbody>
                                       <tr>
                                          <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;">
                                             <div style="text-align: center;">
                                                <span style="font-size:24px"><span style="color:#ff0000"><strong>Pendaftaran Member</strong></span></span>
                                                <br>
                                                <hr>
                                                {{$data['toko']}}
                                             </div>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 <!--[if mso]>
                              </td>
                              <![endif]-->
                              <!--[if mso]>
                           </tr>
                        </table>
                        <![endif]-->
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td valign="top" id="templateBody" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FFFFFF;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 2px solid #EAEAEA;padding-top: 0;padding-bottom: 9px;">
            <style type="text/css">
               ol {
                  list-style-type: none;
                  font-weight: bold;
                  
               }
               ol li{
                  text-align: center;
               }
               p:nth-child(4n),p:nth-child(5n) {
                  background-color: #000000;
                  color: #fff !important;
                  padding: 10px;
                  text-align: center !important;
                  margin:0;
                  font-weight: bold;
               }
               ol:last-of-type {
                  text-transform: capitalize;
                  padding: 0;
               }
            </style>

            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCodeBlock" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
               <tbody class="mcnTextBlockOuter">
                  <tr>
                     <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;">
                        {{$content}}
                     </td>
                  </tr>
               </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
               <tbody class="mcnTextBlockOuter">
                  <tr>
                     <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <!--[if mso]>
                        <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                           <tr>
                              <![endif]-->
                              <!--[if mso]>
                              <td valign="top" width="600" style="width:600px;">
                                 <![endif]-->
                                 <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                                    <tbody>
                                       <tr>
                                          <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;">
                                             <hr>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 <!--[if mso]>
                              </td>
                              <![endif]-->
                              <!--[if mso]>
                           </tr>
                        </table>
                        <![endif]-->
                     </td>
                  </tr>

               </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
               <tbody class="mcnTextBlockOuter">
                  <tr>
                     <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <!--[if mso]>
                        <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                           <tr>
                              <![endif]-->
                              <!--[if mso]>
                              <td valign="top" width="600" style="width:600px;">
                                 <![endif]-->
                                 <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                                    <tbody>
                                       <tr>
                                          <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;">
                                             <div style="text-align: center;"><span style="font-size:14px"><span style="color:#ff0000"><strong>{{parse_url($data['loginLink'])['host']}}</strong></span></span></div>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 <!--[if mso]>
                              </td>
                              <![endif]-->
                              <!--[if mso]>
                           </tr>
                        </table>
                        <![endif]-->
                     </td>
                  </tr>
               </tbody>
            </table>
            
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
               <tbody class="mcnTextBlockOuter">
                  <tr>
                     <td valign="top" class="mcnTextBlockInner" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        
                        <div align="center">
                           <div class="corner" style="color: white;padding: 10px 20px;background: #00c853;width: 200px;border-radius: 10px;font-family: arial;text-align: center;"><a style="color: #fff;text-decoration: none;" href="{{$data['loginLink']}}"><strong>Login Sekarang</strong></a></div>
                        </div>
                     </td>
                  </tr>
               </tbody>
            </table>
            
            <br><br>
         </td>
      </tr>
      <tr>
         <td valign="top" id="templateFooter" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
               <tbody class="mcnTextBlockOuter">
                  <tr>
                     <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                        <!--[if mso]>
                        <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                           <tr>
                              <![endif]-->
                              <!--[if mso]>
                              <td valign="top" width="600" style="width:600px;">
                                 <![endif]-->
                                 <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                                    <tbody>
                                       <tr>
                                          <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: center;">
                                             <em>Email ini dikirimkan karena Anda melakukan transaksi di <a href="{{$data['linkRegistrasi']}}" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #656565;font-weight: normal;text-decoration: underline;"><span style="color:#ff0000">{{parse_url($data['linkRegistrasi'])['host']}}</span></a></em>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 <!--[if mso]>
                              </td>
                              <![endif]-->
                              <!--[if mso]>
                           </tr>
                        </table>
                        <![endif]-->
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </table>
@endsection