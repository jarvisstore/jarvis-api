<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>*|SUBJECT|*</title>
		
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust: none;margin: 0;padding: 0;background-color: #DCEAEE;background-image: url(http://gallery.mailchimp.com/27aac8a65e64c994c4416d6b8/images/bg_sw_greenblue.png);background-position: top left;background-repeat: repeat;width: 100%;">
    	<center>
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="margin: 0;padding: 0;background-color: #DCEAEE;background-image: url(http://gallery.mailchimp.com/27aac8a65e64c994c4416d6b8/images/bg_sw_greenblue.png);background-position: top left;background-repeat: repeat;height: 100%;width: 100%;">
                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse;">
                        <!-- // BEGIN PREHEADER -->
                        
                        <!-- END PREHEADER \ -->
                    </td>
                </tr>
            	<tr>
                	<td align="center" valign="top" style="padding-top: 20px;padding-bottom: 40px;border-collapse: collapse;">
                    	<!-- // BEGIN CONTAINER -->
                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                	<!-- // BEGIN HEADER -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                                    	<tr>
                                        	<td align="center" valign="top" style="border-collapse: collapse;">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                	<tr>
                                                        <td class="headerContent" style="border-collapse: collapse;color: #505050;font-family: Georgia;font-size: 20px;font-weight: bold;line-height: 100%;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;text-align: left;vertical-align: middle;">
                                                            <img src="http://jarvis-store.com/frontend/assets/images/partnership1.jpg" style="max-width: 600px;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" id="headerImage campaign-icon" mc:label="header_image" mc:edit="header_image" mc:allowdesigner="" mc:allowtext=""><br><br>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- END HEADER \ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                	<!-- // BEGIN BODY -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody" style="background-color: #FFFFFF;border-bottom: 0;">
                                    	<tr>
                                        	<td align="center" valign="top" style="border-collapse: collapse;">
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top" class="bodyContent" mc:edit="body_content" style="border-collapse: collapse;color: #808080;font-family: Verdana;font-size: 19px;line-height: 150%;text-align: center;"><h1 class="tpl-content-highlight" style="color: #ce3024;display: block;font-family: Verdana;font-size: 30px;font-style: normal;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: center;">
	Paket Anda Akan Berakhir :(</h1>
                                                            <span style="font-size:18px;"><span style="font-family: verdana, geneva, sans-serif;">Paket akun anda, (*|NAMATOKO|*) akan berakhir pada <strong>*|EXPIRED|*</strong>, untuk melakukan perpanjangan paket silahkan klik tombol berikut </span></span></td>
                                                    </tr>
                                                    <tr>
                                                    	<td align="center" valign="top" style="padding-top: 0;padding-bottom: 40px;border-collapse: collapse;">
                                                            <table border="0" cellpadding="15" cellspacing="0" class="templateButton" style="-moz-border-radius: 5px;-webkit-border-radius: 5px;background-color: #ce3024;border: 0;border-radius: 5px;">
                                                                <tr>
                                                                    <td align="center" valign="middle" class="templateButtonContent" style="padding-left: 25px;padding-right: 25px;border-collapse: collapse;color: #FFFFFF;font-family: Verdana;font-size: 15px;font-weight: normal;letter-spacing: .5px;text-align: center;text-decoration: none;" mc:edit="template_button_content"><a href="http://jarvis-store.com/telkomsel/upgrade" target="_self" style="color: #FFFFFF;font-family: Verdana;font-size: 15px;font-weight: normal;letter-spacing: .5px;text-align: center;text-decoration: none;">Paket Telkomsel</a></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    	<td align="center" valign="top" class="templateDataTable" style="border-collapse: collapse;background-color: #0F778E;border-top: 0;border-bottom: 0;">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" valign="top" style="border-collapse: collapse;">
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td align="left" valign="top" style="padding-top: 10px;border-collapse: collapse;">
                                                                                    <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                                                        <tr mc:repeatable="">
                                                                                            <th valign="top" width="125" class="dataTableHeading" mc:edit="data_table_heading01" style="border-top: 0;border-bottom: 1px dotted #FFFFFF;background-color: #0F778E;color: #FFFFFF;font-family: Verdana;font-size: 12px;font-weight: bold;line-height: 150%;text-align: left;">Bantuan</th>
                                                                                            <td valign="top" width="" class="dataTableContent" mc:edit="data_table_content01" style="border-collapse: collapse;border-top: 0;border-bottom: 1px dotted #FFFFFF;color: #FFFFFF;font-family: Verdana;font-size: 12px;font-weight: normal;line-height: 150%;text-align: left;">Jika ada kesulitan dan berbagai pertanyaan seputar Jarvis Store, anda bisa menghubungi kontak support kami <a href="http://member.jarvis-server.com/contact.php" target="_self" style="color: #f7f1b0;font-weight: bold;text-decoration: underline;">DISINI</a></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- END BODY \ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                	<!-- // BEGIN FOOTER -->
                                	<table border="0" cellpadding="20" cellspacing="0" width="600" id="templateFooter" style="background-color: #FFFFFF;border-top: 0;border-bottom: 0;">
                                    	<tr>
                                        	<td align="center" valign="top" style="border-collapse: collapse;">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                    	<td valign="top" class="footerContent" mc:edit="footer_social" style="border-collapse: collapse;color: #808080;font-family: Helvetica;font-size: 10px;line-height: 150%;text-align: left;"> <a href="http://jarvis-store.com/telkomsel" target="_self" style="color: #606060;font-weight: normal;text-decoration: underline;">Jarvis Store</a>&nbsp;| <a href="http://telkomsel.com/jarvis" target="_self" style="color: #606060;font-weight: normal;text-decoration: underline;">Telkomsel</a></td>
                                                    </tr>
                                                	<tr>
                                                        <td valign="top" class="footerContent" style="padding-top: 20px;border-collapse: collapse;color: #808080;font-family: Helvetica;font-size: 10px;line-height: 150%;text-align: left;" mc:edit="footer_content">
                                                            <em>Copyright &copy; 2015-2016 PT. Mora Pratama Kreasindo in collaboration with Telkomsel, All rights reserved.</em>
                                                            <br>
                                                        </td>
                                                    </tr>
                                                    
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- END FOOTER \ -->
                                </td>
                            </tr>
                        </table>
                        <!-- END CONTAINER \ -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>