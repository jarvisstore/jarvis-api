
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www=
  .w3.org/TR/REC-html40/loose.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Jarvis Store</title>
</head>
<body style='font-family:"ProximaNovaLight", "Helvetica Neue Light", "H=
elvetica Neue", Helvetica, Arial, sans-serif;margin:0;padding:0 0 30px 0;=
background:#fff;line-height:24px;text-align:left'>
<table width="100%" cellspacing="0" cellpadding="0" class="top"=
style="background:#000;height:48px"><tr>
<td align="center">
  <table cellspacing="0" cellpadding="0"><tr>
    <td width="580" style="padding:0 10px;text-align:left">
      <a href="http://jarvis-store.com" target="_blank"> <img src="http://jarvis-store.com/frontend/assets/images/logo.png" alt="Jarvis Store Logo"></a>
      </td>
    </tr></table>
  </td>
</tr></table>
<!-- header -->
<table width="100%" cellspacing="0" cellpadding="0" =
class="header welcome" style="background:#f2f7fa;padding:35px 0;borde=
r-bottom:1px solid #e9edf0"><tr>
<td align="center">
  <table cellspacing="0" cellpadding="0"><tr>
    <td width="580" style="padding:0 10px;text-align:left">
      <br>
      <h1 style="text-align:left;margin:0;padding:0;color:#00=
      0;font-size:32px;font-weight:300">Masa aktif toko kamu sudah habis</h1>
    </td>
  </tr>
</table>
</td>
</tr></table>
<!-- main content --><table width="100%" cellspacing="0" cellpadding==
"0" class="content"><tr>
<td align="center">
  <table cellspacing="0" cellpadding="0"><tr>
    <td width="580" style="padding:35px 10px;text-align:left">    
      <p class="large" style="color:#000;margin-bottom:25px;line-height:25p=
      x;font-size:17px"><strong>Hi {{$akun->namaToko}},</strong></p>
      <p style="color:#000;font-size:16px;line-height:25px;margin-bottom:25px=
      ">Masa aktif toko kamu sudah habis, dan sekarang toko kamu menjadi paket FREE.</p>

      <p style="color:#000;font-size:16px;line-height:25px;margin-bottom:25px=
      ">Ingin kembali menikmati semua fitur dari Jarvis Store.? Kamu bisa memilih lagi beberapa paket harga yang kami sediakan di jarvis store.</p>

      <p style="color:#000;font-size:16px;line-height:25px;margin-bottom:25px=
      "><a class="button" href="http://jarvis-store.com" style="-moz-border-radius:1px;-webkit-border=
      -radius:1px;display:inline-block;border-radius:1px;text-shadow:0 1px 1px =
      #6c8a34;font-weight:bold;padding:0 20px;line-height:45px;background:#96bf=
      48;color:#fff !important;text-decoration:none !important">Pilih Paket</a></p>

      <hr style="border-top:1px solid #f0f0f0;color:#fff;height:1px;backgroun=
      d-color:#fff;border-bottom:none;border-left:none;border-right:none;margin=
      :30px 0 25px">      
      </td>
    </tr></table>
  </td>
</tr></table>

    </body>
    </html>
