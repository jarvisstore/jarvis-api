<!DOCTYPE html>
<html>
	<head>
        @if(isset($metatag))
        	@if($metatag == 1)
			<meta name="SInameBuyer" content="{{$nama}}" />
			<meta name="SIdomain" content="{{$web}}" />
			<meta name="SIemailBuyer" content="{{$email}}" />
			<meta name="SIphoneBuyer" content="{{formatTelp($telp)}}" />
	        @for($i=0; $i < $jmlProduk->count(); $i++)

	        <meta name="SIproduct" productUrl="{{slugProduk($jmlProduk[$i]->produk)}}" productName="{{$jmlProduk[$i]->produk->nama}}"/>

	        @endfor
        	@endif
        @endif
	</head>
	<body>
        {{$data}}
	</body>
</html>
