<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>*|SUBJECT|*</title>
    
    <style type="text/css"></style></head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust: none;margin: 0;padding: 0;background-color: #DCEAEE;background-image: url(http://gallery.mailchimp.com/27aac8a65e64c994c4416d6b8/images/bg_sw_greenblue.png);background-position: top left;background-repeat: repeat;width: 100%;">
      <center>
          <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="margin: 0;padding: 0;background-color: #DCEAEE;background-image: url(http://gallery.mailchimp.com/27aac8a65e64c994c4416d6b8/images/bg_sw_greenblue.png);background-position: top left;background-repeat: repeat;height: 100%;width: 100%;">
                <tbody><tr>
                    <td align="center" valign="top" style="border-collapse: collapse;">
                        <!-- // BEGIN PREHEADER -->
                        <table border="0" cellpadding="10" cellspacing="0" width="100%" id="templatePreheader">
                            <tbody><tr>
                                <td align="center" valign="top" style="border-collapse: collapse;">
                                    
                                </td>
                            </tr>
                        </tbody></table>
                        <!-- END PREHEADER \ -->
                    </td>
                </tr>
              <tr>
                  <td align="center" valign="top" style="padding-top: 20px;padding-bottom: 40px;border-collapse: collapse;">
                      <!-- // BEGIN CONTAINER -->
                        <table border="0" cellpadding="0" cellspacing="0" id="templateContainer" style="
    width: 100%;
    max-width: 600px;
">
                          <tbody><tr>
                              <td align="center" valign="top" style="border-collapse: collapse;">
                                  <!-- // BEGIN HEADER -->
                                  <table border="0" cellpadding="0" cellspacing="0" id="templateHeader" style="
    width: 100%;
    max-width: 600px;
">
                                      <tbody><tr>
                                          <td align="center" valign="top" style="border-collapse: collapse;">
                                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                  <tbody><tr>
                                                        <td class="headerContent" style="border-collapse: collapse;color: #505050;font-family: Georgia;font-size: 20px;font-weight: bold;line-height: 100%;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;text-align: left;vertical-align: middle;">
                                                            <img src="https://gallery.mailchimp.com/c0dd24d271/images/logo.1.png" style="max-width: 600px;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" id="headerImage campaign-icon" mc:label="header_image" mc:edit="header_image" mc:allowdesigner="" mc:allowtext=""><br><br>
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                    <!-- END HEADER \ -->
                                </td>
                            </tr>
                          <tr>
                              <td align="center" valign="top" style="border-collapse: collapse;">
                                  <!-- // BEGIN BODY -->
                                  <table border="0" cellpadding="0" cellspacing="0" id="templateBody" style="background-color: #FFFFFF;border-bottom: 0;width: 100%;max-width: 600px;">
                                      <tbody><tr>
                                          <td align="center" valign="top" style="border-collapse: collapse;">
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                    <tbody><tr>
                                                        <td valign="top" class="bodyContent" mc:edit="body_content" style="border-collapse: collapse;color: #808080;font-family: Verdana;font-size: 19px;line-height: 150%;text-align: center;"><h1 class="tpl-content-highlight" style="color: #ce3024;display: block;font-family: Verdana;font-size: 30px;font-style: normal;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: center;">
  Verifikasi Akun Jarvis Store</h1>
<span style="font-size:18px;"><span style="font-family: verdana, geneva, sans-serif;">Hello member, Kamu bisa memverifikasi akun kamu dengan mengklik tombol di bawah ini.</span></span></td>
                                                    </tr>
                                                    <tr>
                                                      <td align="center" valign="top" style="padding-top: 0;padding-bottom: 40px;border-collapse: collapse;">
                                                            <table border="0" cellpadding="15" cellspacing="0" class="templateButton" style="-moz-border-radius: 5px;-webkit-border-radius: 5px;background-color: #ce3024;border: 0;border-radius: 5px;">
                                                                <tbody><tr>
                                                                    <td align="center" valign="middle" class="templateButtonContent" style="padding-left: 25px;padding-right: 25px;border-collapse: collapse;color: #FFFFFF;font-family: Verdana;font-size: 15px;font-weight: normal;letter-spacing: .5px;text-align: center;text-decoration: none;" mc:edit="template_button_content"><a href="*|URLTOKO|*" target="_self" style="color: #FFFFFF;font-family: Verdana;font-size: 15px;font-weight: normal;letter-spacing: .5px;text-align: center;text-decoration: none;">Verifikasi Akun</a></td>
                                                                </tr>
                                                            </tbody></table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                      <td align="center">
                                                        <hr style="border: 1px dashed #bebebe;">
                                                        <p style="font-family: 'verdana','geneva','sans-serif'; font-size: 13px; color: #555;">atau klik link di bawah ini untuk memverifikasi akun:</p>
                                                        <a href="*|URLTOKO|*" target="_self" style="font-family: Verdana; font-size: 12px; text-decoration: none; font-weight: normal;">*|URLTOKO|*</a>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <td align="center" valign="top" style="border-collapse: collapse;background-color: #0F778E;border-top: 0;border-bottom: 0;">
                                                            
                                                        </td>
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                    <!-- END BODY \ -->
                                </td>
                            </tr>
                          <tr>
                              <td align="center" valign="top" style="border-collapse: collapse;">
                                  <!-- // BEGIN FOOTER -->
                                  <table border="0" cellpadding="20" cellspacing="0" id="templateFooter" style="background-color: #FFFFFF;border-top: 0;border-bottom: 0;max-width: 600px;width: 100%;">
                                      <tbody><tr>
                                          <td align="center" valign="top" style="border-collapse: collapse;">
                                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody><tr>
                                                      <td valign="top" class="footerContent" mc:edit="footer_social" style="border-collapse: collapse;color: #808080;font-family: Helvetica;font-size: 10px;line-height: 150%;text-align: left;"><a href="http://twitter.com/jarvis_store" target="_self" style="color: #606060;font-weight: normal;text-decoration: underline;">Twitter</a> | <a href="http://facebook.com/jarvisstore" target="_self" style="color: #606060;font-weight: normal;text-decoration: underline;">Facebook</a> | <a href="http://member.jarvis-server.com/contact.php" target="_self" style="color: #606060;font-weight: normal;text-decoration: underline;">Kontak &amp; Bantuan</a>&nbsp;| <a href="http://member.jarvis-server.com/" target="_self" style="color: #606060;font-weight: normal;text-decoration: underline;">Member Area</a></td>
                                                    </tr>
                                                  <tr>
                                                        <td valign="top" class="footerContent" style="padding-top: 20px;border-collapse: collapse;color: #808080;font-family: Helvetica;font-size: 10px;line-height: 150%;text-align: left;" mc:edit="footer_content">
                                                            <em>Copyright © 2013-2015 Jarvis Store, All rights reserved.</em>
                                                            <br>
                                                            
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                    <!-- END FOOTER \ -->
                                </td>
                            </tr>
                        </tbody></table>
                        <!-- END CONTAINER \ -->
                    </td>
                </tr>
            </tbody></table>
        </center>
    
</body></html>