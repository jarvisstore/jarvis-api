FORMAT: 1A

# JarvisApi

# Auth [/auth]

## Login. [POST /auth/login]
Login to system using email & password.

+ Request (application/json)
    + Body

            [
                "email",
                "password"
            ]

+ Response 404 (application/json)
    + Body

            {
                "message": "user with that credential not found",
                "status_code": 404
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmphcnZpcy5sb2NhbC9hdXRoL2xvZ2luIiwiaWF0IjoxNTAxMTMwMTcyLCJleHAiOjE1MDExMzM3NzIsIm5iZiI6MTUwMTEzMDE3MiwianRpIjoibFUza25Zakk5Tmd5dTE5ZyIsInN1YiI6Mzc0Mn0.7TTs-kwAwSvnDRg-Q_nxG9NKXE5x84Skco_p334fMhk",
                "user": {
                    "data": {
                        "id": 3742,
                        "name": "",
                        "email": "ajaajak@mailinator.com",
                        "address": "",
                        "telp": ""
                    }
                },
                "account": {
                    "id": 4024,
                    "shop_name": "joools",
                    "setup": true
                }
            }

## Register. [POST /auth/register]
Register new user to jarvis

+ Request (application/json)
    + Body

            [
                "shop_name",
                "email",
                "password"
            ]

+ Response 422 (application/json)
    + Body

            {
                "message": "The shop name field is required.,The Email Address has already been taken.",
                "status_code": 422
            }

+ Response 200 (application/json)
    + Body

            {
                "url": "http:\/\/joools.jstore.co\/account_setup",
                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBpLmphcnZpcy5sb2NhbC9hdXRoL3JlZ2lzdGVyIiwiaWF0IjoxNTAxMTI5OTM3LCJleHAiOjE1MDExMzM1MzcsIm5iZiI6MTUwMTEyOTkzNywianRpIjoiUEFTd3hiWDltam42b3A3MyIsInN1YiI6Mzc0Mn0.VmlUww5ggUv94nhklkvF56s0k6V98ezr_DFYCh3ziS4",
                "setup": true,
                "account": {
                    "id": 4024,
                    "shop_name": "joools",
                    "setup": true
                }
            }

## Store setup. [POST /auth/setup/{id}]
Setup store after register

+ Request (application/json)
    + Headers

            Authorization: bearer your-token-here
    + Body

            [
                "shop_name",
                "owner_name",
                "address",
                "telp",
                "country_id",
                "province_id",
                "city_id",
                "subdistrict_id",
                "post_code",
                "category_id"
            ]

+ Response 422 (application/json)
    + Body

            [
                "The shop name field is required.",
                "The owner name field is required.",
                "The address field is required.",
                "The telp field is required.",
                "The country id field is required.",
                "The province id field is required.",
                "The city id field is required.",
                "The subdistrict id field is required.",
                "The post code field is required.",
                "The category id field is required."
            ]

## Install Theme. [POST /auth/install-theme/{id}]
Setup store after register

+ Parameters
    + id: (string, optional) - Theme id

+ Request (application/json)
    + Headers

            Authorization: bearer your-token-here

+ Response 200 (application/json)
    + Body

            {
                "success": true,
                "message": "Successfully install the theme .",
                "storage": 0
            }

## Forget Password. [POST /auth/forget-password]
Forget password

+ Request (application/json)
    + Body

            {
                "email": "user email address"
            }

+ Response 200 (application/json)
    + Body

            {
                "success": true,
                "message": "Successfully install the theme .",
                "storage": 0
            }

# Products [/products]

## Display all products. [GET /products/?bestseller=true,newproduct=true,query='',category='',collection='',limit=15]
Get a JSON representation of all the products.

+ Parameters
    + bestseller: (string, optional) - filter product based on bestseller
        + Default: 
    + newproduct: (string, optional) - filter product based on newproduct
    + query: (string, optional) - search product by name
    + category: (string, optional) - search product by category name
    + collection: (string, optional) - search product by collection name
    + limit: (string, optional) - how many product return, default is 15

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "id": 93886,
                        "name": "Sepatu Duo",
                        "short_description": "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type...",
                        "description": "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<\/p>",
                        "vendor": "Conversa",
                        "sell_price": 10000,
                        "mitra_price": 60000,
                        "discount_price": 150000,
                        "barcode": "8781238145",
                        "weight": 300,
                        "stock": 4,
                        "image1": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/5.gif",
                        "image2": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/5.gif",
                        "image3": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/5.gif",
                        "image4": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/5.gif",
                        "tags": "sepatu,colorful",
                        "dmt": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                        "kmt": "keyword1,keyword2",
                        "visibility": 1,
                        "bestseller": 0,
                        "new_product": 0,
                        "url": "http:\/\/example.jstore.co\/produk\/sepatu-duo",
                        "options_sku": {
                            "data": []
                        },
                        "options": {
                            "data": []
                        }
                    },
                    {
                        "id": 93906,
                        "name": "adsad",
                        "short_description": "<p>asdad<\/p>",
                        "description": "<p>asdad<\/p>",
                        "vendor": "asd",
                        "sell_price": 1,
                        "mitra_price": 0,
                        "discount_price": 1,
                        "barcode": "",
                        "weight": 1,
                        "stock": -1,
                        "image1": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/20160427-140323.png",
                        "image2": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/adsad-1488187713.jpg",
                        "image3": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/adsad-1488187757.jpg",
                        "image4": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/adsad-1489742695.jpg",
                        "tags": "",
                        "dmt": "",
                        "kmt": "",
                        "visibility": 1,
                        "bestseller": 0,
                        "new_product": 0,
                        "url": "http:\/\/example.jstore.co\/produk\/adsad",
                        "category": {
                            "data": {
                                "id": 51697,
                                "name": "Kategori 4",
                                "slug": "kategori-4",
                                "parent": 51695,
                                "order": 1
                            }
                        },
                        "options_sku": {
                            "data": []
                        },
                        "options": {
                            "data": []
                        }
                    },
                    {
                        "id": 93885,
                        "name": "Sepatu Hijau",
                        "short_description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and...",
                        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                        "vendor": "Gajah Duduks",
                        "sell_price": 80000,
                        "mitra_price": 60000,
                        "discount_price": 150000,
                        "barcode": "8781238145",
                        "weight": 300,
                        "stock": 3,
                        "image1": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/4.gif",
                        "image2": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/4.gif",
                        "image3": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/4.gif",
                        "image4": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/4.gif",
                        "tags": "sepatu, colorful",
                        "dmt": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                        "kmt": "keyword1,keyword2",
                        "visibility": 1,
                        "bestseller": 0,
                        "new_product": 0,
                        "url": "http:\/\/example.jstore.co\/produk\/sepatu-hijau",
                        "options_sku": {
                            "data": []
                        },
                        "options": {
                            "data": []
                        }
                    }
                ],
                "meta": {
                    "pagination": {
                        "total": 6,
                        "count": 3,
                        "per_page": 3,
                        "current_page": 1,
                        "total_pages": 2,
                        "links": {
                            "next": "http:\/\/api.jarvis.local\/products?page=2"
                        }
                    }
                }
            }

## Store new products. [POST /products]
Api to create new product .

+ Parameters
    + name: (string, required) - product name
    + description: (string, required) - Product production
    + category_id: (string, required) - Product category
    + sell_price: (string, required) - product sell price
    + mitra_price: (string, optional) - Mitra price, used for mitra member
    + discount_price: (string, required) - discount price
    + discount_price: (string, required) - discount price
    + weight: (string, required) - Product weight in gram
    + stock: (string, required) - Product stock
    + barcode: (string, optional) - Product barcode
    + tags: (string, optional) - Product tags
    + collections: (string, optional) - Product collection in array
    + dmt: (string, optional) - description meta tag
    + kmt: (string, optional) - keyword meta tag
    + visibility: (string, optional) - Product visibility, 0 for hide. 1 for show
    + bestseller: (string, optional) - Product bestseller status, 0 for hide. 1 for show
    + new_product: (string, optional) - Product new product status, 0 for hide. 1 for show
    + have_options: (string, optional) - Product has options status, 0 for not have option sku. 1 for have option sku
    + options: (string, optional) - Product options name in array,ex: Size, Color, etc
    + varians: (string, optional) - Product varian name for every option name in string separated by coma(,),ex: Xl,L,M,S
    + option_status: (string, optional) - option status for the combination, 1 for save to db
    + option_1: (string, optional) - option 1 name
    + option_2: (string, optional) - option 2 name
    + option_3: (string, optional) - option 3 name
    + option_price: (string, optional) - option price
    + option_stock: (string, optional) - option stock
    + option_barcode: (string, optional) - option barcode

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 94917,
                    "name": "Product Options",
                    "short_description": "Ini product A",
                    "description": "Ini product A",
                    "vendor": "Converse",
                    "sell_price": "100000",
                    "mitra_price": "0",
                    "discount_price": "120000",
                    "barcode": "",
                    "weight": "1000",
                    "stock": "10",
                    "image1": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/image1.jpg",
                    "image2": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/image2.jpg",
                    "image3": "",
                    "image4": "",
                    "tags": "",
                    "dmt": "",
                    "kmt": "",
                    "visibility": "0",
                    "bestseller": "1",
                    "new_product": 0,
                    "url": "http:\/\/example.jstore.co\/produk\/product-options",
                    "category": {
                        "data": {
                            "id": 1,
                            "name": "Wanita",
                            "slug": "wanita",
                            "parent": 0,
                            "order": 1
                        }
                    },
                    "options_sku": {
                        "data": []
                    },
                    "options": {
                        "data": [
                            {
                                "id": 7037,
                                "name": "Warna",
                                "value": "Merah,Biru,Hijau"
                            }
                        ]
                    }
                }
            }

## Display 1 product. [GET /products/{id}]
Get a JSON representation of detail the product.

+ Parameters
    + id: (string, optional) - product id

## Update product. [POST /products/{product_id}]
Api to update product .

+ Parameters
    + name: (string, required) - product name
    + description: (string, required) - Product production
    + category_id: (string, required) - Product category
    + slug: (string, required) - Product slug url
    + sell_price: (string, required) - product sell price
    + mitra_price: (string, optional) - Mitra price, used for mitra member
    + discount_price: (string, required) - discount price
    + discount_price: (string, required) - discount price
    + weight: (string, required) - Product weight in gram
    + stock: (string, required) - Product stock
    + barcode: (string, optional) - Product barcode
    + tags: (string, optional) - Product tags
    + collections: (string, optional) - Product collection in array
    + dmt: (string, optional) - description meta tag
    + kmt: (string, optional) - keyword meta tag
    + visibility: (string, optional) - Product visibility, 0 for hide. 1 for show
    + bestseller: (string, optional) - Product bestseller status, 0 for hide. 1 for show
    + new_product: (string, optional) - Product new product status, 0 for hide. 1 for show
    + product_images: (string, optional) - array of product image, index 0 - 3 for image 1 - 4

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 94917,
                    "name": "Product Options",
                    "short_description": "Ini product A",
                    "description": "Ini product A",
                    "vendor": "Converse",
                    "sell_price": "100000",
                    "mitra_price": "0",
                    "discount_price": "120000",
                    "barcode": "",
                    "weight": "1000",
                    "stock": "10",
                    "image1": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/image1.jpg",
                    "image2": "https:\/\/d2kl9mvmw5l7p9.cloudfront.net\/example-upload\/produk\/image2.jpg",
                    "image3": "",
                    "image4": "",
                    "tags": "",
                    "dmt": "",
                    "kmt": "",
                    "visibility": "0",
                    "bestseller": "1",
                    "new_product": 0,
                    "url": "http:\/\/example.jstore.co\/produk\/product-options",
                    "category": {
                        "data": {
                            "id": 1,
                            "name": "Wanita",
                            "slug": "wanita",
                            "parent": 0,
                            "order": 1
                        }
                    },
                    "options_sku": {
                        "data": []
                    },
                    "options": {
                        "data": [
                            {
                                "id": 7037,
                                "name": "Warna",
                                "value": "Merah,Biru,Hijau"
                            }
                        ]
                    }
                }
            }

# Categories [/categories]

## Display all category. [GET /categories]
Get a JSON representation of all the category.

## Display 1 category. [GET /categories/{id}]
Get a JSON representation of 1 the product category.

+ Parameters
    + id: (string, optional) - category id

## Store Category. [POST /categories]
add new product category

+ Request (application/json)
    + Body

            [
                "name",
                "parent",
                "slug"
            ]

## Update Category. [PUT /categories/id]
update product category

+ Parameters
    + id: (string, optional) - product category

+ Request (application/json)
    + Body

            [
                "name",
                "parent",
                "slug"
            ]

## Delete Category. [DELETE /categories/id]
delete product category

+ Parameters
    + id: (string, optional) - product category

# Discount [/discounts]

## Get all discount. [GET /discounts]
Get a JSON representation of all the category.

## Get 1 category. [GET /discounts/{id}]
Get a JSON representation of 1 discount.

+ Parameters
    + id: (string, optional) - discount id

## Store Discount. [POST /discounts]
add new product Discount

+ Request (application/json)
    + Body

            [
                "code",
                "total",
                "discount",
                "type",
                "minimal_order",
                "start_date",
                "end_date",
                "product_id",
                "category_id",
                "collection_id",
                "status"
            ]

## Update Discount. [PUT /discounts/{id}]
update product Discount

+ Parameters
    + id: (string, optional) - discount id

+ Request (application/json)
    + Body

            [
                "code",
                "total",
                "discount",
                "type",
                "minimal_order",
                "start_date",
                "end_date",
                "product_id",
                "category_id",
                "collection_id",
                "status"
            ]

## Delete Discount. [DELETE /discounts/id]
delete product discount

+ Parameters
    + id: (string, optional) - discount id

# Blogs [/blogs]

## Display all blog. [GET /blogs/?query='']
Get a JSON representation of all the blog.

+ Parameters
    + query: (string, optional) - search blog by name

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "id": 22135,
                        "name": "sample blog title",
                        "tags": "blogs, sample",
                        "content": "smaple blog content",
                        "url": "http:\/\/testingdong.jstore.co\/blog\/tips-dan-trik\/sample-blog-title",
                        "status": 1,
                        "created_at": {
                            "date": "-0001-11-30 00:00:00.000000",
                            "timezone_type": 3,
                            "timezone": "UTC"
                        },
                        "updated_at": {
                            "date": "-0001-11-30 00:00:00.000000",
                            "timezone_type": 3,
                            "timezone": "UTC"
                        }
                    },
                    {
                        "id": 22136,
                        "name": "sample blog title",
                        "tags": "blogs, sample",
                        "content": "smaple blog content",
                        "url": "http:\/\/testingdong.jstore.co\/blog\/tips-dan-trik\/sample-blog-title",
                        "status": 1,
                        "created_at": {
                            "date": "-0001-11-30 00:00:00.000000",
                            "timezone_type": 3,
                            "timezone": "UTC"
                        },
                        "updated_at": {
                            "date": "-0001-11-30 00:00:00.000000",
                            "timezone_type": 3,
                            "timezone": "UTC"
                        }
                    }
                ]
            }

## Display one blog. [GET /blogs/{id}]
Get a JSON representation of one blog.

+ Parameters
    + id: (string, optional) - blog id

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 22135,
                    "name": "sample blog title",
                    "tags": "blogs, sample",
                    "content": "smaple blog content",
                    "url": "http:\/\/testingdong.jstore.co\/blog\/tips-dan-trik\/sample-blog-title"
                }
            }

## Create one blog. [POST /blogs]
Create new blog.

+ Request (application/json)
    + Body

            [
                "title",
                "content",
                "category_id",
                "slug",
                "tags",
                "status"
            ]

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 22137,
                    "name": "sample blog title",
                    "tags": "blogs, sample",
                    "content": "smaple blog content",
                    "url": "http:\/\/testingdong.jstore.co\/blog\/tips-dan-trik\/sample-blog-title",
                    "status": null,
                    "created_at": {
                        "date": "-0001-11-30 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "UTC"
                    },
                    "updated_at": {
                        "date": "-0001-11-30 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "UTC"
                    }
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "The title field is required.,The slug field is required.",
                "status_code": 422
            }

## Update one blog. [PUT /blogs/{id}]


+ Request (application/json)
    + Body

            [
                "title",
                "content",
                "category_id",
                "slug",
                "tags",
                "status"
            ]

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 22137,
                    "name": "sample blog title",
                    "tags": "blogs, sample",
                    "content": "smaple blog content",
                    "url": "http:\/\/testingdong.jstore.co\/blog\/tips-dan-trik\/sample-blog-title",
                    "status": null,
                    "created_at": {
                        "date": "-0001-11-30 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "UTC"
                    },
                    "updated_at": {
                        "date": "-0001-11-30 00:00:00.000000",
                        "timezone_type": 3,
                        "timezone": "UTC"
                    }
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "The title field is required.,The slug field is required.",
                "status_code": 422
            }

## Delete one blog. [DELETE /blogs/{id}]


+ Response 200 (application/json)
    + Body

            {
                "message": "Successfully delete Blog",
                "status_code": 200
            }

# Users [/users]

## Display shop users. [GET /users]
Get a JSON representation of all users.

+ Parameters
    + type: (string, optional) - user type, 0 = guest, 1 = member, 2 = mitra

## Add new shop users. [POST /users]
add new shop user.

+ Request (application/json)
    + Body

            [
                "name",
                "email",
                "password",
                "address",
                "country_id",
                "province_id",
                "city_id",
                "telp",
                "postcode",
                "type",
                "birth_of_date"
            ]

## Display 1 user. [GET /users/{id}]
Get a JSON representation of detail the user.

+ Parameters
    + id: (string, optional) - user id

## update users. [PUT /users/{id}]
update shop user.

+ Request (application/json)
    + Body

            [
                "name",
                "email",
                "password",
                "address",
                "country_id",
                "province_id",
                "city_id",
                "telp",
                "postcode",
                "type",
                "birth_of_date"
            ]

## Delete users. [DELETE /users/{id}]
update shop user.

# Testimonial [/testimonial]

## Get all testimonial. [GET /testimonial]
Get a JSON representation of all the testimonial.

## Display 1 testimonial. [GET /testimonial/{id}]
Get a JSON representation of 1 the testimonial.

+ Parameters
    + id: (string, optional) - testimony id

## Store testimonial. [POST /testimonial]
add new testimonial

+ Request (application/json)
    + Body

            [
                "name",
                "content",
                "visibility"
            ]

## Update Testimonial. [PUT /testimonial/id]
update testimonial

+ Parameters
    + id: (string, optional) - testimonial id

+ Request (application/json)
    + Body

            [
                "name",
                "content",
                "visibility"
            ]

## Delete Testimonial. [DELETE /testimonial/id]
delete testimonial

+ Parameters
    + id: (string, optional) - testimonial id

# Orders [/orders]

## Display all order. [GET /orders]
Get a JSON representation of order.

## Display detail order. [GET /orders/{order_code}]
Get a JSON representation of order.

+ Parameters
    + order_code: (string, optional) - order code

# Image [/images]

## Store new image [POST /images]
Api to upload image.

+ Parameters
    + file: (string, required) - product name

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "filename": "20170926-131620.jpg"
                }
            }

# Country [/countries]

## Display all country. [GET /countries]
Get a JSON representation of all the products.

# Province [/provinces]

## Display all provinces. [GET /provinces/?country_id={id}]
Get a JSON representation of all the province.

+ Parameters
    + country_id: (string, optional) - country id

# City [/cities]

## Display all city. [GET /cities/?province_id={id}]
Get a JSON representation of all the city.

+ Parameters
    + province_id: (string, optional) - filter by province id

# Subdistrict [/subdistricts]

## Display all subdistrict. [GET /subdistricts/?city_id={id}]
Get a JSON representation of all the province.

+ Parameters
    + city_id: (string, optional) - city id

# Shop Category [/shop-categories]

## Display all shop category. [GET /shop-categories]
Get a JSON representation of all the shop category.

# Jarvis Theme [/jarvis-themes]

## Display all jarvis theme. [GET /jarvis-themes/?type=]
Get a JSON representation of all the jarvis theme.

+ Parameters
    + type: (integer, optional) - theme type, 1 for free, 2 for paid

# Account [/account]

## Display shop information. [GET /account]
Get a JSON representation of account